import React, { useState, useCallback } from "react";
import styled from "styled-components";
import { connect } from "react-redux";

import LayoutEditor from "./LayoutEditor";
import FeaturesEditor from "./FeaturesEditor";
import DetailsFeature from "./DetailsFeature";
import Icon from "widgets/Icon";

import { changeLayoutPage } from "redux/layout";
import { updateProperty } from "redux/style";
import { selectItem } from "redux/monday";

const PropertyHeader = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 22px;

  display: flex;
  align-items: center;
  justify-content: center;

  position: relative;

  padding-top: 22px;
  padding-bottom: 22px;

  color: #303655;

  border-bottom: 1px solid #dfdfdf;
`;

const PropertyTab = styled.div`
  padding-top: 17px;
  padding-bottom: 17px;
  margin: 0px;

  width: 300px;

  display: flex;
  margin: auto;

  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 16px;
  text-transform: uppercase;

  color: #333333;
`;

const PropertyTabBar = styled.ul`
  display: flex;
  justify-content: space-between;

  height: 30px;
  width: 100%;

  padding: 0;
  margin: 0;

  background: #ededf3;
  border-radius: 30px;
`;

const PropertyTabItem = styled.li`
  width: 100%;
  cursor: pointer;

  display: flex;
  justify-content: center;
  align-items: center;

  text-transform: uppercase;

  &.active {
    background: #ffffff;
    box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.19);
    border-radius: 26px;
    margin: 3px;
    width: calc(100% - 6px);
  }
`;

const PropertyTabBody = styled.div`
  font-family: Open Sans;
  font-style: normal;
`;

const RightControl = styled.span`
  & > svg {
    width: 30px;
    height: 30px;
  }

  & > svg {
    position: absolute;
    right: 35px;
    top: 50%;
    transform: translateY(-50%);

    cursor: pointer;
  }
`;

const LAYOUTS = ["layout", "features"];

const PropertyLayout = React.memo((props) => {
  const { propertyData, options ,items} = props;
  const { updateProperty, changeLayoutPage, selectItem } = props;

  const [tabIndex, setTab] = useState(0);
  const renderTab = useCallback(() => {
    return LAYOUTS.map((layout, index) => (
      <PropertyTabItem
        key={`property-tab-${index}`}
        className={index === tabIndex ? "active" : ""}
        onClick={() => setTab(index)}>
        {layout}
      </PropertyTabItem>
    ));
  }, [tabIndex]);

  const renderBody = useCallback(() => {
    switch (tabIndex) {
      case 0:
        return (
          <LayoutEditor
            propertyData={propertyData}
            options={options}
            updateProperty={updateProperty}
            selectItem={selectItem}
          />
        );
      case 1:
        if(propertyData.style === "details"){
          return <DetailsFeature updateProperty={updateProperty} propertyData={propertyData} items={items} ></DetailsFeature>
        }else{
          return <FeaturesEditor propertyData={propertyData} options={options} updateProperty={updateProperty} items={items}  /> 
        }
      default:
        return <></>;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tabIndex, propertyData, options]);

  return (
    <>
      <PropertyHeader>
        Properties
        {propertyData.style === "details" ? (
          <RightControl onClick={() => changeLayoutPage("new_component")}>
            <Icon name="plus" />
          </RightControl>
        ) : (
            <></>
          )}
      </PropertyHeader>
      <PropertyTab>
        <PropertyTabBar>{renderTab()}</PropertyTabBar>
      </PropertyTab>
      <PropertyTabBody>{renderBody()}</PropertyTabBody>
    </>
  );
});

const mapStateToProps = (state) => ({
  propertyData: state.StyleReducer.tabs
    ? state.StyleReducer.tabs[state.StyleReducer.currentTab].property
    : {},
  options: state.MondayReducer.columns,
  items : state.MondayReducer.items
});

const mapDispatchToProps = (dispatch) => ({
  updateProperty: (field, value) => dispatch(updateProperty(field, value)),
  changeLayoutPage: (pageIndex) => dispatch(changeLayoutPage(pageIndex)),
  selectItem: (index) => dispatch(selectItem(index)),
  
});
export default connect(mapStateToProps, mapDispatchToProps)(PropertyLayout);
