import React from "react";

import RSelect from "widgets/RSelect";
import StatusCard from "widgets/RStatusCard";

const ListComponent = React.memo((props) => {
  const { propertyData, options } = props;
  const { updateProperty } = props;
  return (
    <>
      <StatusCard name="data">
        <RSelect
          label="Title"
          options={options}
          value={propertyData.title}
          onChange={(id) => updateProperty("title", id)}
          inputType={"stringy"}
        />
        <RSelect
          label="Details"
          options={options}
          value={propertyData.details}
          onChange={(id) => updateProperty("details", id)}
          inputType={"stringy"}
        />
        <RSelect
          label="Caption"
          options={options}
          value={propertyData.caption}
          onChange={(id) => updateProperty("caption", id)}
          inputType={"stringy"}
        />
        <RSelect
          label="Image"
          options={options}
          value={propertyData.image}
          onChange={(id) => updateProperty("image", id)}
          inputType={"imagey"}
        />
      </StatusCard>

      {/* <StatusCard name="options">
        <ROption
          label="Show title when image is not available"
          value={propertyData.show_image}
          onChange={(value) => updateProperty("option", value)}
        />
      </StatusCard> */}
    </>
  );
});

export default ListComponent;
