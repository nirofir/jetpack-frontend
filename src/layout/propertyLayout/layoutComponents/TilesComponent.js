import React from "react";

import RSelect from "widgets/RSelect";
import RStatusCard from "widgets/RStatusCard";
import ROption from "widgets/ROption";
import RInputForm from "widgets/RInputForm";
import RSVGButton from "widgets/RSVGButton";

import Slider, { Handle } from "rc-slider";
import "rc-slider/assets/index.css";
import { IMAGE_VIEW_METHODS, TILE_SHAPES } from "constants/constants";

const TilesComponent = React.memo((props) => {
  const { propertyData, options } = props;
  const { updateProperty } = props;
  return (
    <>
      <RStatusCard name="data">
        <RSelect
          label="Title"
          options={options}
          value={propertyData.title}
          onChange={(id) => updateProperty("title", id)}
          inputType={"stringy"}
        />
        <RSelect
          label="Details"
          options={options}
          value={propertyData.details}
          onChange={(id) => updateProperty("details", id)}
          inputType={"stringy"}
        />
        <RSelect
          label="Image"
          options={options}
          value={propertyData.image}
          onChange={(id) => updateProperty("image", id)}
          inputType={"imagey"}
        />
      </RStatusCard>

      <RStatusCard name="design">
        <RSelect
          label="Image is"
          options={IMAGE_VIEW_METHODS}
          value={propertyData.image_is}
          onChange={(id) => updateProperty("image_is", id)}
        />
        <RSelect
          label="Tile Shape"
          options={TILE_SHAPES}
          value={propertyData.shape}
          onChange={(id) => updateProperty("shape", id)}
        />

        <RInputForm label="Tiles per row">
          <Slider
            value={propertyData.tiles_in_row}
            min={1}
            max={4}
            marks={{ 1: 1, 2: 2, 3: 3, 4: 4 }}
            step={null}
            onChange={(id) => updateProperty("tiles_in_row", id)}
          >
            <Handle />
          </Slider>
        </RInputForm>

        {propertyData.shape === "circle" ? (
          <></>
        ) : (
          <RInputForm label="Corners">
            <RSVGButton
              icon="rectangle"
              active={propertyData.corner}
              onClick={() => updateProperty("corner", true)}
            />
            <RSVGButton
              icon="round"
              active={!propertyData.corner}
              onClick={() => updateProperty("corner", false)}
            />
          </RInputForm>
        )}

        <RInputForm label="Padding">
          <RSVGButton
            icon="padding_out"
            active={propertyData.padding}
            onClick={() => updateProperty("padding", true)}
          />
          <RSVGButton
            icon="padding_in"
            active={!propertyData.padding}
            onClick={() => updateProperty("padding", false)}
          />
        </RInputForm>

        <RInputForm label="Crop behavior">
          <RSVGButton
            icon="crop"
            active={propertyData.crop}
            onClick={() => updateProperty("crop", true)}
          />
          <RSVGButton
            icon="center"
            active={!propertyData.crop}
            onClick={() => updateProperty("crop", false)}
          />
        </RInputForm>
      </RStatusCard>
{/* 
      <RStatusCard name="options">
        <ROption
          label="Show title when image is not available"
          value={propertyData.option}
          onChange={(value) => updateProperty("option", value)}
        />
      </RStatusCard> */}

      <RStatusCard name="text style">
        <RInputForm label="Position">
          <RSVGButton
            icon="below"
            active={propertyData.text_position}
            onClick={() => updateProperty("text_position", true)}
          />
          <RSVGButton
            icon="overlay"
            active={!propertyData.text_position}
            onClick={() => updateProperty("text_position", false)}
          />
        </RInputForm>
        {propertyData.text_position ? (
          <></>
        ) : (
          <RInputForm label="Alignment">
            <RSVGButton
              icon="top_left"
              active={propertyData.text_alignment === "top_left"}
              onClick={() => updateProperty("text_alignment", "top_left")}
            />
            <RSVGButton
              icon="bottom_left"
              active={propertyData.text_alignment === "bottom_left"}
              onClick={() => updateProperty("text_alignment", "bottom_left")}
            />
            <RSVGButton
              icon="bottom_right"
              active={propertyData.text_alignment === "bottom_right"}
              onClick={() => updateProperty("text_alignment", "bottom_right")}
            />
            <RSVGButton
              icon="top_right"
              active={propertyData.text_alignment === "top_right"}
              onClick={() => updateProperty("text_alignment", "top_right")}
            />
            <RSVGButton
              icon="align_center"
              active={propertyData.text_alignment === "align_center"}
              onClick={() => updateProperty("text_alignment", "align_center")}
            />
          </RInputForm>
        )}

        <RInputForm label="text size">
          <RSVGButton
            icon="small"
            active={propertyData.text_size === "small"}
            onClick={() => updateProperty("text_size", "small")}
          />
          <RSVGButton
            icon="medium"
            active={propertyData.text_size === "medium"}
            onClick={() => updateProperty("text_size", "medium")}
          />
          <RSVGButton
            icon="large"
            active={propertyData.text_size === "large"}
            onClick={() => updateProperty("text_size", "large")}
          />
        </RInputForm>

        <ROption
          label="Allow text wrapping"
          value={propertyData.text_wrap}
          onChange={(value) => updateProperty("text_wrap", value)}
        />
        <ROption
          label="All-Caps titles"
          value={propertyData.text_caps}
          onChange={(value) => updateProperty("text_caps", value)}
        />
      </RStatusCard>

      {/* <RStatusCard name="overlays">
        <RSelect
          label="Button"
          options={OVERLAYS}
          value={propertyData.button}
          onClick={(id) => updateProperty("button", id)}
        />
        <RSelect
          label="Tag"
          options={OVERLAYS}
          value={propertyData.tag}
          onClick={(id) => updateProperty("tag", id)}
        />
        <RSelect
          label="Avatar"
          options={OVERLAYS}
          value={propertyData.avatar}
          onClick={(id) => updateProperty("avatar", id)}
        />
        <RSelect
          label="Avatar text"
          options={OVERLAYS}
          value={propertyData.avatar_text}
          onClick={(id) => updateProperty("avatar_text", id)}
        />
        <RSelect
          label="Caption"
          options={OVERLAYS}
          value={propertyData.caption_overlay}
          onClick={(id) => updateProperty("caption_overlay", id)}
        />
      </RStatusCard> */}
    </>
  );
});

export default TilesComponent;
