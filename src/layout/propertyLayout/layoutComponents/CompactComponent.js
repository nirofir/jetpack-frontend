import React from "react";

import RStatusCard from "widgets/RStatusCard";
import RSelect from "widgets/RSelect";

const CompactComponent = React.memo((props) => {
  const { propertyData, options } = props;
  const { updateProperty } = props;

  return (
    <>
      <RStatusCard name="data">
        <RSelect
          label="Title"
          options={options}
          value={propertyData.title}
          onChange={(id) => updateProperty("title", id)}
          inputType={"stringy"}
        />
        <RSelect
          label="Details"
          options={options}
          value={propertyData.details}
          onChange={(id) => updateProperty("details", id)}
          inputType={"stringy"}
        />
        <RSelect
          label="Image"
          options={options}
          value={propertyData.image}
          onChange={(id) => updateProperty("image", id)}
          inputType={"imagey"}
        />
      </RStatusCard>

      {/* <RStatusCard name="options">
        <ROption
          label="Show title when image is not available"
          value={propertyData.show_image}
          onChange={(value) => updateProperty("option", value)}
        />
      </RStatusCard> */}
    </>
  );
});

export default CompactComponent;
