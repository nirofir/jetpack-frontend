import React from "react";

import RStatusCard from "widgets/RStatusCard";
import RSelect from "widgets/RSelect";

const CalendarComponent = React.memo((props) => {
  const { propertyData, options, updateProperty } = props;
  return (
    <>
      <RStatusCard name="data">
        <RSelect
          label="Title"
          options={options}
          value={propertyData.title}
          inputType={"stringy"}
          onChange={(id) => updateProperty("title", id)}
        />
        <RSelect
          label="When"
          options={options}
          value={propertyData.when}
          inputType={"datey"}
          onChange={(id) => updateProperty("when", id)}
        />
        {/* <RSelect
          label="Order"
          options={options}
          value={propertyData.order}
          onChange={(id) => updateProperty("order", id)}
        /> */}
      </RStatusCard>
    </>
  );
});

export default CalendarComponent;
