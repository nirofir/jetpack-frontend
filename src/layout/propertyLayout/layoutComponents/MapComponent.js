import React from "react";

import RStatusCard from "widgets/RStatusCard";
import RSelect from "widgets/RSelect";
import RInputForm from "widgets/RInputForm";
import RSVGButton from "widgets/RSVGButton";
import ROption from "widgets/ROption";

const MapComponent = React.memo((props) => {
  const { propertyData, options } = props;
  const { updateProperty } = props;

  return (
    <>
      <RStatusCard name="data">
        <RSelect
          label="Address"
          options={options}
          value={propertyData.address}
          onChange={(id) => updateProperty("address", id)}
          inputType={"addressy"}
        />
        <RSelect
          label="Title"
          options={options}
          value={propertyData.title}
          onChange={(id) => updateProperty("title", id)}
          inputType={"stringy"}
        />
        <RSelect
          label="Details"
          options={options}
          value={propertyData.details}
          onChange={(id) => updateProperty("details", id)}
          inputType={"stringy"}
        />
        <RSelect
          label="Image"
          options={options}
          value={propertyData.image}
          onChange={(id) => updateProperty("image", id)}
          inputType={"imagey"}
        />
        <RSelect
          label="Caption"
          options={options}
          value={propertyData.caption}
          onChange={(id) => updateProperty("caption", id)}
          inputType={"stringy"}
        />
      </RStatusCard>

      <RStatusCard name="design">
        <RInputForm label="Default view">
          <RSVGButton
            icon="map"
            active={propertyData.view}
            onClick={() => updateProperty("view", true)}
          />
          <RSVGButton
            icon="todolist"
            active={!propertyData.view}
            onClick={() => updateProperty("view", false)}
          />
        </RInputForm>
      </RStatusCard>

      <RStatusCard name="options">
        <ROption
          label="Allow user to show current location"
          value={propertyData.option}
          onChange={(value) => updateProperty("option", value)}
        />
      </RStatusCard>
    </>
  );
});

export default MapComponent;
