import React from "react";

import RSelect from "widgets/RSelect";
import RStatusCard from "widgets/RStatusCard";

const options = [
  {
    icon: "text",
    label: "Name",
    id: "text",
  },
  {
    icon: "text",
    label: "Title",
    id: "text1",
  },
  {
    icon: "text",
    label: "Title",
    id: "text2",
  },
  {
    icon: "image",
    label: "Image",
    id: "text3",
  },
];

const OverlayComponent = React.memo(() => {
  return (
    <>
      <RStatusCard name="data">
        <RSelect label="Title" options={options} value={"text2"} />
        <RSelect label="Details" options={options} value={"text2"} />
        <RSelect label="Order" options={options} value={"text2"} />
      </RStatusCard>
    </>
  );
});

export default OverlayComponent;
