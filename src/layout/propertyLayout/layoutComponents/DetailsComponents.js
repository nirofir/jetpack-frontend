import React from "react";
import { connect } from "react-redux";

import RStatusCard from "widgets/RStatusCard";
import DragComponents from "layout/componentLayout/DragComponents";

import { updateProperty } from "redux/style";
import {
  selectDetailComponent,
  reorderDetailComponents,
  removeDetailComponent,
  duplicateDetailComponent,
} from "redux/style";
import { changeLayoutPage } from "redux/layout";

const DetailsLayout = React.memo((props) => {
  const { componentData, componentDetails, options
    // columnsData 
  } = props;
  const {
    selectComponent,
    changeLayoutPage,
    reorderComponents,
    removeComponent,
    duplicateComponent,
  } = props;
  return (
    <>
      <RStatusCard name="Components">
        <DragComponents
          componentData={componentData}
          componentDetails={componentDetails}
          changeLayoutPage={changeLayoutPage}
          columnsData={options}
          selectComponent={selectComponent}
          reorderComponents={reorderComponents}
          removeComponent={removeComponent}
          duplicateComponent={duplicateComponent}
        />
      </RStatusCard>
    </>
  );
});

const mapStateToProps = (state) => ({
  options: state.MondayReducer.column_json,
  propertyData: state.StyleReducer.tabs[state.StyleReducer.currentTab].property,
  // columnsData: state.ColumnsReducer,
  componentData: state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle.components,
  componentDetails:
    state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle.componentDetails,
});

const mapDispatchToProps = (dispatch) => ({
  updateProperty: (field, value) => dispatch(updateProperty(field, value)),
  changeLayoutPage: (value) => dispatch(changeLayoutPage(value)),
  selectComponent: (item) => dispatch(selectDetailComponent(item)),
  removeComponent: (index) => dispatch(removeDetailComponent(index)),
  reorderComponents: (components, details) => dispatch(reorderDetailComponents(components, details)),
  duplicateComponent: (index) => dispatch(duplicateDetailComponent(index)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsLayout);
