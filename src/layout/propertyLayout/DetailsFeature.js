import React  from "react";

import RSelect from "widgets/RSelect";
import RStatusCard from "widgets/RStatusCard";

const DetailsFeature = React.memo((props) => {
    const { items ,propertyData,updateProperty} = props;
    console.log(items)
    const upd = (id) => {
        // setId(id)
        console.log(items.map(item => item.item_id.value).indexOf(id),"wow")
        updateProperty("detailsIndex", items.map(item => item.item_id.value).indexOf(id))
        // selectItem(items.map(item => item.item_id.value).indexOf(id))
    }
    return (
      <>
        <RStatusCard name="Item">
        <RSelect
          label="Select item by Id"
          options={items.map(item => ({id : item.item_id.text ,
                                     label : item.item_id.text}))}
          value={items[propertyData.detailsIndex].item_id.text}
          onChange={(id) => upd(id)}
        />
        </RStatusCard>
      </>
    );
});

export default DetailsFeature;
