import React ,{useState} from "react";
import styled from "styled-components";
import RButton from "widgets/RButton";
import RTransButton from "widgets/RTransButton";
import RSelect from "widgets/RSelect";

const InputBox = styled.div`
width: 100%;
input{
  width: 100%;
  border: 1px solid #bdbdbd;
  border-radius: 15px; 
  box-sizing: border-box;
  margin-top: 10px;
  margin-bottom: 10px;
  font-size: 14px;
  padding-left: 1rem;
  padding-top: 0.3rem;
  padding-bottom: 0.3rem;
  padding-right: 1rem;
  background: #ffffff;
}

`;

export const FilterComponent = ({ chooseFilter,options,operators,itsVisComponent,items}) => {
  // const [options,setOptions] = useState(optionsColumns)
  const [column,setColumn] = useState(options[0].id);
  const [operator,setOperator] = useState(operators.basic[0].id)
  const [filterBy, setFilterBy] = useState("")
  const [active , setActive] = useState(false)
  const [isOpen,setIsOpen] = useState(false);

  const getOperator = (operType) => operators[operType] ? operators[column] : operators.basic;

  const restoreToDefualt = () => {
    setIsOpen(!isOpen);
    setColumn(options[0].id)
    setOperator(operators.basic[0].id)
    setFilterBy("")
    setActive(false)
  }

  const openFilterThing = () => {
    console.log(isOpen)
    if(isOpen && active){
      chooseFilter({column,operator,filterBy})
    }
    restoreToDefualt()
  }
  const handleOperator = (value) => {
    setActive(true)
    setOperator(value)
  }

  const handleColumn = (value) => {
      setActive(true)
      setColumn(value);
  }

  const handleChange = (f) => {
    setFilterBy(f.target.value)
  }

  return (<div style={
              {display:"flex",
              flexDirection:"column"}}>
        
        { 
          !itsVisComponent &&
          // (isOpen ? 
          //   <p>Add your own filter using the options</p> :
          //   <p>Limit the items displayed based on their prop.</p>)
          (isOpen ? 
                <p>Add your own filter using the options</p> :
                <p>Limit the items displayed based on their prop.</p>)
        }

        {isOpen && <>
                <RSelect
                  label="filter by"
                  options={options}
                  value={column}
                  onChange={(value) => handleColumn(value)}/>
                <RSelect
                  label="operator"
                  options={getOperator(column)}
                  value={operator}
                  onChange={(value) => handleOperator(value)}/>
                {operator == "!!" || operator == "!!!" ||
                  <InputBox>
                  <input onChange={handleChange}>
                  </input>
                  </InputBox >
                }
              </>
        }
      {isOpen ? (active ? <RTransButton label="Add!" click={() => openFilterThing()} />  
                        : <div onClick={() => openFilterThing()} 
                              style={{color : "#009aff", 
                              fontWeight: "bold",
                              textAlign: "center",
                              cursor: "pointer"}} >^</div> )
                : <RTransButton label={itsVisComponent ? "Add Condition" : "Add Filter"} click={() => openFilterThing()} />}
      
  </div>)
}

export const SortComponent = ({ chooseSorting,options}) => {
  const [column,setColumn] = useState(options[0].id);
  const [isAccend,setAccend] = useState(true)
  const [active , setActive] = useState(false)
  const [isOpen,setIsOpen] = useState(false);


  const restoreToDefualt = () => {
    setIsOpen(!isOpen);
    setColumn(options[0].id)
    setActive(false)
  }

  const openAndCloseSortThing = () => {
    console.log(isOpen)
    if(isOpen && active){
      chooseSorting({column,isAccend})
    }
    restoreToDefualt()
  }

  const handleAccendInput = () => {
    setActive(true)
    setAccend(!isAccend)
  }

  const handleColumn = (value) => {
      setActive(true)
      setColumn(value);
  }

  return (<div style={
              {display:"flex",
              flexDirection:"column"}}>
        
        {isOpen ? 
                <p>Choose your sort-by field</p> :
                <p>Order the items displayed based on their prop.</p> 
      }

        {isOpen && 
              <>
                <RSelect
                  label="filter by"
                  options={options}
                  value={column}
                  onChange={(value) => handleColumn(value)}/>
                <div onClick={() => handleAccendInput()}>
                    {isAccend ? "Accending" : "Descending"}
                </div>
              </>
        }
      {isOpen ? (active ? <RTransButton label="Add!" click={() => openAndCloseSortThing()} />  
                        : <div onClick={() => openAndCloseSortThing()} 
                              style={{color : "#009aff", 
                              fontWeight: "bold",
                              textAlign: "center",
                              cursor: "pointer"}} >^</div> )
                : <RTransButton label="Add Sorting" click={() => openAndCloseSortThing()} />}
      
  </div>)
}