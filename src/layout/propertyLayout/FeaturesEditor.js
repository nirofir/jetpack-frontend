import React from "react";

import ROption from "widgets/ROption";
import RButton from "widgets/RButton";
import { connect } from 'react-redux'
import RStatusCard from "widgets/RStatusCard";

import operators from "../../constants/filterBy";
import { FilterComponent, SortComponent } from "./components"

const FeaturesEditor = React.memo((props) => {
  const { propertyData, options ,items} = props;
  const { updateProperty } = props;

  const handleAddFilter = (value) => {
    const operatorsList = operators[value.column] || operators.basic
    value.operator = operatorsList.filter(opr => opr.id === value.operator)[0]
    // const filterObj = getFilterFunction(value)
    updateProperty("filter_by", value)
  }

  const handleAddSorting = (value) => {
    console.log(value)
    const sorting = value.isAccend ? (stuff) => stuff.sort((a, b) => a[value.column].value - b[value.column].value)
      : (stuff) => stuff.sort((a, b) => a[value.column].value + b[value.column].value)
    updateProperty("sort_by", sorting)
  }

  return (
    <>
      <RStatusCard name="search">
        <ROption
          label="Show search bar"
          value={propertyData.hasSearchBar}
          onChange={(value) => updateProperty("hasSearchBar", !propertyData.hasSearchBar)}
        />
      </RStatusCard>

      <RStatusCard name="filter">
        <FilterComponent options={options.slice(1)}
          chooseFilter={(x) => handleAddFilter(x)}
          operators={operators}>
          items={items}
        </FilterComponent>
      </RStatusCard>
      {propertyData.filter_by === "none" ||
        <RStatusCard name="filter list">
          {/* {logicalFilterList.map((filter) =>  */}
          <div style={{
            display: "flex",
            alignItems: "baseline",
            justifyContent: "space-between"
          }}>
            <RButton label={"Field " + propertyData.filter_by.column + " "
              + propertyData.filter_by.operator.label.toLowerCase()
              + " " + propertyData.filter_by.filterBy} gray />

            <div style={{
              color: "#009aff",
              fontWeight: "bold",
              textAlign: "center",
              cursor: "pointer"
            }}
              onClick={() => updateProperty("filter_by", "none")}>X</div>
          </div>
          {/* )} */}
        </RStatusCard>
      }

      <RStatusCard name="sort">
        <SortComponent options={options.slice(1)}
          chooseSorting={(x) => handleAddSorting(x)}>
        </SortComponent>
      </RStatusCard>

    </>
  );
});


const mapStateToProps = (state) => ({

  hasSearchBar: state.StyleReducer.tabs[state.StyleReducer.currentTab].property.hasSearchBar

});
export default connect(mapStateToProps, null)(FeaturesEditor);
