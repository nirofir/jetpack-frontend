import React, { useCallback } from "react";
import styled from "styled-components";

import ListComponent from "./layoutComponents/ListComponent";
import CompactComponent from "./layoutComponents/CompactComponent";
import TilesComponent from "./layoutComponents/TilesComponent";
import OverlayComponent from "./layoutComponents/OverlayComponent";
import MapComponent from "./layoutComponents/MapComponent";
import DetailsComponent from "./layoutComponents/DetailsComponents";
import CalendarComponent from "./layoutComponents/CalendarComponent";

import Icon from "widgets/Icon";
import RStatusCard from "widgets/RStatusCard";
// import constant variables
import { STYLE_WIDGETS } from "constants/constants";

const StyleEditorBox = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 25% [col-start]);
`;

const StyleEditorItem = styled.div`
  height: 60px;
  min-width : 60px;
  width: auto;

  font-weight: normal;
  font-size: 14px;
  line-height: 19px;

  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;

  justify-content: space-between;

  padding: 10px;

  cursor: pointer;

  color: #333333;

  :hover,
  &.active {
    background: #3ad6f514;
    border-radius: 5px;

    color: #009aff;
  }
  :hover svg path,
  &.active svg path {
    fill: #009aff;
  }
`;

const StyleEditorItemIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  height: 100%;
`;

const StyleEditorItemLabel = styled.div`
  display: flex;
  justify-content: center;
`;

const LayoutEditor = React.memo((props) => {
  const { propertyData, options } = props;
  const { updateProperty } = props;

  const renderComponent = useCallback(() => {
    switch (propertyData.style) {
      case "list":
        return (
          <ListComponent
            propertyData={propertyData}
            options={options}
            updateProperty={updateProperty}
          />
        );
      case "compact":
        return (
          <CompactComponent
            propertyData={propertyData}
            options={options}
            updateProperty={updateProperty}
          />
        );
      case "tiles":
        return (
          <TilesComponent
            propertyData={propertyData}
            options={options}
            updateProperty={updateProperty}
          />
        );
      case "calendar":
        return (
          <CalendarComponent
            propertyData={propertyData}
            options={options}
            updateProperty={updateProperty}
          />
        );
      case "overlay":
        return (
          <OverlayComponent
            propertyData={propertyData}
            options={options}
            updateProperty={updateProperty}
          />
        );
      case "map":
        return (
          <MapComponent
            propertyData={propertyData}
            options={options}
            updateProperty={updateProperty}
          />
        );
      case "details":
        return (
          <DetailsComponent
            propertyData={propertyData}
            options={options}
            updateProperty={updateProperty}
          />
        );
      default:
        return <></>;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [propertyData, options]);

  const updatePropertyData = useCallback((id) => {
    updateProperty("style", id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <RStatusCard name="style">
        <StyleEditorBox>
          {STYLE_WIDGETS.map((widget, index) => (
            <StyleEditorItem
              key={`style-editor-${index}`}
              className={propertyData.style === widget.id ? "active" : ""}
              onClick={() => updatePropertyData(widget.id)}
            >
              <StyleEditorItemIcon>
                <Icon name={widget.icon} />
              </StyleEditorItemIcon>
              <StyleEditorItemLabel>{widget.label}</StyleEditorItemLabel>
            </StyleEditorItem>
          ))}
        </StyleEditorBox>
      </RStatusCard>

      {renderComponent()}
    </>
  );
});

export default LayoutEditor;
