import React, { useState, useCallback, useEffect } from "react";

import {
  TEXT_MENUS,
  IMAGE_HEIGHTS,
  CROP_BEHAVIOURS,
  IMAGE_FILL_METHODS,
  IMAGE_STYLE_METHODS,
  ACTIONS_LIST,
} from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";
import RSVGButton from "widgets/RSVGButton";
import RSelect from "widgets/RSelect";
import Visability from "./Visability";

const ImageCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent, appTabs } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  const [selectedOption4, updateSelection4] = useState("name");
  const componentDetail = componentDetails[componentIndex];

  useEffect(() => {
    const item = items[itemIndex];
    setOptions([
      {
        id: "custom",
        label: "Custom Text",
        icon: "",
      },
      ...columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      }),
    ]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          {componentDetail.image === "custom" ? (
            <RInputForm label="Image">
              <input
                type="text"
                placeholder="Image URL"
                value={componentDetail.custom_image}
                onChange={(e) => updateComponent("custom_image", e.target.value)}
                inputType={"imagey"}
              />
            </RInputForm>
          ) : (
            <RSelect
              label="Image"
              options={options}
              value={componentDetail.image}
              onChange={(value) => updateComponent("image", value)}
              inputType={"imagey"}
            />
          )}
        </RStatusCard>

        <RStatusCard name="Design">
          <RInputForm label="Image height">
            {IMAGE_HEIGHTS.map((btn, index) => (
              <RSVGButton
                key={`image-button-${index}`}
                active={componentDetail.image_height === index}
                icon={btn.icon}
                onClick={() => updateComponent("image_height", index)}
              />
            ))}

            <RSVGButton
              active={componentDetail.image_height === IMAGE_HEIGHTS.length}
              onClick={() => updateComponent("image_height", IMAGE_HEIGHTS.length)}>
              1:1
            </RSVGButton>
          </RInputForm>

          <RSelect
            label="Fill"
            options={IMAGE_FILL_METHODS}
            value={componentDetail.fill}
            onChange={(value) => updateComponent("fill", value)}
          />

          <RSelect
            label="Style"
            options={IMAGE_STYLE_METHODS}
            value={componentDetail.style}
            onChange={(value) => updateComponent("style", value)}
          />

          <RInputForm label="Crop behavior">
            {CROP_BEHAVIOURS.map((btn, index) => (
              <RSVGButton
                key={`image-button-${index}`}
                icon={btn.icon}
                active={componentDetail.crop === index}
                onClick={() => updateComponent("crop", index)}
              />
            ))}
          </RInputForm>
        </RStatusCard>

        <RStatusCard name="Overlay">
          <RSelect
            label="Button"
            options={options}
            value={selectedOption4}
            onChange={(value) => updateSelection4(value)}
          />

          <RSelect
            label="Tag"
            options={options}
            value={selectedOption4}
            onChange={(value) => updateSelection4(value)}
          />

          <RSelect
            label="Avatar"
            options={options}
            value={selectedOption4}
            onChange={(value) => updateSelection4(value)}
          />

          <RSelect
            label="Avatar text"
            options={options}
            value={selectedOption4}
            onChange={(value) => updateSelection4(value)}
          />

          <RSelect
            label="Caption"
            options={options}
            value={selectedOption4}
            onChange={(value) => updateSelection4(value)}
          />
        </RStatusCard>
      </>
    ),
    [componentDetail, options, selectedOption4, updateComponent]
  );

  const renderFeaturesBody = () => (
    <>
      <Visability
        updateComponent={updateComponent}
        options={columns}
        componentDetail={componentDetail}></Visability>

      <RStatusCard name="Action">
        <RSelect
          options={ACTIONS_LIST}
          value={componentDetail.action}
          onChange={(value) => updateComponent("action", value)}
        />
        <RSelect
          label={componentDetail.action === "link_screen" ? "Tab" : componentDetail.data}
          options={componentDetail.action === "link_screen" ? appTabs : options}
          value={componentDetail.data}
          onChange={(value) => updateComponent("data", value)}
        />
      </RStatusCard>
    </>
  );

  const renderBody = () => {
    try {
      if (tab === 0) return renderLayoutBody();
      else return renderFeaturesBody();
    } catch {
      // renderLayoutBody();
    }
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default ImageCard;
