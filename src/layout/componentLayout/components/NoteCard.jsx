import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS } from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";
import RSelect from "widgets/RSelect";
import Visability from "./Visability";

import "./_style.css";

const NoteCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  const [details, setDetails] = useState("");
  const [image, setImage] = useState("");
  const componentDetail = componentDetails[componentIndex];

  useEffect(() => {
    const item = items[itemIndex];
    setOptions(
      columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [columns]);

  useEffect(() => setDetails(componentDetail.details), [componentDetail.details]);

  const onKeyDownHandler = useCallback(
    (e, type) => {
      if (e.keyCode === 13) {
        if (type === "details") updateComponent("details", details);
        else if (type === "image") updateComponent("image", image);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [details, image]
  );

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          <RSelect
            label="Column"
            options={options}
            value={componentDetail.column}
            onChange={(value) => updateComponent("column", value)}
          />

          <RInputForm label="Details">
            <input
              type="text"
              placeholder="Enter title"
              value={details}
              onChange={(e) => setDetails(e.target.value)}
              onKeyDown={(e) => onKeyDownHandler(e, "details")}
            />
          </RInputForm>

          <RInputForm label="Image">
            <input
              type="text"
              placeholder="Enter placeholder"
              value={image}
              onChange={(e) => setImage(e.target.value)}
              onKeyDown={(e) => onKeyDownHandler(e, "image")}
            />
          </RInputForm>
        </RStatusCard>
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [options, componentDetail, details, image]
  );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else
      return (
        <Visability
          componentDetail={componentDetail}
          updateComponent={updateComponent}
          options={columns}></Visability>
      );
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default NoteCard;
