import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS, TEXT_ALIGNMENTS } from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";
import RSelect from "widgets/RSelect";
import ROption from "widgets/ROption";
import RSVGButton from "widgets/RSVGButton";
import Visability from "./Visability";

import "./_style.css";

const styles = [
  {
    icon: "",
    label: "Small",
    id: "small",
  },
  {
    icon: "",
    label: "Regular",
    id: "regular",
  },
  {
    icon: "",
    label: "Large",
    id: "large",
  },
  {
    icon: "",
    label: "Footnote",
    id: "footnote",
  },
  {
    icon: "",
    label: "Headline1",
    id: "headline1",
  },
  {
    icon: "",
    label: "Headline2",
    id: "headline2",
  },
  {
    icon: "",
    label: "Headline3",
    id: "headline3",
  },
];

const TextCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  const componentDetail = componentDetails[componentIndex];

  useEffect(() => {
    console.log("Columns: ", columns);
    console.log("Item: ", items[itemIndex]);
    const item = items[itemIndex];
    setOptions([
      {
        id: "custom",
        label: "Custom Text",
        icon: "",
      },
      ...columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      }),
    ]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          {componentDetail.text === "custom" ? (
            <RInputForm label="Text">
              <input
                type="text"
                placeholder="Text"
                value={componentDetail.custom}
                onChange={(e) => updateComponent("custom", e.target.value)}
              />
            </RInputForm>
          ) : (
            <RSelect
              label="Text"
              options={options}
              value={componentDetail.text}
              onChange={(value) => updateComponent("text", value)}
            />
          )}
        </RStatusCard>

        <RStatusCard name="Design">
          <RSelect
            label="Style"
            options={styles}
            value={componentDetail.style}
            onChange={(value) => updateComponent("style", value)}
          />

          <RInputForm label="Align">
            {TEXT_ALIGNMENTS.map((alignment, index) => (
              <RSVGButton
                key={`text-alignment-${index}`}
                icon={alignment.icon}
                active={index === componentDetail.align}
                onClick={() => updateComponent("align", index)}
              />
            ))}
          </RInputForm>

          <ROption
            value={componentDetail.is_all_caps}
            onChange={(value) => updateComponent("is_all_caps", value)}
            label="All caps"
          />
          <ROption
            value={componentDetail.is_trancated}
            onChange={(value) => updateComponent("is_trancated", value)}
            label="Truncate text lines"
          />
        </RStatusCard>
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [componentDetail, options]
  );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else
      return (
        <Visability
          componentDetail={componentDetail}
          updateComponent={updateComponent}
          options={columns}></Visability>
      );
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default TextCard;
