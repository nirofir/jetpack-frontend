import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS, IMAGE_VIEW_METHODS } from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RSelect from "widgets/RSelect";
import RInputForm from "widgets/RInputForm";
import Visability from "./Visability";

const TitleCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  const componentDetail = componentDetails[componentIndex];
  console.log(componentDetail);
  useEffect(() => {
    const item = items[itemIndex];
    setOptions([
      {
        id: "custom",
        label: "Custom Text",
        icon: "",
      },
      ...columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      }),
    ]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          {componentDetail.title === "custom" ? (
            <RInputForm label="Title">
              <input
                type="text"
                placeholder="Text"
                value={componentDetail.custom_title}
                onChange={(e) => updateComponent("custom_title", e.target.value)}
              />
            </RInputForm>
          ) : (
            <RSelect
              label="Title"
              options={options}
              value={componentDetail.title}
              onChange={(value) => updateComponent("title", value)}
            />
          )}
          {componentDetail.details === "custom" ? (
            <RInputForm label="Details">
              <input
                type="text"
                placeholder="Details"
                value={componentDetail.custom_details}
                onChange={(e) => updateComponent("custom_details", e.target.value)}
              />
            </RInputForm>
          ) : (
            <RSelect
              label="Details"
              options={options}
              value={componentDetail.details}
              onChange={(value) => updateComponent("details", value)}
            />
          )}
          {componentDetail.image === "custom" ? (
            <RInputForm label="Image URL">
              <input
                type="text"
                placeholder="Image URL"
                value={componentDetail.custom_image}
                onChange={(e) => updateComponent("custom_image", e.target.value)}
              />
            </RInputForm>
          ) : (
            <RSelect
              label="Image"
              options={options}
              value={componentDetail.image}
              onChange={(value) => updateComponent("image", value)}
              inputType={"imagey"}
            />
          )}
        </RStatusCard>
        <RStatusCard name="Design">
          <RSelect
            label="Image is"
            options={IMAGE_VIEW_METHODS}
            value={componentDetail.design}
            onChange={(value) => updateComponent("url", value)}
          />
        </RStatusCard>
      </>
    ),
    [componentDetail, options, updateComponent]
  );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else
      return (
        <Visability
          componentDetail={componentDetail}
          updateComponent={updateComponent}
          options={columns}></Visability>
      );
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default TitleCard;
