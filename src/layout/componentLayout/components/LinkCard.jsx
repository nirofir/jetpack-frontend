import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS, LINK_LABEL_LIST, LINK_TITLE_LIST } from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RSelect from "widgets/RSelect";
import Visability from "./Visability";

import "./_style.css";

const LinkCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  // const [selectedOption, updateSelection] = useState("name");
  const componentDetail = componentDetails[componentIndex];

  useEffect(() => {
    const item = items[itemIndex];
    setOptions(
      columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [columns]);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          <RSelect
            label="Link"
            options={options}
            value={componentDetail.link}
            onChange={(value) => updateComponent("link", value)}
            inputType={"linky"}
          />
        </RStatusCard>

        <RStatusCard name="Design">
          <RSelect
            label="Label"
            options={LINK_LABEL_LIST}
            value={componentDetail.label}
            onChange={(value) => updateComponent("label", value)}
          />

          <RSelect
            label="Title"
            options={LINK_TITLE_LIST}
            value={componentDetail.title}
            onChange={(value) => updateComponent("title", value)}
          />
        </RStatusCard>
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [options, componentDetail]
  );

  // const renderFeaturesBody = useCallback(
  //   () => (
  //     <>
  //       <RStatusCard name="Visibility">
  //         <p className="description">
  //           Set conditions for when the coponent should be visible
  //         </p>
  //         <RTransButton label="Add Condition" />
  //       </RStatusCard>
  //     </>
  //   ),
  //   []
  // );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else
      return (
        <Visability
          componentDetail={componentDetail}
          updateComponent={updateComponent}
          options={columns}></Visability>
      );
  };
  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default LinkCard;
