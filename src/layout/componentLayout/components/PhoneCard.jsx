import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS } from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";
import RTransButton from "widgets/RTransButton";
import RSelect from "widgets/RSelect";
import ROption from "widgets/ROption";
import Icon from "widgets/Icon";
import Visability from "./Visability";

import "./_style.css";

// const options = [
//   {
//     icon: 'text',
//     label: 'Title',
//   },
//   {
//     icon: 'no_select',
//     label: '',
//   },
// ];

// const conditionalOptions = [
//   {
//     icon: 'text',
//     label: 'Name',
//   },
//   {
//     icon: 'phone',
//     label: 'Phone',
//   },
//   {
//     icon: 'mail',
//     label: 'Email',
//   },
// ];

// const questionOptions = [
//   {
//     icon: '',
//     label: 'Is',
//   },
//   {
//     icon: '',
//     label: 'Is not',
//   },
//   {
//     icon: '',
//     label: 'Is empty',
//   },
//   {
//     icon: '',
//     label: 'Is not empty',
//   },
//   {
//     icon: '',
//     label: 'Contains',
//   },
//   {
//     icon: '',
//     label: "Doesn't contain",
//   },
// ];

// const answerOptions = [
//   {
//     icon: '',
//     label: 'Enter value',
//   },
//   {
//     icon: 'text',
//     label: 'Name',
//   },
//   {
//     icon: 'phone',
//     label: 'Phone',
//   },
// ];

const PhoneCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(1);
  const [options, setOptions] = useState(null);
  const [title, setTitle] = useState("");
  // const [selectedOption, updateSelection] = useState("name");
  // const [selectedOption1, updateSelection1] = useState("email");
  const [selectedOption2, updateSelection2] = useState("name");
  const [selectedQuestion, updateQuestion] = useState("name");
  const [selectedAnswer, updateAnswer] = useState("name");
  const componentDetail = componentDetails[componentIndex];

  useEffect(() => {
    const item = items[itemIndex];
    setOptions(
      columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [columns]);

  useEffect(() => {
    setTitle(componentDetail.title);
  }, [componentDetail.title]);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const onKeyDownHandler = useCallback(
    (e) => {
      if (e.keyCode === 13) updateComponent("title", title);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [title]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          <RSelect
            label="Column"
            options={options}
            value={componentDetail.column}
            onChange={(value) => updateComponent("column", value)}
          />

          <RSelect
            label="Message Body"
            options={options}
            value={componentDetail.body}
            onChange={(value) => updateComponent("body", value)}
          />
        </RStatusCard>

        <RStatusCard name="Design">
          <RInputForm label="title">
            <input
              type="text"
              placeholder="Phone"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              onKeyDown={(e) => onKeyDownHandler(e)}
            />
          </RInputForm>

          <ROption label="Allow text messages" value={true} readOnly />
        </RStatusCard>
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [options, componentDetail, title]
  );

  // const renderFeaturesBody = useCallback(
  //   () => (
  //     <>
  //       <RStatusCard name="Visibility">
  //         <p className="description">Set conditions for when the coponent should be visible</p>

  //         <div className="my-4">
  //           <button className="sort-button py-1 px-3">Sort by sheet order backwards</button>
  //         </div>

  //         <div className="conditional-box mb-2">
  //           <div className="conditional-body">
  //             <RSelect
  //               options={options}
  //               value={selectedOption2}
  //               changeValue={(value) => updateSelection2(value)}
  //             />
  //             <div className="d-flex justify-content-between mt-2">
  //               <RSelect
  //                 options={options}
  //                 value={selectedQuestion}
  //                 changeValue={(value) => updateQuestion(value)}
  //               />

  //               <RSelect
  //                 options={options}
  //                 value={selectedAnswer}
  //                 changeValue={(value) => updateAnswer(value)}
  //               />
  //             </div>
  //           </div>
  //           <div className="conditional-action">
  //             <Icon name="minus" />{" "}
  //           </div>
  //         </div>

  //         <div className="property-tab-header">
  //           <ul className="w-100">
  //             <li className="d-flex justify-content-center align-items-center active">AND</li>
  //             <li className="d-flex justify-content-center align-items-center">OR</li>
  //           </ul>
  //         </div>

  //         <div className="conditional-box mb-2">
  //           <div className="conditional-body">
  //             <RSelect
  //               options={options}
  //               value={selectedOption2}
  //               changeValue={(value) => updateSelection2(value)}
  //             />
  //             <div className="d-flex justify-content-between mt-2">
  //               <RSelect
  //                 options={options}
  //                 value={selectedQuestion}
  //                 changeValue={(value) => updateQuestion(value)}
  //               />

  //               <RSelect
  //                 options={options}
  //                 value={selectedAnswer}
  //                 changeValue={(value) => updateAnswer(value)}
  //               />
  //             </div>
  //           </div>
  //           <div className="conditional-action">
  //             <Icon name="minus" />{" "}
  //           </div>
  //         </div>

  //         <RTransButton label="Add Condition" />
  //       </RStatusCard>
  //     </>
  //   ),
  //   [options, selectedAnswer, selectedOption2, selectedQuestion]
  // );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else
      return (
        <Visability
          componentDetail={componentDetail}
          updateComponent={updateComponent}
          options={columns}></Visability>
      );
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default PhoneCard;
