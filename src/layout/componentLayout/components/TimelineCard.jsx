import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS, TIMELINE_CONTROLS } from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";
import RSVGButton from "widgets/RSVGButton";
import RSelect from "widgets/RSelect";
import Visability from "./Visability";

import "./_style.css";

const TimelineCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  const componentDetail = componentDetails[componentIndex];

  console.log("Component Detail", componentDetail);

  useEffect(() => {
    const item = items[itemIndex];
    setOptions(
      columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [columns]);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          <RSelect
            label="Column"
            options={options}
            value={componentDetail.column}
            onChange={(value) => updateComponent("column", value)}
          />
        </RStatusCard>

        <RStatusCard name="Design">
          <RInputForm label="Title">
            <input
              type="text"
              placeholder="Text"
              value={componentDetail.title}
              onChange={(e) => updateComponent("title", e.target.value)}
            />
          </RInputForm>

          <RInputForm label="Type">
            {TIMELINE_CONTROLS.map((control, index) => (
              <RSVGButton
                key={`timeline-control-${index}`}
                icon={control.icon}
                active={index === componentDetail.type}
                onClick={() => updateComponent("type", index)}
              />
            ))}
          </RInputForm>
        </RStatusCard>
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [options, componentDetail]
  );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else
      return (
        <Visability
          componentDetail={componentDetail}
          updateComponent={updateComponent}
          options={columns}></Visability>
      );
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default TimelineCard;
