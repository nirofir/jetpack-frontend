import React, { useState, useCallback, useEffect } from "react";

import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";
import RTransButton from "widgets/RTransButton";
import RSelect from "widgets/RSelect";
import RButton from "widgets/RButton";
import { FilterComponent, SortComponent } from "../../propertyLayout/components";
import operators from "../../../constants/filterBy";

const Visability = (props) => {
  const { componentDetail, options } = props;
  const { updateComponent } = props;

  const handleAddFilter = (value) => {
    const operatorsList = operators[value.column] || operators.basic;
    value.operator = operatorsList.filter((opr) => opr.id === value.operator)[0];
    updateComponent("viz_filter_by", value);
  };
  return (
    <>
      <RStatusCard name="Visibility">
        <p className="description">Set conditions for when the coponent should be visible</p>
        <FilterComponent
          options={options && options.slice(1)}
          chooseFilter={(x) => handleAddFilter(x)}
          operators={operators}
          itsVisComponent={true}></FilterComponent>
      </RStatusCard>
      {!componentDetail.viz_filter_by || componentDetail.viz_filter_by === "none" || (
        <RStatusCard name="filter list">
          {/* {logicalFilterList.map((filter) =>  */}
          <div style={{ display: "flex", alignItems: "baseline", justifyContent: "space-between" }}>
            <RButton
              label={
                "Field " +
                componentDetail.viz_filter_by.column +
                " " +
                componentDetail.viz_filter_by.operator.label.toLowerCase() +
                " " +
                componentDetail.viz_filter_by.filterBy
              }
              gray
            />

            <div
              style={{ color: "#009aff", fontWeight: "bold", textAlign: "center", cursor: "pointer" }}
              onClick={() => updateComponent("viz_filter_by", "none")}>
              X
            </div>
          </div>
          {/* )} */}
        </RStatusCard>
      )}
    </>
  );
};

export default Visability;
