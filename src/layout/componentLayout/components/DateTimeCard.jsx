import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS } from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";
import RSelect from "widgets/RSelect";
import Visability from "./Visability";

import "./_style.css";

const FORMAT_LIST = [
  {
    id: "DD/MM/YYYY",
    icon: "",
    label: "DD/MM/YYYY",
  },
  {
    id: "DD/MM",
    icon: "",
    label: "DD/MM",
  },
  {
    id: "MM/DD",
    icon: "",
    label: "MM/DD",
  },
  {
    id: "MM",
    icon: "",
    label: "MM",
  },
  {
    id: "YYYY",
    icon: "",
    label: "YYYY",
  },
  {
    id: "DD-MMM-YYYY",
    icon: "",
    label: "DD-MMM-YYYY",
  },
];

const DateTimeCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  const componentDetail = componentDetails[componentIndex];

  console.log("Component Detail", componentDetail);

  useEffect(() => {
    const item = items[itemIndex];
    setOptions(
      columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [columns]);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          <RSelect
            label="Column"
            options={options}
            value={componentDetail.column}
            onChange={(value) => updateComponent("column", value)}
            inputType={"datey"}
          />
        </RStatusCard>

        <RStatusCard>
          <RInputForm label="title">
            <input
              type="text"
              placeholder="Title"
              value={componentDetail.title}
              onChange={(e) => updateComponent("title", e.target.value)}
            />
          </RInputForm>

          <RSelect
            label="Format"
            options={FORMAT_LIST}
            value={componentDetail.format}
            onChange={(value) => updateComponent("format", value)}
          />
        </RStatusCard>
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [options, componentDetail]
  );

  // const renderFeaturesBody = useCallback(
  //     () => (
  //         <RStatusCard name="Visibility">
  //             <p className="description">
  //                 Set conditions for when the coponent should be visible
  //     </p>
  //             <RTransButton label="Add Condition" />
  //         </RStatusCard>
  //     ),
  //     []
  // );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else
      return (
        <Visability
          componentDetail={componentDetail}
          updateComponent={updateComponent}
          options={columns}></Visability>
      );
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default DateTimeCard;
