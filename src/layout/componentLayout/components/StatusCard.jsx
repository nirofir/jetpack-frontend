import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS, SHAPE_LIST } from "constants/constants";
import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";
import RSelect from "widgets/RSelect";
import RSVGButton from "widgets/RSVGButton";
import Visability from "./Visability";

import "./_style.css";

const StatusCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  const componentDetail = componentDetails[componentIndex];

  console.log("Component Detail", componentDetail);

  useEffect(() => {
    const item = items[itemIndex];
    setOptions(
      columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [columns]);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          <RSelect
            label="Column"
            options={options}
            value={componentDetail.column}
            onChange={(value) => updateComponent("column", value)}
            inputType={"statusy"}
          />
        </RStatusCard>

        <RStatusCard name="Design">
          <RInputForm label="Title">
            <input
              type="text"
              placeholder="Text"
              value={componentDetail.title}
              onChange={(e) => updateComponent("title", e.target.value)}
            />
          </RInputForm>

          <RInputForm label="Shape">
            {SHAPE_LIST.map((shape, index) => (
              <RSVGButton
                key={`shape-control-${index}`}
                icon={shape.icon}
                active={componentDetail.shape === index}
                onClick={() => updateComponent("shape", index)}
              />
            ))}
          </RInputForm>

          {/* <RInputForm label="Color">
            {CIRCLE_COLORS.map((color, index) => (
              <RSVGButton 
              key={`status-color-${index}`}
              style={{ background: color }}
              active={componentDetail.icon === index}
              />
            ))}
          </RInputForm> */}
        </RStatusCard>
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [options, componentDetail]
  );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else
      return (
        <Visability
          componentDetail={componentDetail}
          updateComponent={updateComponent}
          options={columns}></Visability>
      );
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default StatusCard;
