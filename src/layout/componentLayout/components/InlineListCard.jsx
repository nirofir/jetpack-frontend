import React, { useState, useCallback, useEffect } from "react";
import styled from "styled-components";

import { TEXT_MENUS, INLINE_STYLES, INLINE_ACTIONS_LIST } from "constants/constants";
import { connect } from "react-redux";
import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";

import RTransButton from "widgets/RTransButton";
import RSelect from "widgets/RSelect";
import ROption from "widgets/ROption";
import Icon from "widgets/Icon";

const StyleEditorBox = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 25% [col-start]);
`;

const StyleEditorItem = styled.div`
  height: 60px;
  min-width: 60px;

  width: auto;

  font-weight: normal;
  font-size: 14px;
  line-height: 19px;

  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;

  justify-content: space-between;

  padding: 10px;

  cursor: pointer;

  color: #333333;

  :hover,
  &.active {
    background: #3ad6f514;
    border-radius: 5px;

    color: #009aff;
  }
  :hover svg path,
  &.active svg path {
    fill: #009aff;
  }
`;

const StyleEditorItemIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  height: 100%;
`;

const StyleEditorItemLabel = styled.div`
  display: flex;
  justify-content: center;
`;

const InlineListCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  const componentDetail = componentDetails[componentIndex];

  useEffect(() => {
    const item = items[itemIndex];
    setOptions(
      columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [columns]);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        {console.log(updateComponent)}
        <RStatusCard>
          <RInputForm label="Title">
            <input type="text" placeholder="Contacts" />
          </RInputForm>
        </RStatusCard>
        <RStatusCard name="style">
          <StyleEditorBox>
            {INLINE_STYLES.map((widget, index) => (
              <StyleEditorItem
                key={`style-editor-${index}`}
                className={`${componentDetail.style === index ? "active" : ""}`}
                onClick={() => updateComponent("style", index)}>
                <StyleEditorItemIcon>
                  <Icon name={widget.icon} />
                </StyleEditorItemIcon>
                <StyleEditorItemLabel>{widget.name}</StyleEditorItemLabel>
              </StyleEditorItem>
            ))}
          </StyleEditorBox>
        </RStatusCard>
        <RStatusCard name="Sources">
          <RSelect
            label="Values"
            options={options}
            value={componentDetail.values}
            onChange={(value) => updateComponent("values", value)}
          />
        </RStatusCard>
        <RStatusCard name="data">
          <RSelect
            label="Title"
            options={options}
            value={componentDetail.title}
            onChange={(value) => updateComponent("title", value)}
          />

          <RSelect
            label="Details"
            options={options}
            value={componentDetail.details}
            onChange={(value) => updateComponent("details", value)}
          />

          <RSelect
            label="Caption"
            options={options}
            value={componentDetail.caption}
            onChange={(value) => updateComponent("caption", value)}
          />

          <RSelect
            label="Image"
            options={options}
            value={componentDetail.image}
            onChange={(value) => updateComponent("image", value)}
          />
        </RStatusCard>
        {/* {/* <RStatusCard name="Options"> */}
        <ROption
          value={componentDetail.show_few}
          onChange={(value) => updateComponent("show_few", value)}
          label="Only show a few items"
        />
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [options, componentDetail]
  );

  const renderFeaturesBody = () => (
    <>
      <RStatusCard name="Search">
        <ROption
          label="Show search bar"
          value={componentDetail.show_search_bar}
          //onChange={(value) => updateComponent("show_search_bar", value)
          onChange={(value) => updateComponent("show_search_bar", value)}
        />
      </RStatusCard>

      <RStatusCard name="Filter">
        <p className="description">Limit the items displayed based on their properties.</p>

        <RTransButton label="Add Filter" />
      </RStatusCard>

      <RStatusCard name="Sort">
        <RTransButton label="Add Sort" />
      </RStatusCard>

      <RStatusCard name="Visibility">
        <p className="description">Set conditions for when the coponent should be visible</p>
        <RTransButton label="Add Condition" />
      </RStatusCard>

      <RStatusCard name="Action">
        <RSelect
          options={INLINE_ACTIONS_LIST}
          value={componentDetail.action}
          onChange={(value) => updateComponent("action", value)}
        />
      </RStatusCard>
    </>
  );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else return renderFeaturesBody();
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

const mapDispatchToProps = (dispatch) => ({
  // publishApp: () => dispatch(publishApp())
});

const mapStateToProps = (state) => ({
  hasSearchBar: state.StyleReducer.tabs[state.StyleReducer.currentTab].property,
});

export default connect(mapStateToProps, mapDispatchToProps)(InlineListCard);
