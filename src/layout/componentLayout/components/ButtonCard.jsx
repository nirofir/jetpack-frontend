import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS, TEXT_DECORATIONS, ACTIONS_LIST } from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";
import RSVGButton from "widgets/RSVGButton";
import RSelect from "widgets/RSelect";
import Visability from "./Visability";

import "./_style.css";

const ButtonCard = React.memo((props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent, appTabs } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  const componentDetail = componentDetails[componentIndex];

  useEffect(() => {
    const item = items[itemIndex];
    setOptions(
      columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [columns]);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          <RInputForm label="title">
            <input
              type="text"
              placeholder="Near"
              value={componentDetail.title}
              onChange={(e) => updateComponent("title", e.target.value)}
            />
          </RInputForm>

          <RInputForm label="Show as">
            {TEXT_DECORATIONS.map((text_decoration, index) => (
              <RSVGButton
                key={`text-decoration-button${index}`}
                icon={text_decoration.icon}
                active={componentDetail.show_as === index}
                onClick={() => updateComponent("show_as", index)}
              />
            ))}
          </RInputForm>
        </RStatusCard>
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [componentDetail]
  );

  const renderFeaturesBody = () => (
    <>
      <Visability
        updateComponent={updateComponent}
        options={columns}
        componentDetail={componentDetail}></Visability>

      <RStatusCard name="Action">
        <RSelect
          options={ACTIONS_LIST}
          value={componentDetail.action}
          onChange={(value) => updateComponent("action", value)}
        />
        <RSelect
          label={componentDetail.action === "link_screen" ? "Tab" : componentDetail.data}
          options={componentDetail.action === "link_screen" ? appTabs : options}
          value={componentDetail.data}
          onChange={(value) => updateComponent("data", value)}
        />
      </RStatusCard>
    </>
  );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else return renderFeaturesBody();
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
});

export default ButtonCard;
