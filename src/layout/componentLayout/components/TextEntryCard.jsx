import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS } from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RInputForm from "widgets/RInputForm";
import RSelect from "widgets/RSelect";
import Visability from "./Visability";

import "./_style.css";

const TextEntryCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  const [title, setTitle] = useState("");
  const [hint, setHint] = useState("");
  // const [selectedOption, updateSelection] = useState("email");
  const componentDetail = componentDetails[componentIndex];

  console.log("Component Detail", componentDetail);

  useEffect(() => {
    const item = items[itemIndex];
    setOptions(
      columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [columns]);

  useEffect(() => {
    setTitle(componentDetail.title);
    setHint(componentDetail.hint_text);
  }, [componentDetail.title, componentDetail.hint_text]);

  const onKeyDownHandler = useCallback(
    (e, type) => {
      if (e.keyCode === 13) {
        if (type === "title") updateComponent("title", title);
        else if (type === "hint") updateComponent("hint_text", hint);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [title, hint]
  );

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          <RSelect
            label="Column"
            options={options}
            value={componentDetail.column}
            onChange={(value) => updateComponent("column", value)}
            inputType={"stringy"}
          />
        </RStatusCard>

        <RStatusCard>
          <RInputForm label="title">
            <input
              type="text"
              placeholder="Number"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              onKeyDown={(e) => onKeyDownHandler(e, "title")}
            />
          </RInputForm>

          <RInputForm label="Hint text">
            <input
              type="text"
              placeholder="Placeholder"
              value={hint}
              onChange={(e) => setHint(e.target.value)}
              onKeyDown={(e) => onKeyDownHandler(e, "hint")}
            />
          </RInputForm>
        </RStatusCard>
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [options, componentDetail, title, hint]
  );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else
      return (
        <Visability
          componentDetail={componentDetail}
          updateComponent={updateComponent}
          options={columns}></Visability>
      );
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default TextEntryCard;
