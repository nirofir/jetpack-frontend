import React, { useState, useCallback, useEffect } from "react";

import { TEXT_MENUS, MAP_ZOOM_LIST } from "constants/constants";

import RStatusCard from "widgets/RStatusCard";
import RSelect from "widgets/RSelect";
import Visability from "./Visability";

import "./_style.css";

const MapCard = (props) => {
  const { columns, items, itemIndex, componentIndex, componentDetails, updateComponent } = props;
  const [tab, setTab] = useState(0);
  const [options, setOptions] = useState(null);
  // const [selectedOption, updateSelection] = useState("name");
  const componentDetail = componentDetails[componentIndex];

  useEffect(() => {
    const item = items[itemIndex];
    setOptions(
      columns.filter((column) => {
        if (item[column.id] && typeof item[column.id].text === "string") return true;
        return false;
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderHeader = useCallback(
    () => (
      <div className="property-tab-header">
        <ul className="w-100">
          {TEXT_MENUS.map((menu, index) => (
            <li
              key={`component-header-${index}`}
              className={`d-flex justify-content-center align-items-center ${
                tab === index ? "active" : ""
              }`}
              onClick={() => setTab(index)}>
              {menu}
            </li>
          ))}
        </ul>
      </div>
    ),
    [setTab, tab]
  );

  const renderLayoutBody = useCallback(
    () => (
      <>
        <RStatusCard name="data">
          <RSelect
            label="Address"
            options={options}
            value={componentDetail.address}
            onChange={(value) => updateComponent("address", value)}
            inputType={"addressy"}
          />
        </RStatusCard>

        <RStatusCard name="Design">
          <RSelect
            label="Zoom"
            options={MAP_ZOOM_LIST}
            value={componentDetail.zoom}
            onChange={(value) => updateComponent("zoom", value)}
          />
        </RStatusCard>
      </>
    ),
    [componentDetail, options, updateComponent]
  );

  const renderBody = () => {
    if (tab === 0) return renderLayoutBody();
    else
      return (
        <Visability
          componentDetail={componentDetail}
          updateComponent={updateComponent}
          options={columns}></Visability>
      );
  };

  return (
    <>
      {renderHeader()}
      {renderBody()}
    </>
  );
};

export default MapCard;
