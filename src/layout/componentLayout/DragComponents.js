import React, { useCallback } from "react";
import styled from "styled-components";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

import RDragBox from "widgets/RDragBox";

const DragComponentItem = styled.div`
  user-select: none;
  margin: 0 0 8px 0;
`;

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const getItemStyle = (isDragging) => ({
  boxShadow: isDragging ? `0px 5px 5px rgba(0, 0, 0, 0.25)` : "",
});

const DragComponents = React.memo((props) => {
  const { componentData, componentDetails, columnsData } = props;
  const {
    changeLayoutPage,
    selectComponent,
    reorderComponents,
    removeComponent,
    duplicateComponent,
  } = props;

  const onDragEnd = useCallback(
    (result) => {
      if (!result.destination) return;

      const newComponentData = reorder(componentData, result.source.index, result.destination.index);
      const newComponentDetails = reorder(componentDetails, result.source.index, result.destination.index);

      reorderComponents(newComponentData, newComponentDetails);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [componentData, componentDetails]
  );

  const onSelectItem = useCallback((index) => {
    console.log("Select Drag Component: ", index);
    selectComponent(index);
    changeLayoutPage("edit_component");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onCloseItem = useCallback((index) => {
    console.log("Removing Drag Component", index);
    removeComponent(index);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onDuplicateItem = useCallback((index) => {
    console.log("Duplicating Drag component", index);
    duplicateComponent(index);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  console.log(componentData)
  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="droppable">
        {(provided, snapshot) => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            {componentData &&
              componentData.map((item, index) => {
                console.log(item)
                return (

                  <Draggable key={item.id} draggableId={item.id} index={index}>
                    {(provided, snapshot) => (
                      <div>
                        <DragComponentItem
                          {...provided.dragHandleProps}
                          {...provided.draggableProps}
                          ref={provided.innerRef}
                          style={provided.draggableProps.style}>
                          <RDragBox
                            {...item}
                            {...provided.dragHandleProps}
                            {...provided.draggableProps}
                            //label={columnsData[item.label.toLowerCase()]}
                            label={columnsData[item.label ? item.label.toLowerCase() : item.name]}

                            style={getItemStyle(snapshot.isDragging)}
                            onSelect={() => onSelectItem(index)}
                            onClose={() => onCloseItem(index)}
                            onDuplicate={() => onDuplicateItem(index)}
                          />
                        </DragComponentItem>
                      </div>
                    )}
                  </Draggable>
                )
              })}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
});

export default DragComponents;
