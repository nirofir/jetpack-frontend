import React, { useState, useCallback } from "react";
import styled from "styled-components";
import { connect } from "react-redux";

import ActionTextCard from "./components/ActionTextCard";
import TextCard from "./components/TextCard";
import SeparatorCard from "./components/SeparatorCard";
import TitleCard from "./components/TitleCard";
import ImageCard from "./components/ImageCard";
import MapCard from "./components/MapCard";
import ButtonCard from "./components/ButtonCard";
import LinkCard from "./components/LinkCard";
import PhoneCard from "./components/PhoneCard";
import InlineListCard from "./components/InlineListCard";
import NoteCard from "./components/NoteCard";
import NumberEntryCard from "./components/NumberEntryCard";
import TextEntryCard from "./components/TextEntryCard";
import CommentCard from "./components/CommentCard";
import DateCard from "./components/DateCard";
import DateTimeCard from "./components/DateTimeCard";
import ItemIDCard from "./components/ItemIDCard";
import PeopleCard from "./components/PeopleCard";
import TimelineCard from "./components/TimelineCard";
import ProgressCard from "./components/ProgressCard";
import TagsCard from "./components/TagsCard";
import WorldClockCard from "./components/WorldClockCard";
import VoteCard from "./components/VoteCard";
import StatusCard from "./components/StatusCard";
import RatingCard from "./components/RatingCard";
import CreationLogCard from "./components/CreationLogCard";
import LastUpdateCard from "./components/LastUpdateCard";
import VideoCard from "./components/VideoCard"

import Icon from "widgets/Icon";
import RStatusCard from "widgets/RStatusCard";

import { COMPONENT_STYLES, COMPONENT_LIST_STYLES } from "constants/constants";
import { addComponent, updateComponent, removeComponent } from "redux/style";
import {
  addDetailComponent as addComponentDetail,
  updateDetailComponent as updateComponentDetail,
  removeDetailComponent as removeComponentDetail,
} from "redux/style";
import { goBackLayoutPage } from "redux/layout";

const ComponentLayoutHeader = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 22px;

  display: flex;
  align-items: center;
  justify-content: center;

  position: relative;

  padding-top: 22px;
  padding-bottom: 22px;

  color: #303655;

  border-bottom: 1px solid #dfdfdf;

  & > svg path {
    fill: #009aff;
  }
`;

const LeftControl = styled.span`
  & > svg {
    position: absolute;
    left: 35px;
    top: 50%;
    transform: translateY(-50%);

    cursor: pointer;
  }
`;

const RightControl = styled.span`
  & > svg {
    width: 20px;
    height: 20px;
  }

  & > svg {
    position: absolute;
    right: 35px;
    top: 50%;
    transform: translateY(-50%);

    cursor: pointer;
  }
`;

const ComponentGridList = styled.div`
  margin-top: 10px;
  padding: 0;
  display: grid;
  grid-template-columns: repeat(4, 25% [col-start]);

  & button {
    background: #313139;
    border-radius: 10px;
    border: none;
    outline: none;

    height: 48px;
    width: 48px;
  }
`;

const TextButtonGroup = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  font-family: Open Sans;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;
  text-align: center;

  color: #313139;

  margin-bottom: 10px;
`;

const ViewTab = styled.div`
  padding-top: 17px;
  padding-bottom: 17px;
  margin: 0px;

  width: 300px;

  display: flex;
  margin: auto;

  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 16px;
  text-transform: uppercase;

  color: #333333;
`;

const ViewTabBar = styled.ul`
  display: flex;
  justify-content: space-between;

  height: 30px;
  width: 100%;

  padding: 0;
  margin: 0;

  background: #ededf3;
  border-radius: 30px;
`;

const ViewTabItem = styled.li`
  width: 100%;
  cursor: pointer;

  display: flex;
  justify-content: center;
  align-items: center;

  text-transform: uppercase;

  &.active {
    background: #ffffff;
    box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.19);
    border-radius: 26px;
    margin: 3px;
    width: calc(100% - 6px);
  }
`;

const ListItem = styled.div`
  padding: 6px 9px 6px 9px;
  margin: 18px 0px 18px 0px;

  font-family: Open Sans;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;

  color: #737383;

  background: #ffffff;

  border: 1px solid #bdbdbd;
  box-sizing: border-box;
  border-radius: 28px;

  cursor: pointer;

  display: grid;
  grid-template-columns: [first] 10% [line2] 90%;

  & svg {
    width: 17px;
    height: 17px;
  }

  & svg path {
    fill: black;
  }
`;
const ListItemIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
const ListItemDesc = styled.div`
  display: flex;
  align-items: center;
`;

const NewComponentLayout = React.memo((props) => {
  // variables
  const {
    mondayData,
    mobileStyle,
    appTabs,

    componentDetails,
    componentIndex,

    detailComponentDetails,
    detailComponentIndex,
  } = props;
  // functions
  const {
    goBackLayoutPage,

    addComponent,
    removeComponent,
    updateComponent,

    addComponentDetail,
    updateComponentDetail,
    removeComponentDetail,
  } = props;

  const [tab, setTab] = useState(null);
  const [isGridView, setView] = useState(true);

  const onClickTabItem = useCallback((label) => {
    console.log(label)
    mobileStyle === "details" ? addComponentDetail(label) : addComponent(label);
    setTab(label);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderStyleButtons = useCallback(() => {
    return COMPONENT_STYLES.map((component, index) => (
      <RStatusCard name={component.label} key={`component-card-${index}`}>
        <ComponentGridList>
          {component.items &&
            component.items.map((item, id) => {
              let goodButtons = <button style={{ cursor: 'pointer' }} onClick={() => onClickTabItem(item.label)}>
                <Icon name={item.icon} />
              </button>
              return (
                < TextButtonGroup key={`component-button-${id}`}>

                  {goodButtons}
                  <label>{item.label}</label>

                </TextButtonGroup>
              )
            })}
        </ComponentGridList>
      </RStatusCard >
    ));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderStyleLists = useCallback(() => {
    return COMPONENT_LIST_STYLES.map((component, index) => (
      <RStatusCard name={component.label} key={`component-card-${index}`}>
        {component.items &&
          component.items.map((item, idx) => (
            <ListItem key={`component-${index}-${idx}`} onClick={() => onClickTabItem(item.label)}>
              <ListItemIcon>
                <Icon name={item.icon} />
              </ListItemIcon>
              <ListItemDesc>{item.text}</ListItemDesc>
            </ListItem>
          ))}
      </RStatusCard>
    ));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderTabView = useCallback(() => {
    return (
      <>
        <ViewTabItem className={isGridView && "active"} onClick={() => setView(true)}>
          Grid
        </ViewTabItem>
        <ViewTabItem className={isGridView || "active"} onClick={() => setView(false)}>
          List
        </ViewTabItem>
      </>
    );
  }, [isGridView]);

  const renderBody = useCallback(() => {
    if (!tab) {
      if (isGridView) return renderStyleButtons();
      else return renderStyleLists();
    }
    const index = mobileStyle === "details" ? detailComponentIndex : (componentIndex);

    const details = mobileStyle === "details" ? detailComponentDetails : componentDetails;
    const update = mobileStyle === "details" ? updateComponentDetail : updateComponent;
    switch (tab) {
      case "Video":
        return (<VideoCard
          {...mondayData}
          componentIndex={index}
          componentDetails={details}
          updateComponent={update}
          appTabs={appTabs}>

        </VideoCard>)

      case "Action Text":
        return (
          <ActionTextCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
            appTabs={appTabs}
          />
        );
      case "Text":
        return (
          <TextCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Separator":
        return <SeparatorCard
          {...mondayData}
          componentIndex={index}
          componentDetails={details}
          updateComponent={update} />;
      case "Title":
        return (
          <TitleCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Image":
        return (
          <ImageCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
            appTabs={appTabs}
          />
        );
      case "Map":
        return (
          <MapCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Button":
        return (
          <ButtonCard

            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
            appTabs={appTabs}
          />
        );
      case "Link":
        return (
          <LinkCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Phone":
        return (
          <PhoneCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Inline List":
        return (
          <InlineListCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Notes":
        return (
          <NoteCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Number Entry":
        return (
          <NumberEntryCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Text Entry":
        return (
          <TextEntryCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Comments":
        return (
          <CommentCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Date":
        return (
          <DateCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Date Time":
        return (
          <DateTimeCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Item ID":
        return (
          <ItemIDCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "People":
        return (
          <PeopleCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Timeline":
        return (
          <TimelineCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Tags":
        return (
          <TagsCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Progress Bar":
        return (
          <ProgressCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "World Clock":
        return (
          <WorldClockCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Vote":
        return (
          <VoteCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Status":
        return (
          <StatusCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Rating":
        return (
          <RatingCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Creation Log":
        return (
          <CreationLogCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Last Updated":
        return (
          <LastUpdateCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      default:
        return "";
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    tab,
    isGridView,
    mobileStyle,
    componentIndex,
    componentDetails,
    detailComponentIndex,
    detailComponentDetails,
  ]);

  const goBack = useCallback(() => {
    if (!tab) goBackLayoutPage();
    setTab(null);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tab]);

  const removeCurrentComponent = useCallback(() => {
    console.log("ComponentIndex: ", componentIndex);
    mobileStyle === "details"
      ? removeComponent(componentIndex)
      : removeComponentDetail(detailComponentIndex);
    goBackLayoutPage();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mobileStyle, componentIndex, detailComponentIndex]);

  return (
    <>
      <ComponentLayoutHeader>
        <LeftControl onClick={() => goBack()}>
          <Icon name="back" />
        </LeftControl>

        {!tab ? "Components" : tab}

        {tab ? (
          <RightControl onClick={() => removeCurrentComponent()}>
            <Icon name="trash" />
          </RightControl>
        ) : (
            ""
          )}
      </ComponentLayoutHeader>
      {!tab ? (
        <ViewTab>
          <ViewTabBar>{renderTabView()}</ViewTabBar>
        </ViewTab>
      ) : (
          ""
        )}
      {renderBody()}
    </>
  );
});

const mapStateToProps = (state) => {


  return {
    mondayData: state.MondayReducer,

    appTabs: state.StyleReducer.tabs.map((tab, index) => {
      return { id: tab.title, icon: tab.icon, label: tab.title, name: tab.title, index }
    }),

    mobileStyle: state.StyleReducer.tabs[state.StyleReducer.currentTab].property.style,
    // componentData: state.StyleReducer.components,
    componentDetails: state.StyleReducer.tabs[state.StyleReducer.currentTab].componentDetails,

    componentIndex: state.StyleReducer.tabs[state.StyleReducer.currentTab].componentIndex,
    // componentDetailData: state.DetailReducer.components,
    detailComponentDetails:
      state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle.componentDetails,
    detailComponentIndex:
      state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle.componentIndex,
    //state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle.componentIndex,
  }


}
  ;

const mapDispatchToProps = (dispatch) => ({
  goBackLayoutPage: () => dispatch(goBackLayoutPage()),

  addComponent: (label) => dispatch(addComponent(label)),
  updateComponent: (field, value) => dispatch(updateComponent(field, value)),
  removeComponent: (index) => dispatch(removeComponent(index)),

  addComponentDetail: (label) => dispatch(addComponentDetail(label)),

  updateComponentDetail: (field, value) => dispatch(updateComponentDetail(field, value)),
  removeComponentDetail: (index) => dispatch(removeComponentDetail(index)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewComponentLayout);
