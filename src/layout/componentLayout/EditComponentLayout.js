import React, { useCallback, useEffect } from "react";
import styled from "styled-components";
import { connect } from "react-redux";

import ActionTextCard from "./components/ActionTextCard";
import TextCard from "./components/TextCard";
import SeparatorCard from "./components/SeparatorCard";
import TitleCard from "./components/TitleCard";
import ImageCard from "./components/ImageCard";
import MapCard from "./components/MapCard";
import ButtonCard from "./components/ButtonCard";
import LinkCard from "./components/LinkCard";
import PhoneCard from "./components/PhoneCard";
import InlineListCard from "./components/InlineListCard";
import NoteCard from "./components/NoteCard";
import NumberEntryCard from "./components/NumberEntryCard";
import TextEntryCard from "./components/TextEntryCard";
import CommentCard from "./components/CommentCard";
import DateCard from "./components/DateCard";
import ItemIDCard from "./components/ItemIDCard";
import PeopleCard from "./components/PeopleCard";
import TimelineCard from "./components/TimelineCard";
import ProgressCard from "./components/ProgressCard";
import TagsCard from "./components/TagsCard";
import WorldClockCard from "./components/WorldClockCard";
import VoteCard from "./components/VoteCard";
import StatusCard from "./components/StatusCard";
import RatingCard from "./components/RatingCard";
import CreationLogCard from "./components/CreationLogCard";
import LastUpdateCard from "./components/LastUpdateCard";
import VideoCard from "./components/VideoCard"

import Icon from "widgets/Icon";
import { goBackLayoutPage, changeLayoutPage } from "redux/layout";
import { updateComponent, removeComponent } from "redux/style";
import {
  updateDetailComponent as updateComponentDetail,
  removeDetailComponent as removeComponentDetail,
} from "redux/style";

const ComponentLayoutHeader = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 22px;

  display: flex;
  align-items: center;
  justify-content: center;

  position: relative;

  padding-top: 22px;
  padding-bottom: 22px;

  color: #303655;

  border-bottom: 1px solid #dfdfdf;

  & > svg path {
    fill: #009aff;
  }
`;

const LeftControl = styled.span`
  & > svg {
    position: absolute;
    left: 35px;
    top: 50%;
    transform: translateY(-50%);

    cursor: pointer;
  }
`;

const RightControl = styled.span`
  & > svg {
    width: 20px;
    height: 20px;
  }

  & > svg {
    position: absolute;
    right: 35px;
    top: 50%;
    transform: translateY(-50%);

    cursor: pointer;
  }
`;

const EditComponentLayout = React.memo((props) => {
  const {
    mondayData,
    mobileStyle,

    componentData,
    componentIndex,
    componentDetails,
    appTabs,
    userId,
    detailComponentData,
    detailComponentDetails,
    detailComponentIndex,
  } = props;
  const {
    goBackLayoutPage,
    changeLayoutPage,
    updateComponent,
    removeComponent,

    updateComponentDetail,
    removeComponentDetail,
  } = props;

  const index = mobileStyle === "details" ? detailComponentIndex : componentIndex;
  const data = mobileStyle === "details" ? detailComponentData : componentData;
  const details = mobileStyle === "details" ? detailComponentDetails : componentDetails;
  const update = mobileStyle === "details" ? updateComponentDetail : updateComponent;

  const name =
    mobileStyle === "details"
      ? detailComponentData[detailComponentIndex] && detailComponentData[detailComponentIndex].name
      : componentData[componentIndex] && componentData[componentIndex].name;



  const renderBody = () => {
    console.log("Name: ", name);
    console.log("Index: ", index);
    console.log("Details: ", details);

    switch (name) {
      case "Video":
        return (<VideoCard
          {...mondayData}
          componentIndex={index}
          componentDetails={details}
          updateComponent={update}
          appTabs={appTabs}>

        </VideoCard>)
      case "Action Text":
        return (
          <ActionTextCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
            appTabs={appTabs}

          />
        );
      case "Text":
        return (
          <TextCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Separator":
        return <SeparatorCard
          {...mondayData}
          componentIndex={index}
          componentDetails={details}
          updateComponent={update} />;
      case "Title":
        return (
          <TitleCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Image":
        return (
          <ImageCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
            appTabs={appTabs}

          />
        );
      case "Map":
        return (
          <MapCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Button":
        return (
          <ButtonCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
            appTabs={appTabs}
          />
        );
      case "Link":
        return (
          <LinkCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Phone":
        return (
          <PhoneCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Inline List":
        return (
          <InlineListCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Notes":
        return (
          <NoteCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Number Entry":
        return (
          <NumberEntryCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Text Entry":
        return (
          <TextEntryCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Comments":
        return (
          <CommentCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Date":
        return (
          <DateCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Item ID":
        return (
          <ItemIDCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "People":
        return (
          <PeopleCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Timeline":
        return (
          <TimelineCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Tags":
        return (
          <TagsCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Progress Bar":
        return (
          <ProgressCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "World Clock":
        return (
          <WorldClockCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Vote":
        return (
          <VoteCard
            userId={userId}
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Status":
        return (
          <StatusCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Rating":
        return (
          <RatingCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Creation Log":
        return (
          <CreationLogCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      case "Last Updated":
        return (
          <LastUpdateCard
            {...mondayData}
            componentIndex={index}
            componentDetails={details}
            updateComponent={update}
          />
        );
      default:
        // console.log(detailComponentData,componentData,"lol")
        // if(!detailComponentData && !componentData) {
        //   console.log(detailComponentData,componentData,"lol")
        //   changeLayoutPage("property")
        // }
        return <></>;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }

  const goBack = useCallback(() => {
    goBackLayoutPage();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const removeCurrentComponent = useCallback(() => {
    console.log("ComponentIndex: ", componentIndex);
    if (mobileStyle === "details") removeComponentDetail(detailComponentIndex);
    else removeComponent(componentIndex);

    goBackLayoutPage();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [componentIndex, detailComponentIndex, mobileStyle]);

  return (
    <>
      {data[index] ? (
        <>
          <ComponentLayoutHeader>
            <LeftControl onClick={() => goBack()}>
              <Icon name="back" />
            </LeftControl>

            {data[index].name}

            <RightControl onClick={() => removeCurrentComponent()}>
              <Icon name="trash" />
            </RightControl>
          </ComponentLayoutHeader>
          {renderBody()}
        </>
      ) : (
          <></>
        )}
    </>
  );
});

const mapStateToProps = (state) => ({
  mondayData: state.MondayReducer,
  mobileStyle: state.StyleReducer.tabs[state.StyleReducer.currentTab].property.style,
  appTabs: state.StyleReducer.tabs.map((tab, index) => {
    return { id: tab.title, icon: tab.icon, label: tab.title, name: tab.title, index }
  }),


  componentData: state.StyleReducer.tabs[state.StyleReducer.currentTab].components,
  componentIndex: state.StyleReducer.tabs[state.StyleReducer.currentTab].componentIndex,
  componentDetails: state.StyleReducer.tabs[state.StyleReducer.currentTab].componentDetails,

  detailComponentData: state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle.components,
  detailComponentDetails:
    state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle.componentDetails,
  detailComponentIndex:
    state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle.componentIndex,
  userId: state.MondayReducer.userId,

});

const mapDispatchToProps = (dispatch) => ({
  goBackLayoutPage: () => dispatch(goBackLayoutPage()),
  changeLayoutPage: () => dispatch(changeLayoutPage()),

  updateComponent: (field, value) => dispatch(updateComponent(field, value)),
  removeComponent: (index) => dispatch(removeComponent(index)),

  updateComponentDetail: (field, value) => dispatch(updateComponentDetail(field, value)),
  removeComponentDetail: (index) => dispatch(removeComponentDetail(index)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditComponentLayout);
