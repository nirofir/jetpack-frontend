import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";

import Icon from "widgets/Icon";
import RStatusCard from "widgets/RStatusCard";
import RSelect from "widgets/RSelect";
import DragComponents from "./DragComponents";

import {
  updateProperty,
  selectComponent,
  reorderComponents,
  removeComponent,
  duplicateComponent,
  toggleComponentView,
} from "redux/style";

import {
  selectDetailComponent,
  reorderDetailComponents,
  removeDetailComponent,
  duplicateDetailComponent,
} from "redux/style";

import { changeLayoutPage } from "redux/layout";
import RInputForm from "widgets/RInputForm";

const ComponentLayoutHeader = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 22px;

  display: flex;
  align-items: center;
  justify-content: center;

  position: relative;

  padding-top: 22px;
  padding-bottom: 22px;

  color: #303655;

  border-bottom: 1px solid #dfdfdf;

  & > svg path {
    fill: #009aff;
  }
`;

const LeftControl = styled.span`
  & > svg {
    position: absolute;
    left: 35px;
    top: 50%;
    transform: translateY(-50%);

    cursor: pointer;
  }
`;
const RightControl = styled.span`
  & > svg {
    width: 30px;
    height: 30px;
  }

  & > svg {
    position: absolute;
    right: 35px;
    top: 50%;
    transform: translateY(-50%);

    cursor: pointer;
  }
`;

// const ComponentTab = styled.div`
//   width: 50%;

//   background: transparent;

//   display: flex;
//   align-items: center;
//   justify-content: center;

//   text-transform: uppercase;

//   cursor: pointer;

//   &.active {
//     margin: 3px;

//     background: #ffffff;
//     box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.19);
//     border-radius: 26px;
//   }
// `;

const ComponentLayout = React.memo((props) => {
  const {
    options,
    propertyData,
    componentData,
    componentDetails,
    columnsData,
    detailComponentData,
    detailComponentDetails,
  } = props;
  const {
    updateProperty,
    toggleComponentView,
    changeLayoutPage,

    selectComponent,
    reorderComponents,
    removeComponent,
    duplicateComponent,

    selectDetailComponent,
    removeDetailComponent,
    reorderDetailComponents,
    duplicateDetailComponent,
  } = props;
  return (
    <>
      <ComponentLayoutHeader>
        <LeftControl onClick={() => toggleComponentView()}>
          <Icon name="back" />
        </LeftControl>
        Components
        <RightControl onClick={() => changeLayoutPage("new_component")}>
          <Icon name="plus" />
        </RightControl>
      </ComponentLayoutHeader>

      {/* <ComponentTabHeader>
        <ComponentTab className="active">Layout</ComponentTab>
        <ComponentTab>Edit</ComponentTab>
      </ComponentTabHeader> */}

      <RStatusCard name="Title">
        {propertyData.appHeaderTitle === "custom" ? (
          <RInputForm label="Title">
            <input
              type="text"
              placeholder="Text"
              value={propertyData.appHeaderCustomTitle}
              onChange={(e) => updateProperty("appHeaderCustomTitle", e.target.value)}
            />
          </RInputForm>
        ) : (
            <RSelect
              label="Title"
              options={[{ id: "custom", label: "Custom Text", icon: "" }, ...options]}
              value={propertyData.appHeaderTitle}
              inputType={"stringy"}
              onChange={(value) => updateProperty("appHeaderTitle", value)}
            />
          )}
      </RStatusCard>

      <RStatusCard name="Components">
        {propertyData.style === "details" ? (
          <DragComponents
            columnsData={columnsData}
            changeLayoutPage={changeLayoutPage}
            componentData={detailComponentData}
            componentDetails={detailComponentDetails}
            selectComponent={selectDetailComponent}
            reorderComponents={reorderDetailComponents}
            removeComponent={removeDetailComponent}
            duplicateComponent={duplicateDetailComponent}
          />
        ) : (
            <DragComponents
              columnsData={columnsData}
              changeLayoutPage={changeLayoutPage}
              componentData={componentData}
              componentDetails={componentDetails}
              selectComponent={selectComponent}
              reorderComponents={reorderComponents}
              removeComponent={removeComponent}
              duplicateComponent={duplicateComponent}
            />
          )}
      </RStatusCard>
    </>
  );
});

const mapStateToProps = (state) => ({
  options: state.MondayReducer.columns,
  propertyData: state.StyleReducer.tabs[state.StyleReducer.currentTab].property,
  columnsData: state.MondayReducer.column_json,

  componentData: state.StyleReducer.tabs[state.StyleReducer.currentTab].components,
  componentDetails: state.StyleReducer.tabs[state.StyleReducer.currentTab].componentDetails,

  detailComponentData: state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle.components,
  detailComponentDetails:
    state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle.componentDetails,
});

const mapDispatchToProps = (dispatch) => ({
  updateProperty: (field, value) => dispatch(updateProperty(field, value)),
  toggleComponentView: () => dispatch(toggleComponentView()),
  changeLayoutPage: (value) => dispatch(changeLayoutPage(value)),

  selectComponent: (item) => dispatch(selectComponent(item)),
  removeComponent: (index) => dispatch(removeComponent(index)),
  reorderComponents: (components, details) => dispatch(reorderComponents(components, details)),
  duplicateComponent: (index) => dispatch(duplicateComponent(index)),

  selectDetailComponent: (item) => dispatch(selectDetailComponent(item)),
  removeDetailComponent: (index) => dispatch(removeDetailComponent(index)),
  reorderDetailComponents: (components, details) => dispatch(reorderDetailComponents(components, details)),
  duplicateDetailComponent: (index) => dispatch(duplicateDetailComponent(index)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ComponentLayout);
