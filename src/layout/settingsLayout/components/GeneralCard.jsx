import React from 'react';

import AppInfoCard from './GeneralComponents/AppInfoCard';
import AppTemplateCard from './GeneralComponents/AppTemplateCard';
import TabletModeCard from './GeneralComponents/TabletModeCard';
import RefreshCard from './GeneralComponents/RefreshCard';
import AnalyticsCard from './GeneralComponents/AnalyticsCard';
import ZapierCard from './GeneralComponents/ZapierCard';
import './_style.css';

const GeneralCard = () => {
  return (
    <div className="general-card">
      <AppInfoCard />
      <AppTemplateCard />
      <TabletModeCard />
      <RefreshCard />
      <AnalyticsCard />
      <ZapierCard />
    </div>
  );
};

export default GeneralCard;
