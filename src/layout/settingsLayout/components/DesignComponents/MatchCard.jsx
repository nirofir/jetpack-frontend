import React from 'react';

import './_style.css';
const MatchCard = () => {
  return (
    <div className="status-card match-card">
      <div className="option-card-item">
        <input type="checkbox" checked={true} readOnly />
        <label className="font-weight-bold">Match device’s theme</label>
      </div>
      <p>
        The app’s theme will automatically change based on the device’s current
        appearance settings.
      </p>
    </div>
  );
};

export default MatchCard;
