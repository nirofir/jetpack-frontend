import React from 'react';

import './_style.css';
const IconCard = () => {
  return (
    <div className="status-card">
      <label>Icon</label>
      <div className="d-flex">
        <div className="icon-loader">&nbsp;</div>
        <div className="icon-loader-script justify-content-between">
          <p>Choose an emoji or upload an icon. Square images work best.</p>
          <div className="d-flex justify-content-center">
            <button>Upload</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default IconCard;
