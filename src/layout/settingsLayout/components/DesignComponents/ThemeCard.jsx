import React from "react";
import { connect } from "react-redux";
import { changeColorTheme, changeColorThemeLight } from "../../../../redux/style";
import { LIGHT_THEMES, DARK_THEMES, FRAME_URL } from "constants/constants";

import "./_style.css";

const ThemeCard = (props) => {
  const { changeColorTheme, changeColorThemeLight } = props;
  return (
    <div className="status-card theme-card">
      <label>THEMES</label>
      <div className="d-flex justify-content-between mb-2">
        {LIGHT_THEMES.map((theme, index) => {
          return (
            <div className="d-flex flex-column" key={`light-${index}`}>
              <img
                src={FRAME_URL + theme.image}
                alt=""
                onClick={() => changeColorThemeLight(theme.bodyColor, theme.headColor, theme.footColor, theme.fontColorHead)}
              />
              <p className="text-center">{theme.label}</p>
            </div>
          );
        })}
      </div>
      <div className="d-flex justify-content-between">
        {DARK_THEMES.map((theme, index) => (
          <div className="d-flex flex-column" key={`dark-${index}`}>
            <img
              src={FRAME_URL + theme.image}
              alt=""
              onClick={() => changeColorTheme(theme.bodyColor, theme.headColor, theme.footColor, theme.fontColor, theme.fontColorHead)}
            />
            <p className="text-center">{theme.label}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeColorThemeLight: (colorBody, colorHead, colorFoot, colorFont, colorFontHead) =>
      dispatch(changeColorThemeLight(colorBody, colorHead, colorFoot, colorFont, colorFontHead)),
    changeColorTheme: (colorBody, colorHead, colorFoot, colorFont, colorFontHead) =>
      dispatch(changeColorTheme(colorBody, colorHead, colorFoot, colorFont, colorFontHead)),

  };

};

export default connect(null, mapDispatchToProps)(ThemeCard);
