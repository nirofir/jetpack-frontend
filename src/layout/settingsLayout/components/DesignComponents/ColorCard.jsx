import React from 'react';

import './_style.css';

const colors = [
  '#F44336',
  '#FF5722',
  '#FF9800',
  '#4CAF50',
  '#009688',
  '#00BCD4',
  '#8854D0',
  '#F62765',
  'conic-gradient(from 180deg at 50% 50%, #FF0000 0deg, #FFF500 78.75deg, #00FF19 150deg, #00D1FF 202.5deg, #001AFF 286.87deg, #DB00FF 356.25deg, #FF0000 360deg)',
];

const ColorCard = () => {
  return (
    <div className="status-card">
      <label>ACCENT COLOR</label>
      <div className="d-flex justify-content-between">
        {colors.map((color, index) => (
          <button
            key={`color+${index}`}
            className="color-btn"
            style={{ background: color }}
          ></button>
        ))}
      </div>
    </div>
  );
};

export default ColorCard;
