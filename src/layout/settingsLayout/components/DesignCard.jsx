import React, { useState } from "react";

import IconCard from "./DesignComponents/IconCard";
import { changeColor, changeColorHead, changeColorFoot, changeColorFont } from "../../../redux/style";
import { connect } from "react-redux";
import ThemeCard from "./DesignComponents/ThemeCard";
import MatchCard from "./DesignComponents/MatchCard";

import { SliderPicker } from "react-color";
import "./_style.css";

const DesignCard = (props) => {
  const { changeColor, changeColorHead, changeColorFoot, changeColorFont } = props;
  const [color, setColor] = useState("#FFFFFF");
  const [colorHead, setColorHead] = useState("#FFFFFF");
  const [colorFoot, setColorFoot] = useState("#FFFFFF");
  const [colorFont, setColorFont] = useState("black");
  const handleColorChange = (color) => {
    changeColor(color);
    setColor(color);
  };
  const handleColorChangeFont = (color) => {
    changeColorFont(color);
    setColorFont(color);
  };

  const handleColorChangeHead = (color) => {
    changeColorHead(color);
    setColorHead(color);
  };
  const handleColorChangeFoot = (color) => {
    changeColorFoot(color);
    setColorFoot(color);
  };
  return (
    <div className="design-card">
      {/* <IconCard /> */}

      {/* <SliderPicker color={color} onChange={(updatedColor) => handleColorChange(updatedColor)} />
      <SliderPicker color={colorHead} onChange={(updatedColor) => handleColorChangeHead(updatedColor)} />
      <SliderPicker color={colorFoot} onChange={(updatedColor) => handleColorChangeFoot(updatedColor)} />
      <SliderPicker color={colorFont} onChange={(updatedColor) => handleColorChangeFont(updatedColor)} /> */}
      <ThemeCard />
      {/* <MatchCard /> */}
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeColor: (color) => dispatch(changeColor(color)),
    changeColorHead: (color) => dispatch(changeColorHead(color)),
    changeColorFoot: (color) => dispatch(changeColorFoot(color)),
    changeColorFont: (color) => dispatch(changeColorFont(color)),
  };
};

export default connect(null, mapDispatchToProps)(DesignCard);
