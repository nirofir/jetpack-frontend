import React from "react";

import Icon from "widgets/Icon";

import "./_style.css";
const TextCard = () => {
  return (
    <div className="status-card text-card">
      <label>text</label>

      <div className="d-flex justify-content-between mb-2">
        <label>Greeting</label>
        <input type="text" placeholder="ex. Welcome" />
      </div>

      <div className="d-flex justify-content-between mb-2">
        <label>Description</label>
        <input type="text" placeholder="ex. Sign-in with email" />
      </div>

      <div className="d-flex justify-content-between mb-2">
        <label>Theme</label>
        <div className="text-align">
          <button className="active">
            <Icon name="blue_center" />
          </button>
          <button>
            <Icon name="overlay" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default TextCard;
