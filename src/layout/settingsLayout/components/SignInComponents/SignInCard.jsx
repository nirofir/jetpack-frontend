import React, { useState } from "react";

import RSelect from "widgets/RSelect";
import "./_style.css";
const options = [
  {
    id: "public",
    icon: "world",
    label: "Public",
  },
  {
    id: "circle",
    icon: "circle",
    label: "Circle",
  },
];
const SignInCard = () => {
  const [selectedOption, updateSelection] = useState("public");
  return (
    <div className="status-card">
      <label>Sign in</label>
      <RSelect
        label="Sign in"
        options={options}
        value={selectedOption}
        changeValue={(value) => updateSelection(value)}
      />
    </div>
  );
};

export default SignInCard;
