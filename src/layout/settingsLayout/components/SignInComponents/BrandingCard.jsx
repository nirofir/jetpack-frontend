import React from "react";

import Icon from "widgets/Icon";
import RSVGButton from "widgets/RSVGButton";

import "./_style.css";
const BrandingCard = () => {
  return (
    <div className="status-card branding-card">
      <label>branding</label>

      <div className="d-flex justify-content-between mb-2">
        <label>Background</label>
        <div className="file-upload">
          <span>
            <Icon name="upload" />
          </span>
          <input type="text" placeholder="Upload" />
        </div>
      </div>

      <div className="d-flex justify-content-between mb-2">
        <label>Overlay</label>
        <div className="d-flex">
          <RSVGButton icon="overlay1" active />
          <RSVGButton icon="overlay2" />
          <RSVGButton icon="overlay3" />
        </div>
      </div>

      <div className="d-flex justify-content-between mb-2">
        <label>Logo</label>
        <div className="file-upload">
          <span>
            <Icon name="upload" />
          </span>
          <input type="text" placeholder="Upload" />
        </div>
      </div>

      <div className="d-flex justify-content-between mb-4">
        <label>Logo Size</label>
        <div className="d-flex">
          <RSVGButton icon="small_dot" />
          <RSVGButton icon="medium_dot" active />
          <RSVGButton icon="large_dot" />
        </div>
      </div>
      <div className="option-card-item">
        <input type="checkbox" checked={true} readOnly />
        <label className="font-weight-bold">
          Use background image in loading screen
        </label>
      </div>
    </div>
  );
};

export default BrandingCard;
