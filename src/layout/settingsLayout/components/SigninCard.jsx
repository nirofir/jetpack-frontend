import React from 'react';

import SignInCardComponent from './SignInComponents/SignInCard';
import TextCard from './SignInComponents/TextCard';
import BrandingCard from './SignInComponents/BrandingCard';
import './_style.css';

const SigninCard = () => {
  return (
    <div className="sign-in-card">
      <SignInCardComponent />
      <TextCard />
      <BrandingCard />
    </div>
  );
};

export default SigninCard;
