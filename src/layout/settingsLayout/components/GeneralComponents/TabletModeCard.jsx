import React from 'react';

import './_style.css';
const TabletModeCard = () => {
  return (
    <div className="status-card tablet-card">
      <label>TABLET MODE</label>

      <div className="option-card-item">
        <input type="checkbox" checked={true} readOnly />
        <label className="font-weight-bold">
          Allow opening the app in tablet / desktop
        </label>
      </div>

      <p>
        Tablet mode is a Pro feature, but we're offering it for free until July
        2020.
      </p>
    </div>
  );
};

export default TabletModeCard;
