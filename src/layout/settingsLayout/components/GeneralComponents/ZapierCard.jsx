import React from 'react';

import './_style.css';
const ZapierCard = () => {
  return (
    <div className="status-card zapier-card">
      <label>zapier</label>

      <div className="d-flex justify-content-between">
        <label>Zapier API Key</label>
        <button className="zapier-btn">Copy to clipboard</button>
      </div>
    </div>
  );
};

export default ZapierCard;
