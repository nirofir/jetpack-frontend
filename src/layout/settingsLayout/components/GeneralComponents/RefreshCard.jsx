import React, { useState } from "react";

import RSelect from "widgets/RSelect";
import "./_style.css";

const options = [
  {
    id: "edit_only",
    icon: "pencil",
    label: "Only on edit",
  },
];
const RefreshCard = () => {
  const [selectOption, updateSelection] = useState("edit_only");
  return (
    <div className="status-card refresh-card">
      <label>REFRESH</label>

      <RSelect
        label="Reload sheet"
        options={options}
        value={selectOption}
        changeValue={(value) => updateSelection(value)}
      />
    </div>
  );
};

export default RefreshCard;
