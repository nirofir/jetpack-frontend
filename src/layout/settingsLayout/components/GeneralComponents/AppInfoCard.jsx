import React from 'react';

import './_style.css';
const AppInfoCard = () => {
  return (
    <div className="status-card app-info-card">
      <label>APP INFO</label>

      <div className="d-flex justify-content-between mb-2">
        <p>Name</p>
        <input type="text" value="Simaple CRM" />
      </div>

      <div className="d-flex justify-content-between mb-2">
        <p>Description</p>
        <input type="text" value="Made with JetpackApps.." />
      </div>

      <div className="d-flex justify-content-between">
        <p>Author</p>
        <input type="text" value="Salman Hossain Saif" />
      </div>
    </div>
  );
};

export default AppInfoCard;
