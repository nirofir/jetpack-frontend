import React from 'react';

import './_style.css';
const AppTemplateCard = () => {
  return (
    <div className="status-card app-info-card">
      <label>APP TEMPLATE</label>

      <div className="option-card-item">
        <input type="checkbox" checked={true} readOnly />
        <label className="font-weight-bold">
          Allow anyone with the link to copy the app & sheet
        </label>
      </div>
    </div>
  );
};

export default AppTemplateCard;
