import React from 'react';

import './_style.css';
const AnalyticsCard = () => {
  return (
    <div className="status-card app-info-card">
      <label>GOOGLE ANALYTICS</label>

      <div className="d-flex justify-content-between mb-2">
        <p>Tracking ID</p>
        <input type="text" placeholder="Enter trackign ID" />
      </div>
    </div>
  );
};

export default AnalyticsCard;
