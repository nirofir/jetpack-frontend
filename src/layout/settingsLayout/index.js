import React, { useState } from "react";
import styled from "styled-components";

import DesignCard from "./components/DesignCard";

import "./_styles.css";

const Header = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 22px;

  display: flex;
  align-items: center;
  justify-content: center;

  padding-top: 22px;
  padding-bottom: 22px;

  color: #303655;

  border-bottom: 1px solid #dfdfdf;
`;

// const Tabs = styled.div`
//   padding: 17px 0px 17px 0px;
//   margin: 0px;

//   width: 300px;

//   display: flex;
//   margin: auto;

//   font-style: normal;
//   font-weight: bold;
//   font-size: 12px;
//   line-height: 16px;
//   text-transform: uppercase;

//   color: #333333;
// `;

// const TabBar = styled.ul`
//   display: flex;
//   justify-content: space-between;

//   height: 30px;
//   width: 100%;

//   padding: 0;
//   margin: 0;

//   background: #ededf3;
//   border-radius: 30px;
// `;

// const TabItem = styled.li`
//   width: 100%;
//   cursor: pointer;

//   display: flex;
//   justify-content: center;
//   align-items: center;

//   text-transform: uppercase;

//   &.active {
//     background: #ffffff;
//     box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.19);
//     border-radius: 26px;
//     margin: 3px;
//     width: calc(100% - 6px);
//   }
// `;

// const TabMenuItems = ["design", "general", "sign-in"];

const SettingsLayout = React.memo(() => {
  // const [tabIndex, setTab] = useState(0);
  // const renderTabMenu = useCallback(
  //   () => (
  //     <Tabs>
  //       <TabBar>
  //         {TabMenuItems.map((item, index) => (
  //           <TabItem
  //             key={`settings-tab-${index}`}
  //             className={index === tabIndex ? "active" : ""}
  //             onClick={() => setTab(index)}
  //           >
  //             {item}
  //           </TabItem>
  //         ))}
  //       </TabBar>
  //     </Tabs>
  //   ),
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  //   [tabIndex]
  // );
  // const renderBody = useCallback(() => {
  //   switch (tabIndex) {
  //     case 1:
  //       return <GeneralCard />;
  //     case 2:
  //       return <SigninCard />;
  //     default:
  //       return <DesignCard />;
  //   }
  // }, [tabIndex]);
  return (
    <>
      <Header>Themes</Header>
      <DesignCard></DesignCard>
      {/* {renderTabMenu()}
      {renderBody()} */}
    </>
  );
});


export default SettingsLayout;
