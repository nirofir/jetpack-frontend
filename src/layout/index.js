import React, { useCallback } from "react";
import styled from "styled-components";
import { connect } from "react-redux";

import PropertyLayout from "./propertyLayout";
import ComponentLayout from "./componentLayout";
import NewComponentLayout from "./componentLayout/NewComponentLayout";
import EditComponentLayout from "./componentLayout/EditComponentLayout";
import SettingsLayout from "./settingsLayout";
import TabsLayout from "./tabsLayout";

const LayoutBox = styled.div`
  width: 367px;
  height: calc(100vh - 80px);

  background: transparent;

  position: absolute;
  right: 0;
  margin-right: -17px;

  overflow-y: scroll;
  overflow-x: hidden;
`;

const Layout = React.memo((props) => {
  const { layout, menuIndex, componentView } = props;

  const renderComponentLayout = useCallback(() => {
    switch (layout) {
      case "new_component":
        return <NewComponentLayout />;
      case "edit_component":
        return <EditComponentLayout />;
      default:
        if (!componentView) return <PropertyLayout />;
        else return <ComponentLayout />;
    }
  }, [layout, componentView]);

  // const renderTabsLayout = useCallback(() => {
  //   switch(Layout) {
  //     case "new_tab":
  //       return <NewTab></NewTab>
  //       return <TabsLayout></TabsLayout>;

  //     default :
  //       return <TabsLayout />;
  //   }
  // }, [layout]);


  const renderLayoutBar = useCallback(() => {
    switch (menuIndex) {
      case 0:
        return renderComponentLayout();
      case 1:
        return <TabsLayout></TabsLayout>;
      case 3:
        return <SettingsLayout />;
      default:
        return renderComponentLayout();
      // default:
      // return renderLayout();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [menuIndex, componentView, layout]);

  return <LayoutBox>{renderLayoutBar()}</LayoutBox>;
});

const mapStateToProps = (state) => ({
  menuIndex: state.NavReducer,
  layout: state.LayoutReducer.layoutPage,
  componentView: state.StyleReducer.componentView,
});

export default connect(mapStateToProps)(Layout);
