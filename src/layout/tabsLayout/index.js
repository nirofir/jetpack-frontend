import React from "react";
import styled from "styled-components";
import Icon from "widgets/Icon";

import StatusCard from "widgets/RStatusCard";
import {  addTab, reorderTabs, changeTab } from "redux/style";
import { changeLayoutPage, goBackLayoutPage } from "redux/layout";

import { connect } from "react-redux";

import DraggableTabs from "./DraggableTabs";
import AddTab from './AddTab';

const ComponentLayoutHeader = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 22px;

  display: flex;
  align-items: center;
  justify-content: center;

  position: relative;

  padding-top: 22px;
  padding-bottom: 22px;


  border-bottom: 1px solid #dfdfdf;

  & > svg path {
    fill: #009aff;
  }
`;

const RightControl = styled.span`
  font-family: Open Sans;
    font-style: normal;
    color: #333333;
  & > svg {
    width: 30px;
    height: 30px;
  }

  & > svg {
    position: absolute;
    right: 35px;
    top: 50%;
    transform: translateY(-50%);
    width: 24px;

    cursor: pointer;
  }
`;

const TabsMain = React.memo((props) => {
  const addTab = () => {
    props.addTab("new tab", "lemon");
    props.changeLayoutPage("new_tab");
  };
  return (
    <>
      <ComponentLayoutHeader>
        Tabs
        <RightControl onClick={() => addTab()}>
          <Icon name="plus" />
        </RightControl>
      </ComponentLayoutHeader>
      <StatusCard name="tabs">
        <DraggableTabs
          changeLayoutPage={props.changeLayoutPage}
          tabs={props.tabs}
          changeTab={props.changeTab}
          reorderTabs={props.reorderTabs}
        />
      </StatusCard>
    </>
  );
});

const TabsLayout = React.memo((props) => {
  const renderTabLayout = () => {
    return props.layout === "new_tab" ? (
      <AddTab goBackLayoutPage={props.goBackLayoutPage}></AddTab>
    ) : (
      <TabsMain
        addTab={props.addTab}
        changeLayoutPage={props.changeLayoutPage}
        tabsOptions={props.tabsOptions}
        tabs={props.tabs}
        reorderTabs={props.reorderTabs}
        changeTab={props.changeTab}></TabsMain>
    );
  };
  return renderTabLayout();
});

const mapStateToProps = (state) => ({
  tabsOptions: state.StyleReducer.tabs.map((tab, index) => ({
    title: tab.title,
    icon: tab.icon,
    key: index,
    id: tab.id,
  })),
  tabs: state.StyleReducer.tabs,
  layout: state.LayoutReducer.layoutPage,
});

const mapDispatchToProps = (dispatch) => ({
  addTab: (title, icon) => dispatch(addTab(title, icon)),
  changeLayoutPage: (pageIndex) => dispatch(changeLayoutPage(pageIndex)),
  goBackLayoutPage: () => dispatch(goBackLayoutPage()),
  reorderTabs: (orderedTabs) => dispatch(reorderTabs(orderedTabs)),
  changeTab: (index) => dispatch(changeTab(index)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TabsLayout);
