import React, { useCallback } from "react";
import styled from "styled-components";

import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

import TDragBox from "widgets/TDragBox";

const DragComponentItem = styled.div`
  user-select: none;
  margin: 0 0 8px 0;
`;

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const getItemStyle = (isDragging) => ({
  boxShadow: isDragging ? `0px 5px 5px rgba(0, 0, 0, 0.25)` : "",
});

const DraggableTabs = React.memo((props) => {
  const { tabs } = props;
  const { changeLayoutPage, changeTab, reorderTabs } = props;

  const onDragEnd = useCallback(
    (result) => {
      //reorder tabs
      if (!result.destination) return;
      const newTabs = reorder(tabs, result.source.index, result.destination.index);
      reorderTabs(newTabs);
    },
    [reorderTabs, tabs]
  );

  const onSelectItem = useCallback((index) => {
    console.log("Select Drag tab: ", index,);
    changeTab(index);
    changeLayoutPage("new_tab");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="droppable">
        {(provided, snapshot) => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            {tabs &&
              tabs.map((item, index) => (
                <Draggable key={item.id} draggableId={item.id + ""} index={index}>
                  {(provided, snapshot) => (
                    <div>
                      <DragComponentItem
                        {...provided.dragHandleProps}
                        {...provided.draggableProps}
                        ref={provided.innerRef}
                        style={provided.draggableProps.style}>
                        <TDragBox
                          {...item}
                          {...provided.dragHandleProps}
                          {...provided.draggableProps}
                          label={item.title}
                          style={getItemStyle(snapshot.isDragging)}
                          onSelect={() => onSelectItem(index)}
                        />
                      </DragComponentItem>
                    </div>
                  )}
                </Draggable>
              ))}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
});

export default DraggableTabs;
