import React, { useCallback, useState } from "react";
import styled from "styled-components";
import Icon from "widgets/Icon";

import StatusCard from "widgets/RStatusCard";
import {icons} from "./icons";

import { connect } from "react-redux";
import {updateTabLabel,changeTabIcon, deleteTab} from "redux/style"

const LeftControl = styled.span`
  & > svg {
    position: absolute;
    left: 35px;
    top: 50%;
    transform: translateY(-50%);

    cursor: pointer;
  }
`;
const RightControl = styled.span`
  & > svg {
    width: 30px;
    height: 30px;
  }

  & > svg {
    position: absolute;
    right: 35px;
    top: 50%;
    transform: translateY(-50%);
    width: 20px;
    cursor: pointer;
  }
`;

const ComponentLayoutHeader = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 22px;

  display: flex;
  align-items: center;
  justify-content: center;

  position: relative;

  padding-top: 22px;
  padding-bottom: 22px;

  color: #303655;

  border-bottom: 1px solid #dfdfdf;

  & > svg path {
    fill: #009aff;
  }
`;

const TabTabItem = styled.li`
  width: 100%;
  cursor: pointer;

  display: flex;
  justify-content: center;
  align-items: center;

  text-transform: uppercase;

  &.active {
    background: #ffffff;
    box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.19);
    border-radius: 26px;
    margin: 3px;
    width: calc(100% - 6px);
  }
`;

const PropertyTab = styled.div`
  padding-top: 17px;
  padding-bottom: 40px;
  margin: 0px;

  width: 300px;

  display: flex;
  margin: auto;

  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 16px;
  text-transform: uppercase;

  color: #333333;
`;


const PropertyTabBar = styled.ul`
  display: flex;
  justify-content: space-between;

  height: 30px;
  width: 100%;

  padding: 0;
  margin: 0;

  background: #ededf3;
  border-radius: 30px;
`;


const InputAndLabel = styled.div`
  padding-left:20px;
  padding-right:20px;
  height: 30px;
  display: flex;
  justify-content:space-between;

`;

const Label = styled.input`
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 16px;
  text-transform: uppercase;  
  color: #333333;
  border: 1px solid #BDBDBD;
  box-sizing: border-box;
  border-radius: 28px;
`

const LabelInput = (props) => {
    const updateLabelField = (e) => {
      props.updateTabLabel(e.target.value)
    }

    return (
        <InputAndLabel >
          <span>Label</span>
          <Label type="text" placeholder={props.tabLabel}
  onChange={(e) => updateLabelField(e)} autoFocus>
          </Label>
        </InputAndLabel>
    );
  };

const Iconi = styled.div`
  cursor: pointer;
`
const TabIcons = (props) => {
    const iconz = icons()
    return Object.keys(iconz).map((x,index) => (
      <Iconi onClick={() => props.setIcon(x)} key={index}>
        {iconz[x].icon}
      </Iconi>)
      )
}

const IconGrid = styled.li`
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 10px;

  display: grid;
  grid-template-columns: auto auto auto auto auto;
  grid-template-rows: auto auto auto auto auto auto auto;
  justify-items: center;
  row-gap:28px;

  &.active {
    background: #ffffff;
    box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.19);
    border-radius: 26px;
    margin: 3px;
    width: calc(100% - 6px);
  }
`;
const AddTabLayout = styled.div`
  width: 100%;
  display:flex;
  flex-direction: column;

`
const TABSOPTIONS = ["layout", "feature"];


const AddTab = React.memo((props) => {

  const [tabIndex, setTab] = useState(0);
  const renderTab = useCallback(() => {
    return TABSOPTIONS.map((layout, index) => (
      <TabTabItem
        key={`property-tab-${index}`}
        className={index === tabIndex ? "active" : ""}
        onClick={() => setTab(index)}>
        {layout}
      </TabTabItem>
    ));
  }, [tabIndex]);

    return (<AddTabLayout>
      <ComponentLayoutHeader>
        <LeftControl onClick={() => props.goBackLayoutPage()}>
          <Icon name="back" />
        </LeftControl>
        {props.tabLabel === "NEW TAB" ? "New Tab" : "Edit Tab"}
        {props.tabsLength > 1 && <RightControl onClick={() => props.deleteTab()}>
          <Icon name="trash" />
        </RightControl>}
      </ComponentLayoutHeader>
      <PropertyTab>
        <PropertyTabBar>{renderTab()}</PropertyTabBar>
      </PropertyTab>
        <LabelInput updateTabLabel={props.updateTabLabel} tabLabel={props.tabLabel}></LabelInput>
        <StatusCard>
        </StatusCard>
        <IconGrid>

          <TabIcons setIcon={props.changeTabIcon}></TabIcons>
        </IconGrid>
    </AddTabLayout>
    );
  });

  const mapStateToProps = (state) => ({
    currentTab: state.StyleReducer.currentTab,  
    layout: state.LayoutReducer.layoutPage,
    tabLabel: state.StyleReducer.tabs[state.StyleReducer.currentTab].title,
    tabsLength : state.StyleReducer.tabs.length
  });
    
  const mapDispatchToProps = (dispatch) => ({
    updateTabLabel: (newTitle) => dispatch(updateTabLabel(newTitle)),
    changeTabIcon: (newIcon) => dispatch(changeTabIcon(newIcon)),
    deleteTab: () => dispatch(deleteTab())
  });
export default connect(mapStateToProps, mapDispatchToProps)(AddTab);