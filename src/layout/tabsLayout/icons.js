import { GiAfrica,Gi3DMeeple,Gi3DStairs ,GiAlarmClock, GiTabiBoot,GiAmphora,GiAmericanFootballHelmet,GiAmericanShield,GiAquarius,GiArcher,GiArrest,GiAtlas,GiBananaBunch,GiBalloonDog,GiBatMask,GiBisonGiBilledCap,GiBrainTentacle,GiBrute,GiCaduceus,GiCamelHead,GiCentaur,GiShuriken,Gi3DHammer,GiAmericanFootballBall,GiAllSeeingEye} from "react-icons/gi";
import {GiBoltBomb, GiBrazilFlag, GiBrickWall, GiBowlingPin, GiCancer, GiCampingTent, GiChalkOutlineMurder, GiCompactDisc, GiCutLemon ,GiDiamondRing, GiMoneyStack, GiGoose ,GiFreemasonry} from "react-icons/gi";
import React from "react";

// const style = 
// svg {
//   width: 18px;
//   height: 18px;
// } 
// const style = {width:"28px", height:"28px"}

export const icons = (style = {width:"28px", height:"28px"}) => { return {
    africa : { icon: <GiAfrica style={style}></GiAfrica>},
    ppl : { icon: <Gi3DMeeple style={style}></Gi3DMeeple>},
    stairs : { icon: <Gi3DStairs style={style}></Gi3DStairs>},
    clock : { icon: <GiAlarmClock style={style}></GiAlarmClock>},
    boot : { icon: <GiTabiBoot style={style}></GiTabiBoot>},
    amp : { icon: <GiAmphora style={style}></GiAmphora>},
    helmet : { icon: <GiAmericanFootballHelmet style={style} ></GiAmericanFootballHelmet>},
    shield : { icon: <GiAmericanShield style={style}></GiAmericanShield>},
    water : { icon: <GiAquarius style={style}></GiAquarius>},
    atlas : { icon: <GiAtlas style={style}></GiAtlas>},
    banana : { icon: <GiBananaBunch style={style}></GiBananaBunch>},
    baloon : { icon: <GiBalloonDog style={style}></GiBalloonDog>},
    mask : { icon: <GiBatMask style={style}></GiBatMask>},
    tentecle : { icon: <GiBrainTentacle style={style}></GiBrainTentacle>},
    brute : { icon: <GiBrute style={style}></GiBrute>},
    cad : { icon: <GiCaduceus style={style}></GiCaduceus>},
    camel : { icon: <GiCamelHead style={style}></GiCamelHead>},
    centaur : { icon: <GiCentaur style={style}></GiCentaur>},
    shuriken : { icon: <GiShuriken style={style}></GiShuriken>},
    hammer : { icon: <Gi3DHammer style={style}></Gi3DHammer>},
    ball : { icon: <GiAmericanFootballBall style={style}></GiAmericanFootballBall>},
    eye : { icon: <GiAllSeeingEye style={style}></GiAllSeeingEye>},
    ball2 : { icon: <GiBoltBomb style={style}></GiBoltBomb>},
    brazil : { icon: <GiBrazilFlag style={style}></GiBrazilFlag>},
    wall : { icon: <GiBrickWall style={style}></GiBrickWall>},
    bowling : { icon: <GiBowlingPin style={style}></GiBowlingPin>},
    cancer : { icon: <GiCancer style={style}></GiCancer>},
    camping : { icon: <GiCampingTent style={style}></GiCampingTent>},
    line : { icon: <GiChalkOutlineMurder style={style}></GiChalkOutlineMurder>},
    compact : { icon: <GiCompactDisc style={style}></GiCompactDisc>},
    lemon : { icon: <GiCutLemon style={style}></GiCutLemon>},
    ring : { icon: <GiDiamondRing style={style}></GiDiamondRing>},
    moeny : { icon: <GiMoneyStack style={style}></GiMoneyStack>},
    goose : { icon: <GiGoose style={style}></GiGoose>},
    freemasonary : { icon: <GiFreemasonry style={style}></GiFreemasonry>},
}}
