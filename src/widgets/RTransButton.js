import React from "react";
import styled from "styled-components";

import Icon from "./Icon";

const RTransButtonBox = styled.div`
  display: flex;
  align-items: center;

  margin-top: 15px;

  font-size: 14px;
  color: #009aff;
  font-weight: bold;
  line-height: 19px;
  cursor: pointer;

  & > svg {
    width: 20px;
    height: 20px;
  }

  & > svg path {
    fill: #009aff;
  }

  & > span {
    margin-left: 10px;
    text-transform: capitalize;
  }
`;

const RTransButton = React.memo(({ name, label , click}) => {
  const clickBtn = () => {
    if(click) click();
  }
  return (
    <RTransButtonBox onClick={() => clickBtn()}>
      <Icon name={name ? name : "plus"}/>
      <span>{label}</span>
    </RTransButtonBox>
  );
});

export default RTransButton;
