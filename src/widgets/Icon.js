import React from "react";

import { ReactComponent as Text } from "assets/svg/text.svg";
import { ReactComponent as Image } from "assets/svg/image.svg";
import { ReactComponent as Cart } from "assets/svg/cart.svg";
import { ReactComponent as World } from "assets/svg/world.svg";
import { ReactComponent as Circle } from "assets/svg/circle.svg";
import { ReactComponent as NoSelect } from "assets/svg/no_select.svg";
import { ReactComponent as Mail } from "assets/svg/mail.svg";
import { ReactComponent as Movie } from "assets/svg/movie.svg";
import { ReactComponent as Float } from "assets/svg/float.svg";
import { ReactComponent as Flat } from "assets/svg/flat.svg";
import { ReactComponent as None } from "assets/svg/none.svg";
import { ReactComponent as Shop } from "assets/svg/shop.svg";
import { ReactComponent as User } from "assets/svg/user.svg";
import { ReactComponent as Speech } from "assets/svg/speech.svg";
import { ReactComponent as ActionText } from "assets/svg/action_text.svg";
import { ReactComponent as RichText } from "assets/svg/rich_text.svg";
import { ReactComponent as Tags } from "assets/svg/tags.svg";
import { ReactComponent as Date } from "assets/svg/date.svg";
import { ReactComponent as Pin } from "assets/svg/pin.svg";
import { ReactComponent as Contact } from "assets/svg/contact.svg";
import { ReactComponent as Table } from "assets/svg/table.svg";
import { ReactComponent as Separator } from "assets/svg/separator.svg";
import { ReactComponent as Title } from "assets/svg/title.svg";
import { ReactComponent as Image1 } from "assets/svg/image_.svg";
import { ReactComponent as MapPointer } from "assets/svg/map_pointer.svg";
import { ReactComponent as Ok } from "assets/svg/ok.svg";
import { ReactComponent as Link } from "assets/svg/link.svg";
import { ReactComponent as Phone } from "assets/svg/phone.svg";
import { ReactComponent as Phone1 } from "assets/svg/phone_.svg";
import { ReactComponent as List1 } from "assets/svg/list_.svg";
import { ReactComponent as Date1 } from "assets/svg/date_.svg";
import { ReactComponent as Clock } from "assets/svg/clock.svg";
import { ReactComponent as Status } from "assets/svg/status.svg";
import { ReactComponent as Rating } from "assets/svg/rating.svg";
import { ReactComponent as Vote } from "assets/svg/vote.svg";
import { ReactComponent as Todo } from "assets/svg/todo.svg";
import { ReactComponent as Reload1 } from "assets/svg/reload_.svg";
import { ReactComponent as TimeLine } from "assets/svg/timeline.svg";
import { ReactComponent as Progress } from "assets/svg/progress.svg";
import { ReactComponent as Comment } from "assets/svg/comment.svg";
import { ReactComponent as TextEntry } from "assets/svg/text_entry.svg";
import { ReactComponent as Notes } from "assets/svg/notes.svg";
import { ReactComponent as NumberEntry } from "assets/svg/number_entry.svg";
import { ReactComponent as List } from "assets/svg/list.svg";
import { ReactComponent as Compact } from "assets/svg/compact.svg";
import { ReactComponent as Tiles } from "assets/svg/tiles.svg";
import { ReactComponent as Calendar } from "assets/svg/calendar.svg";
import { ReactComponent as Map } from "assets/svg/map.svg";
import { ReactComponent as Details } from "assets/svg/details.svg";
import { ReactComponent as Layout } from "assets/svg/layout.svg";
import { ReactComponent as Tabs } from "assets/svg/tabs.svg";
import { ReactComponent as Data } from "assets/svg/data.svg";
import { ReactComponent as Preview } from "assets/svg/preview.svg";
import { ReactComponent as Settings } from "assets/svg/settings.svg";
import { ReactComponent as Divider } from "assets/svg/divider.svg";
import { ReactComponent as Edit } from "assets/svg/edit.svg";
import { ReactComponent as Reload } from "assets/svg/reload.svg";
import { ReactComponent as Alert } from "assets/svg/alert.svg";
import { ReactComponent as Search } from "assets/svg/search.svg";
import { ReactComponent as IphoneHeader } from "assets/svg/iphone-header.svg";
import { ReactComponent as Connection } from "assets/svg/connection.svg";
import { ReactComponent as Wifi } from "assets/svg/wi-fi.svg";
import { ReactComponent as Battery } from "assets/svg/battery.svg";
import { ReactComponent as Plus } from "assets/svg/plus.svg";
import { ReactComponent as Back } from "assets/svg/back.svg";
import { ReactComponent as Hamburger } from "assets/svg/hamburger.svg";
import { ReactComponent as Prev } from "assets/svg/prev.svg";
import { ReactComponent as Next } from "assets/svg/next.svg";
import { ReactComponent as Question } from "assets/svg/question.svg";
import { ReactComponent as Rectangle } from "assets/svg/rectangle.svg";
import { ReactComponent as Round } from "assets/svg/round.svg";
import { ReactComponent as PaddingOut } from "assets/svg/padding_out.svg";
import { ReactComponent as PaddingIn } from "assets/svg/padding_in.svg";
import { ReactComponent as Crop } from "assets/svg/crop.svg";
import { ReactComponent as Center } from "assets/svg/center.svg";
import { ReactComponent as Below } from "assets/svg/below.svg";
import { ReactComponent as Overlay } from "assets/svg/overlay.svg";
import { ReactComponent as TopLeft } from "assets/svg/top_left.svg";
import { ReactComponent as BottomLeft } from "assets/svg/bottom_left.svg";
import { ReactComponent as BottomRight } from "assets/svg/bottom_right.svg";
import { ReactComponent as TopRight } from "assets/svg/top_right.svg";
import { ReactComponent as AlignCenter } from "assets/svg/align_center.svg";
import { ReactComponent as SmallText } from "assets/svg/a_10.svg";
import { ReactComponent as MediumText } from "assets/svg/a_14.svg";
import { ReactComponent as LargeText } from "assets/svg/a_16.svg";
import { ReactComponent as TodoList } from "assets/svg/todolist.svg";
import { ReactComponent as Pencil } from "assets/svg/pencil.svg";
import { ReactComponent as Upload } from "assets/svg/upload.svg";
import { ReactComponent as LargeDot } from "assets/svg/large_dot.svg";
import { ReactComponent as MediumDot } from "assets/svg/medium_dot.svg";
import { ReactComponent as SmallDot } from "assets/svg/small_dot.svg";
import { ReactComponent as Overlay1 } from "assets/svg/overlay1.svg";
import { ReactComponent as Overlay2 } from "assets/svg/overlay2.svg";
import { ReactComponent as Overlay3 } from "assets/svg/overlay3.svg";
import { ReactComponent as BlueCenter } from "assets/svg/blue_center.svg";
import { ReactComponent as TextAlignLeft } from "assets/svg/text_align_left.svg";
import { ReactComponent as TextAlignRight } from "assets/svg/text_align_right.svg";
import { ReactComponent as TextAlignCenter } from "assets/svg/text_align_center.svg";
import { ReactComponent as TextAlignFill } from "assets/svg/text_align_fill.svg";
import { ReactComponent as Check } from "assets/svg/check.svg";
import { ReactComponent as FillArea } from "assets/svg/fill_area.svg";
import { ReactComponent as EdgeEdge } from "assets/svg/edge_edge.svg";
import { ReactComponent as TextFill } from "assets/svg/text_fill.svg";
import { ReactComponent as TextInvert } from "assets/svg/text_invert.svg";
import { ReactComponent as TextOutline } from "assets/svg/text_outline.svg";
import { ReactComponent as Minus } from "assets/svg/minus.svg";
import { ReactComponent as HandPointer } from "assets/svg/hand_pointer.svg";
import { ReactComponent as Order } from "assets/svg/order.svg";
import { ReactComponent as Member } from "assets/svg/member.svg";
import { ReactComponent as Chart } from "assets/svg/chart.svg";
import { ReactComponent as Speedometer } from "assets/svg/speedometer.svg";
import { ReactComponent as IProgress } from "assets/svg/progress_invert.svg";
import { ReactComponent as OutlineRect } from "assets/svg/rectangle_outline.svg";
import { ReactComponent as Star } from "assets/svg/star.svg";
import { ReactComponent as Heart } from "assets/svg/heart.svg";
import { ReactComponent as HandOutline } from "assets/svg/hand_outline.svg";
import { ReactComponent as CheckBox } from "assets/svg/checkbox.svg";
import { ReactComponent as Key } from "assets/svg/key.svg";
import { ReactComponent as HandFill } from "assets/svg/hand_fill.svg";
import { ReactComponent as Moon } from "assets/svg/moon.svg";
import { ReactComponent as Sun } from "assets/svg/sun.svg";
import { ReactComponent as VoteOutline } from "assets/svg/vote_outline.svg";
import { ReactComponent as StarFill } from "assets/svg/star_fill.svg";
import { ReactComponent as HeartFill } from "assets/svg/heart_fill.svg";
import { ReactComponent as Indicator } from "assets/svg/indicator.svg";
import { ReactComponent as RainBow } from "assets/svg/rainbow.svg";
import { ReactComponent as ArrowRight } from "assets/svg/arrow_right.svg";
import { ReactComponent as Printer } from "assets/svg/printer.svg";
import { ReactComponent as Duplicate } from "assets/svg/duplicate.svg";
import { ReactComponent as Cross } from "assets/svg/cross.svg";
import { ReactComponent as Note } from "assets/svg/note.svg";
import { ReactComponent as Comments } from "assets/svg/comments.svg";
import { ReactComponent as Trash } from "assets/svg/trash.svg";
import { ReactComponent as CheckoutRoundOutline } from 'assets/svg/checkout_round_outline.svg';
import { ReactComponent as CheckoutRoundFill } from 'assets/svg/checkout_round_fill.svg';

// dropdown icons
import { ReactComponent as Square } from "assets/dropdown_svg/square.svg";
import { ReactComponent as Horizontal1 } from "assets/dropdown_svg/horizontal3_1.svg";
import { ReactComponent as Horizontal2 } from "assets/dropdown_svg/horizontal3_2.svg";
import { ReactComponent as Horizontal3 } from "assets/dropdown_svg/horizontal4_3.svg";
import { ReactComponent as Vertical2 } from "assets/dropdown_svg/vertical2_3.svg";
import { ReactComponent as Vertical3 } from "assets/dropdown_svg/vertical3_4.svg";
import { ReactComponent as OpenLink } from "assets/dropdown_svg/open_link.svg";
import { ReactComponent as ViewDetail } from 'assets/dropdown_svg/view_detail.svg';
import { ReactComponent as DialPhone } from 'assets/dropdown_svg/phone.svg';
import { ReactComponent as LinkScreen } from 'assets/dropdown_svg/link_screen.svg';
import { ReactComponent as OpenWebview } from 'assets/dropdown_svg/open_webview.svg';
import { ReactComponent as SendEmail } from 'assets/dropdown_svg/email.svg';
import { ReactComponent as SendMessage } from 'assets/dropdown_svg/message.svg';
import { ReactComponent as ShowSharing } from 'assets/dropdown_svg/share.svg';
import { ReactComponent as ShowMap } from 'assets/dropdown_svg/map.svg';

const svgIcons = [
  { label: "text", icon: <Text /> },
  { label: "image", icon: <Image /> },
  { label: "cart", icon: <Cart /> },
  { label: "world", icon: <World /> },
  { label: "circle", icon: <Circle /> },
  { label: "no_select", icon: <NoSelect /> },
  { label: "mail", icon: <Mail /> },
  { label: "movie", icon: <Movie /> },
  { label: "float", icon: <Float /> },
  { label: "flat", icon: <Flat /> },
  { label: "none", icon: <None /> },
  { label: "shop", icon: <Shop /> },
  { label: "user", icon: <User /> },
  { label: "speech", icon: <Speech /> },
  { label: "action_text", icon: <ActionText /> },
  { label: "rich_text", icon: <RichText /> },
  { label: "tags", icon: <Tags /> },
  { label: "date", icon: <Date /> },
  { label: "pin", icon: <Pin /> },
  { label: "contact", icon: <Contact /> },
  { label: "table", icon: <Table /> },
  { label: "separator", icon: <Separator /> },
  { label: "title", icon: <Title /> },
  { label: "image_", icon: <Image1 /> },
  { label: "map_pointer", icon: <MapPointer /> },
  { label: "ok", icon: <Ok /> },
  { label: "link", icon: <Link /> },
  { label: "phone", icon: <Phone /> },
  { label: "phone_", icon: <Phone1 /> },
  { label: "list_", icon: <List1 /> },
  { label: "date_", icon: <Date1 /> },
  { label: "clock", icon: <Clock /> },
  { label: "status", icon: <Status /> },
  { label: "rating", icon: <Rating /> },
  { label: "vote", icon: <Vote /> },
  { label: "todo", icon: <Todo /> },
  { label: "reload_", icon: <Reload1 /> },
  { label: "timeline", icon: <TimeLine /> },
  { label: "progress", icon: <Progress /> },
  { label: "comment", icon: <Comment /> },
  { label: "text_entry", icon: <TextEntry /> },
  { label: "notes", icon: <Notes /> },
  { label: "number_entry", icon: <NumberEntry /> },
  { label: "list", icon: <List /> },
  { label: "tiles", icon: <Tiles /> },
  { label: "compact", icon: <Compact /> },
  { label: "calendar", icon: <Calendar /> },
  { label: "map", icon: <Map /> },
  { label: "details", icon: <Details /> },
  { label: "layout", icon: <Layout /> },
  { label: "tabs", icon: <Tabs /> },
  { label: "data", icon: <Data /> },
  { label: "preview", icon: <Preview /> },
  { label: "settings", icon: <Settings /> },
  { label: "divider", icon: <Divider /> },
  { label: "edit", icon: <Edit /> },
  { label: "reload", icon: <Reload /> },
  { label: "alert", icon: <Alert /> },
  { label: "search", icon: <Search /> },
  { label: "iphone_header", icon: <IphoneHeader /> },
  { label: "connection", icon: <Connection /> },
  { label: "wifi", icon: <Wifi /> },
  { label: "battery", icon: <Battery /> },
  { label: "back", icon: <Back /> },
  { label: "plus", icon: <Plus /> },
  { label: "hamburger", icon: <Hamburger /> },
  { label: "prev", icon: <Prev /> },
  { label: "next", icon: <Next /> },
  { label: "question", icon: <Question /> },
  { label: "rectangle", icon: <Rectangle /> },
  { label: "round", icon: <Round /> },
  { label: "padding_out", icon: <PaddingOut /> },
  { label: "padding_in", icon: <PaddingIn /> },
  { label: "crop", icon: <Crop /> },
  { label: "center", icon: <Center /> },
  { label: "below", icon: <Below /> },
  { label: "overlay", icon: <Overlay /> },
  { label: "top_left", icon: <TopLeft /> },
  { label: "bottom_left", icon: <BottomLeft /> },
  { label: "bottom_right", icon: <BottomRight /> },
  { label: "top_right", icon: <TopRight /> },
  { label: "align_center", icon: <AlignCenter /> },
  { label: "small", icon: <SmallText /> },
  { label: "medium", icon: <MediumText /> },
  { label: "large", icon: <LargeText /> },
  { label: "todolist", icon: <TodoList /> },
  { label: "pencil", icon: <Pencil /> },
  { label: "upload", icon: <Upload /> },
  { label: "large_dot", icon: <LargeDot /> },
  { label: "medium_dot", icon: <MediumDot /> },
  { label: "small_dot", icon: <SmallDot /> },
  { label: "overlay1", icon: <Overlay1 /> },
  { label: "overlay2", icon: <Overlay2 /> },
  { label: "overlay3", icon: <Overlay3 /> },
  { label: "blue_center", icon: <BlueCenter /> },
  { label: "text_align_left", icon: <TextAlignLeft /> },
  { label: "text_align_right", icon: <TextAlignRight /> },
  { label: "text_align_center", icon: <TextAlignCenter /> },
  { label: "text_align_fill", icon: <TextAlignFill /> },
  { label: "check", icon: <Check /> },
  { label: "square", icon: <Square /> },
  { label: "horizontal3_1", icon: <Horizontal1 /> },
  { label: "horizontal3_2", icon: <Horizontal2 /> },
  { label: "fill_area", icon: <FillArea /> },
  { label: "edge_edge", icon: <EdgeEdge /> },
  { label: "open_link", icon: <OpenLink /> },
  { label: "text_fill", icon: <TextFill /> },
  { label: "text_invert", icon: <TextInvert /> },
  { label: "text_outline", icon: <TextOutline /> },
  { label: "minus", icon: <Minus /> },
  { label: "hand_pointer", icon: <HandPointer /> },
  { label: "order", icon: <Order /> },
  { label: "member", icon: <Member /> },
  { label: "chart", icon: <Chart /> },
  { label: "speedometer", icon: <Speedometer /> },
  { label: "progress_invert", icon: <IProgress /> },
  { label: "outline_rectangle", icon: <OutlineRect /> },
  { label: "star", icon: <Star /> },
  { label: "heart", icon: <Heart /> },
  { label: "hand_outline", icon: <HandOutline /> },
  { label: "checkbox", icon: <CheckBox /> },
  { label: "key", icon: <Key /> },
  { label: "hand_fill", icon: <HandFill /> },
  { label: "sun", icon: <Sun /> },
  { label: "moon", icon: <Moon /> },
  { label: "vote_outline", icon: <VoteOutline /> },
  { label: "star_fill", icon: <StarFill /> },
  { label: "heart_fill", icon: <HeartFill /> },
  { label: "indicator", icon: <Indicator /> },
  { label: "rainbow", icon: <RainBow /> },
  { label: "arrow_right", icon: <ArrowRight /> },
  { label: "horizontal4_3", icon: <Horizontal3 /> },
  { label: "vertical2_3", icon: <Vertical2 /> },
  { label: "vertical3_4", icon: <Vertical3 /> },
  { label: "printer", icon: <Printer /> },
  { label: "cross", icon: <Cross /> },
  { label: "note", icon: <Note /> },
  { label: "comments", icon: <Comments /> },
  { label: "trash", icon: <Trash /> },
  { label: "checkout_round_outline", icon: <CheckoutRoundOutline /> },
  { label: 'checkout_round_fill', icon: <CheckoutRoundFill /> },
  // Drop Down Icons
  { label: 'view_details', icon: <ViewDetail /> },
  { label: "duplicate", icon: <Duplicate /> },
  { label: "dial_phone", icon: <DialPhone /> },
  { label: 'link_screen', icon: <LinkScreen /> },
  { label: 'open_link', icon: <OpenLink /> },
  { label: 'open_webview', icon: <OpenWebview /> },
  { label: 'send_email', icon: <SendEmail /> },
  { label: 'send_message', icon: <SendMessage /> },
  { label: 'show_sharing', icon: <ShowSharing /> },
  { label: 'show_map', icon: <ShowMap /> },

];

// let icons = {};
// for (var i = 0, length = svgIconNames.length; i < length; i++) {
//   const icon = svgIconNames[i];
//   icons = {
//     ...icons,
//     [icon.label]: icon.icon,
//   };
// }

const Icon = React.memo(
  ({ name }) => {
    const icon = svgIcons.filter((icon) => icon.label === name);
    if (icon && icon.length > 0) return icon[0].icon;
    return <></>;
  }
);

export default Icon;
