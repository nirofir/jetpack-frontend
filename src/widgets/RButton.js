import React from "react";
import styled from "styled-components";

const RButtonBox = styled.button`
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 20px;
  padding-right: 20px;

  margin-top: 15px;

  display: flex;
  justify-content: center;
  align-items: center;

  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 19px;

  display: flex;
  align-items: center;

  color: #333333;

  border: none;
  border-radius: 44px;

  cursor: pointer;

  &.gray {
    background: #ededf3;
  }
`;

const RButton = React.memo(({ label, gray }) => {
  return <RButtonBox className={gray ? "gray" : ""}>{label}</RButtonBox>;
});

export default RButton;
