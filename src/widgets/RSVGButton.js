import React from "react";
import styled from "styled-components";

import Icon from "./Icon";

const RButtonBox = styled.button`
  background: transparent;
  border-radius: 5px;
  border: none;

  width: 30px;
  height: 30px;

  display: flex;
  justify-content: center;
  align-items: center;

  :hover,
  &.active {
    background: #3ad6f533;
  }
`;

const RSVGButton = React.memo(({ icon, active, onClick, children }) => {
  return (
    <RButtonBox className={active ? "active" : ""} onClick={() => onClick()}>
      {icon ? <Icon name={icon} /> : children}
    </RButtonBox>
  );
});

export default RSVGButton;
