import React from "react";
import styled from "styled-components";

import Icon from "widgets/Icon";

const RSearchBox = styled.div`
  position: relative;
  height: 40px;

  margin-bottom: 15px;

  padding-left: 10px;
  padding-right: 10px;

  & > svg {
    position: absolute;
    left: 1rem;
    top: 50%;
    transform: translateY(-50%);
  }
`;
const SearchInput = styled.input`
  background: ${props => !props.background1 ? '#f2f2f2' : props.background1} ;
  border-radius: 19.5px;

  height: 100%;
  width: 100%;

  box-sizing: border-box;

  padding-left: 43px;

  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 25px;

  color: ${props => props.placeHolderSearch};

  display: block;
  border: none;
  ::placeholder {
    color: ${props => props.placeHolderSearch};
}

`;




const RSearchBar = React.memo((props) => {
  return (
    <RSearchBox >
      <svg width="17" height="17" fill={props.magnifingColor} viewBox="0 0 17 17" xmlns="http://www.w3.org/2000/svg">
        <path d="M3.80078 9.64453C4.60286 10.4466 5.57682 10.8477 6.72266 10.8477C7.86849 10.8477 8.84245 10.4466 9.64453 9.64453C10.4466 8.84245 10.8477 7.86849 10.8477 6.72266C10.8477 5.57682 10.4466 4.60286 9.64453 3.80078C8.84245 2.9987 7.86849 2.59766 6.72266 2.59766C5.57682 2.59766 4.60286 2.9987 3.80078 3.80078C2.9987 4.60286 2.59766 5.57682 2.59766 6.72266C2.59766 7.86849 2.9987 8.84245 3.80078 9.64453ZM12.2227 10.8477L16.7773 15.4023L15.4023 16.7773L10.8477 12.2227V11.4922L10.5898 11.2344C9.5013 12.1797 8.21224 12.6523 6.72266 12.6523C5.0612 12.6523 3.64323 12.0794 2.46875 10.9336C1.32292 9.78776 0.75 8.38411 0.75 6.72266C0.75 5.0612 1.32292 3.65755 2.46875 2.51172C3.64323 1.33724 5.0612 0.75 6.72266 0.75C8.38411 0.75 9.78776 1.33724 10.9336 2.51172C12.0794 3.65755 12.6523 5.0612 12.6523 6.72266C12.6523 8.21224 12.1797 9.5013 11.2344 10.5898L11.4922 10.8477H12.2227Z"
        />
      </svg>

      <SearchInput type="text" background1={props.backgroundColor1} placeHolderSearch={props.placeHolderSearchColor} placeholder="Search" onChange={(updatedSerch) => props.filterPersonHandler(updatedSerch)} />
    </RSearchBox>
  );
});

export default RSearchBar;
