import React from "react";
import styled from "styled-components";

import Icon from "./Icon";

const RDragDiv = styled.div`
  user-select: none;

  font-family: Segoe MDL2 Assets;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 16px;

  height: 40px;

  display: flex;
  align-items: center;
  border: none;

  margin-top: 10px;

  text-transform: capitalize;

  cursor: pointer;

  & > svg {
    margin-right: 0.5rem;
  }
  &>svg: first-child path {
    fill: #737383;
  }
`;

const RDragRegion = styled.div`
  width: 100%;
  height: 100%;

  border: 1px solid #e0e0e0;
  border-radius: 4px;

  display: flex;
  justify-content: space-between;

  font-family: Open Sans;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;

  color: #333333;

  box-sizing: border-box;

  &:hover {
    box-shadow: 0px 5px 5px rgba(0, 0, 0, 0.25);
  }

  & svg path {
    fill: #333333;
  }
`;

const BoldRegion = styled.div`
  width: 100%;
  display: flex;
  align-items: center;

  font-weight: 600;

  & > svg {
    margin: 10px;

    width: 16px;
    height: 16px;
  }
`;

const LightRegion = styled.div`
  width: 70%;
  display: flex;
  align-items: center;
  position: relative;

  font-weight: normal;
  color: #33333388;

  & svg {
    width: 12px;
    height: 12px;
  }

  & svg:hover {
    width: 14px;
    height: 14px;
  }

  & svg:hover path {
    fill: #000000;
  }

  & span:first-child {
    position: absolute;
    right: 40px;
  }

  & span:last-child {
    position: absolute;
    right: 15px;
  }
`;

const RDragBox = React.memo(({ name, label, icon, onSelect, onClose, onDuplicate }) => {
  return (
    <RDragDiv>
      <Icon name="hamburger" />
      <RDragRegion>
        <BoldRegion onClick={() => onSelect()}>
          <Icon name={icon} />
          {name}
        </BoldRegion>
        <LightRegion>
          {label}
          <span onClick={() => onDuplicate()}>
            <Icon name="duplicate" />
          </span>
          <span onClick={() => onClose()}>
            <Icon name="cross" />
          </span>
        </LightRegion>
      </RDragRegion>
    </RDragDiv>
  );
});

export default RDragBox;
