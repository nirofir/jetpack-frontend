import React from "react";
import styled from "styled-components";

const ROptionBox = styled.div`
  display: grid;
  grid-template-columns: [first] 30px [line2] auto;

  font-size: 14px;

  margin-top: 15px;

  & input[type="checkbox"] {
    width: 18px;
    height: 18px;
  }
`;

const ROption = React.memo(({ value, label, onChange }) => {
  return (
    <ROptionBox>
      <input
        type="checkbox"
        checked={value}
        onChange={() => onChange(!value)}
      />
      <label>{label}</label>
    </ROptionBox>
  );
});

export default ROption;
