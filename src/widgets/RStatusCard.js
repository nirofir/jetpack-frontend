import React from "react";
import styled from "styled-components";

const RStatusCardBox = styled.div`
  border-bottom: 1px solid #dfdfdf;

  padding: 20px;

  font-family: Open Sans;
  font-style: normal;

  color: #333333;

  & > p {
    font-weight: normal;
    font-size: 14px;
    line-height: 19px;

    margin: 0;
    padding: 0;

    margin-top: 15px;
  }

  & > p.description {
    font-weight: 600;
    font-size: 12px;
    line-height: 16px;
  }
`;

const RStatusCardName = styled.label`
  font-weight: bold;
  font-size: 16px;
  line-height: 22px;
  color: #333333;

  margin-bottom: 15px;
  text-transform: uppercase;
`;

const RStatusCard = React.memo(({ name, children }) => {
  return (
    <RStatusCardBox>
      <RStatusCardName>{name}</RStatusCardName>
      {children}
    </RStatusCardBox>
  );
});

export default RStatusCard;
