import React, { useState, useEffect, useCallback } from "react";
import styled from "styled-components";
import {getAvailColumns} from "../constants/defualtPageView"
import Icon from "./Icon";

const SelectForm = styled.div`
  display: grid;
  grid-template-columns: [first] 35% [line2] 65%;

  width: 100%;
  box-sizing: border-box;

  margin-top: 10px;
  margin-bottom: 10px;

  font-size: 14px;
  text-transform: capitalize;

  & > label {
    margin: 0;
    padding: 0;
    display: flex;
    align-items: center;

    text-transform: captalize;
  }
`;

const SelectBox = styled.div`
  width: 100%;
  box-sizing: border-box;

  position: relative;
  cursor: pointer;

  & svg path {
    
  }
`;

const SelectRow = styled.div`
  border: 1px solid #bdbdbd;
  border-radius: 15px;

  display: flex;
  align-items: center;

  padding-left: 1rem;
  padding-top: 0.3rem;
  padding-bottom: 0.3rem;
  padding-right: 1rem;

  background: #ffffff;

  &.active {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
  }

  :hover,
  &.active {
    border: 1px solid #333333;
  }

  & > svg:first-child {
    margin-right: 0.3rem;

    width: 15px;
    height: 15px;

    & > path {
      fill: #333333;
    }
  }
`;

const SelectCart = styled.span`
  position: absolute;
  right: 1rem;
`;

const SelectDetail = styled.div`
  background: #ffffff;

  position: absolute;
  top: auto;

  z-index: 1;

  box-shadow: 0px 9px 17px rgba(0, 0, 0, 0.25);

  border-bottom-left-radius: 15px;
  border-bottom-right-radius: 15px;

  width: 100%;
`;

const SelectOption = styled.div`
  border-top: none;

  padding-left: 1rem;
  padding-top: 0.4rem;
  padding-bottom: 0.4rem;
  padding-right: 1rem;

  position: relative;

  display: flex;
  align-items: center;

  text-transform: capitalize;

  :hover,
  &.active {
    background-color: #bdbdbd4d;
  }

  & > svg:first-child {
    margin-right: 0.3rem;

    width: 15px;
    height: 15px;

    & path {
      fill: #333333;
    }
    
  }

  &.active > svg:last-child {
    position:absolute;
    top: 50%;
    right: 10px;
    transform: translateY(-50%);
    

    margin-right: unset;
    width: unset;
    height: unset;
  }

  &.active > svg:last-child path {
    fill: #2ad27f;
  }
`;
const randText = Math.random();

const RSelect = React.memo(({ label, options, value, onChange,inputType }) => {
  const [isView, setView] = useState(false);

  useEffect(() => {
    document.body.addEventListener("click", bodyClickHandler);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const bodyClickHandler = (e) => {
    if (
      e.target.getAttribute("name") !== `rselect-option-item-${randText}` &&
      e.target.getAttribute("name") !== `rselect-${randText}`
    )
      setView(false);
  };

  const updateSelection = useCallback(
    (id) => {
      console.log(id);
      setView(false);
      onChange(id);
    },
    [onChange]
  );

  const isSelected = useCallback(
    (id) => {
      if (id === value) return <Icon name="check" />;
      return <></>;
    },
    [value]
  );

  const renderSelectedRow = useCallback(() => {
    if (!options) return <></>;
    const item = options.filter((option) => option.id === value);
    if (item && item.length === 1) {
      return (
        <>
          {item[0].icon ? <Icon name={item[0].icon} /> : ""}
          {item[0].label}
        </>
      );
    } else {
      return <></>;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [options, value]);

  const renderDetail = useCallback(() => {
    if (!(isView && options && options.length > 0)) {
      return <></>;
    }
    let filteredOptions;
    if(options[0].id === "none"){
       filteredOptions = inputType ? [options[0], ...getAvailColumns(options,inputType)] : options;
    }else {
       filteredOptions = inputType ? getAvailColumns(options,inputType) : options;
    }
    return (
      <SelectDetail>
        {filteredOptions.map((option, index) => (
          <SelectOption
            key={`select-option-${index}`}
            name={`rselect-option-item-${randText}`}
            className={option.id === value ? "active" : ""}
            onClick={() => updateSelection(option.id)}
          >
            {option.icon ? <Icon name={option.icon} /> : ""}
            {option.label}
            {isSelected(option.id)}
          </SelectOption>
        ))}
      </SelectDetail>
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isView, options, value]);

  return (
    <SelectForm style={{ display: label ? "grid" : "flex" }}>
      <label>{label}</label>
      <SelectBox name="rselect">
        <SelectRow
          name={`rselect-${randText}`}
          className={isView ? "active" : ""}
          onClick={() => setView(true)}
        >
          {renderSelectedRow()}
          <SelectCart>
            <Icon name="cart" />
          </SelectCart>
        </SelectRow>
        {renderDetail()}
      </SelectBox>
    </SelectForm>
  );
});

export default RSelect;
