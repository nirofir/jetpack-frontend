import React from "react";
import styled from "styled-components";

import Icon from "./Icon";
import {icons} from "layout/tabsLayout/icons";

const RDragDiv = styled.div`
  user-select: none;

  font-family: Segoe MDL2 Assets;
  font-style: normal;
  font-weight: normal;
  height: 30px;

  display: flex;
  align-items: center;
  border: none;

  margin-top: 10px;

  cursor: pointer;

  & > svg {
    margin-right: 0.5rem;
  }
  &>svg: first-child path {
    fill: #737383;
  }
`;

const RDragRegion = styled.div`
  width: 100%;
  height: 100%;

  border: 1px solid #bdbdbd;
  border-radius: 28px;

  display: flex;
  justify-content: space-between;

  color: #333333;

  box-sizing: border-box;

  & svg path {
    fill: #333333;
  }
`;

const BoldRegion = styled.div`
  flex: 1;
  color: #333333;

  width: 100%;
  display: flex;
  align-items: center;

  font-family: Open Sans;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;

  & > svg {
    margin: 10px;

    width: 16px;
    height: 16px;
  }
`;

const LightRegion = styled.div`
  flex: 4;
  display: flex;
  align-items: center;
  position: relative;

  font-weight: normal;
  font-family: Open Sans;
  font-style: normal;
  color: #333333;
  & svg {
    width: 12px;
    height: 12px;
  }

  & svg:hover {
    width: 14px;
    height: 14px;
  }

  & svg:hover path {
    fill: #000000;
  }

  & span:first-child {
    position: absolute;
    right: 40px;
  }

  & span:last-child {
    position: absolute;
    right: 15px;
  }
`;

const RDragBox = React.memo(({ name, label, icon, onSelect }) => {
  return (
    <RDragDiv onClick={() => onSelect()}>
      <Icon name="hamburger" />
      <RDragRegion>
        <BoldRegion>
          {icons()[icon].icon}
          {/* <Icon name={icon} /> */}
          {/* <Icon name={"Text"} /> */}
        </BoldRegion>
        <LightRegion> {label}</LightRegion>
      </RDragRegion>
    </RDragDiv>
  );
});

export default RDragBox;
