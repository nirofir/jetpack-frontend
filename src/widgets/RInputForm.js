import React from "react";
import styled from "styled-components";

const RBox = styled.div`
  display: grid;
  grid-template-columns: [first] 40% [line2] 60%;

  margin-top: 10px;
  margin-bottom: 10px;

  font-size: 14px;

  & > label {
    margin: 0;
    padding: 0;
    display: flex;
    align-items: center;

    text-transform: capitalize;
  }

  & .rc-slider-mark {
    display: none;
  }
`;

const RInput = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;

  padding-left: 10px;

  & > input[type="text"] {
    height: 30px;

    background: #ffffff;

    border: 1px solid #bdbdbd;
    box-sizing: border-box;
    border-radius: 28px;

    font-size: 14px;
    line-height: 19px;

    color: #333333;

    padding-left: 15px;
  }
`;

const RInputForm = React.memo(({ label, children }) => {
  return (
    <RBox>
      <label>{label}</label>
      <RInput>{children}</RInput>
    </RBox>
  );
});

export default RInputForm;
