import { CLIENT_ID, API_REDIRECT_URI, API_REDIRECT_URIMobile } from "constants/constants";
import moment from "moment";

import Icons from 'constants/namespaces/icons.json';

export const getAuthUrl = (boardId) => {
  return `https://auth.monday.com/oauth2/authorize?client_id=${CLIENT_ID}&redirect_uri=${API_REDIRECT_URI}&state=${boardId}`;
};

export const getAuthUrlMobile = (boardId, boardName) => {
  return `https://auth.monday.com/oauth2/authorize?client_id=${CLIENT_ID}&redirect_uri=${API_REDIRECT_URIMobile}&state=${boardId}_${boardName}`;
};

export const parseQuery = (queryString) => {
  var query = {};
  var pairs = (queryString[0] === "?"
    ? queryString.substr(1)
    : queryString
  ).split("&");
  for (var i = 0; i < pairs.length; i++) {
    var pair = pairs[i].split("=");
    query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || "");
  }
  return query;
};

export const getCookie = (cname) => {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

export const getProgressByTime = (from, to) => {
  const from_date = new Date(from).getTime();
  const to_date = new Date(to).getTime();
  const current_date = new Date().getTime();
  const percent =
    current_date >= to_date
      ? 100
      : current_date >= from_date
        ? parseInt(((current_date - from_date) / (to_date - from_date)) * 100, 10)
        : 0;
  return percent;
};

export const parseColumns = (data) => {
  const items = data.items;
  if (items.length === 0) return [];
  const columns = data.columns;
  let widgets = [
    {
      icon: "no_select",
      label: "",
      id: "none",
    },
  ];
  let column_JSON = {};

  for (const column of columns) {
    widgets.push({
      icon: Icons[column.type] || 'text',
      label: column.title,
      id: column.id,
      type: column.type,
      settings_str: JSON.parse(column.settings_str)
    });
    column_JSON = {
      ...column_JSON,
      [column.id]: column.title,

    };
  }
  return {
    columns: widgets,
    column_json: column_JSON
  };
};

export const getHeight = (shape) => {
  switch (shape) {
    case "horizontal3_1":
      return 355 / 3;
    case "horizontal3_2":
      return 355 / 1.5;
    case "horizontal4_3":
      return (355 * 3) / 4;
    case "vertical2_3":
      return (355 * 3) / 2;
    case "vertical3_4":
      return (355 * 4) / 3;
    default:
      return 355;
  }
};

export const getTextAlignment = (text_position, text_alignment) => {
  let style = {
    justifyContent: "flex-end",
    alignItems: "flex-start",
  };

  if (text_position) return style;

  switch (text_alignment) {
    case "top_left":
      style.justifyContent = "flex-start";
      return style;

    case "bottom_left":
      return style;

    case "bottom_right":
      style.alignItems = "flex-end";
      return style;

    case "top_right":
      style.alignItems = "flex-end";
      style.justifyContent = "flex-start";
      return style;

    case "align_center":
      style.alignItems = "center";
      style.justifyContent = "center";
      return style;

    default:
      return style;
  }
};

export const getFontStyle = (text_caps, text_size, font_size, line_height) => {
  let textTransform = text_caps ? "uppercase" : "";

  if (text_size === "small")
    return {
      fontSize: `${font_size}px`,
      lineHeight: `${line_height}px`,
      textTransform,
    };
  else if (text_size === "medium") {
    return {
      fontSize: `${font_size + 3}px`,
      lineHeight: `${line_height + 3}px`,
      textTransform,
    };
  }
  return {
    fontSize: `${font_size + 6}px`,
    lineHeight: `${line_height + 6}px`,
    textTransform,
  };
};

export const getDate = (items,when) => {
  const birthdays = items.sort((itemA, itemB) =>
    itemA[when].text > itemB[when].text
      ? 1
      : itemB[when].text > itemA[when].text
        ? -1
        : 0
  );
  return birthdays.map((item) => ({
    ...item,
    date4: {
      ...item[when],
      text: `${moment(item[when].text).format("dddd")}, ${moment(
        item[when].text
        ).format("LL")}`,
      time: item[when].value.time
    },
  }));
};

export const getZoom = (zoom) => {
  const zoomValues = {
    "near": 8,
    "medium": 6,
    "far": 3
  };
  return zoomValues[zoom];
}

export const getFilterFunction = ({ operator, column, filterBy }) => {
  switch (operator.filterType) {
    case "hasValue": {
      if (operator.id === "!!") {
        return (data) => {
          return data.filter(data => !!data[column].value)
        }
      } else if (operator.id === "!!!") {
        return (data) => {
          return data.filter(data => !data[column].value)
        }
      }
    }

    case "equal": {
      if (operator.id === "===") {
        return (data) => {
          return data.filter(data => data[column].value.toLowerCase() === filterBy.toLowerCase())
        }
      } else if (operator.id === "!==") {
        return (data) => {
          return data.filter(data => data[column].value.toLowerCase() !== filterBy.toLowerCase())
        }
      }
    }

    case "text": {
      if (operator.id === "contains") {
        return (data) => {
          return data.filter(data => data[column].value.includes(filterBy))
        }
      } else if (operator.id === "not contains") {
        return (data) => {
          return data.filter(data => !data[column].value.includes(filterBy))
        }
      }
    }

    case "tag": {
      if (operator.id === "ht") {
        console.log(operator, column, filterBy )
        return (data) => {
          return data.filter(data => data[column].text.toLowerCase() === filterBy.toLowerCase())
        }
      } else if (operator.id === "dht") {
        console.log(operator, column, filterBy )
        return (data) => {
          return data.filter(data => data[column].text.toLowerCase() !== filterBy.toLowerCase())
        }
      }
    }
    
    default:
      return;
  }
}
