import {
    fetchToken,
    fetchBoardData,
    getBoardData,
    getStyle
} from 'api/monday';

// export const initialize = async (boardId) => {
//     const tokenResp = (await fetchToken()).data;
//     console.log("FetchToken", tokenResp);

//     if (tokenResp.status === 200) {
//         console.log("Got token from database");
//         const token = tokenResp.data.token;
//         const boardResp = (await fetchBoardData(token,boardId)).data;
//         // const styleResp = (await getStyle()).data;

//         if (boardResp.status === 200) {
//             return {
//                 token,
//                 data: boardResp.data,
//                 style: styleResp.status === 200 ? styleResp.data : null
//             }
//         }
//         return {
//             token,
//             data: (await getBoardData()).data,
//             style: styleResp.status === 200 ? styleResp.data : null
//         }
//     }
//     return false;
// }