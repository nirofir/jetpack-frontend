export const toCamelCase = (str) => {
  return str
    .split(" ")
    .map(function (word, index) {
      // If it is the first word make sure to lowercase all the chars.
      if (index === 0) {
        return word.toLowerCase();
      }
      // If it is not the first word only upper case the first char and lowercase the rest.
      return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    })
    .join("");
};

export const serialize = (items, details) => {
  return items.map((item, index) => ({
    ...item,
    id: `${toCamelCase(item.name)}_${index}`,
    label: details[index].hint
      ? details[index][details[index].hint]
      : item.label,
  }));
};
