import React from "react";
import styled from "styled-components";

import Header from "./header";
import Body from "./body";
import Footer from "./footer";
import { connect } from "react-redux";

import Icon from "widgets/Icon";

const PhoneBox = styled.div`
  width: 375px;
  height: 700px; 
  background: #ffffff;
  margin-top:180px;
  margin-bottom:100px;

  box-shadow: 0px 9px 17px rgba(0, 0, 0, 0.25);
  border-radius: 50px;

  bottom: 5;
  color: #303655;
  direction: ltr;
  overflow: hidden;
  zoom : 0.8;

`;

const PhoneHeader = styled.div`
  height: 32px;

  padding-left: 20px;
  padding-right: 20px;

  display: flex;
  justify-content: space-between;
  
`;

const PhoneClock = styled.label`
  font-family: Cairo;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 22px;

  display: flex;
  justify-content: center;

  margin-top: 15px;
`;

const PhoneStatus = styled.div`
  width: 70px;

  margin-top: 15px;

  display: flex;
  justify-content: space-between;
`;

const PhoneBody = styled.div`
  height: calc(100% - 80px);
  margin: 5px;

  position: relative;

  overflow: hidden;

  display: flex;
  flex-direction: column;
  overflow: hidden;

  background: ${props => props.iphoneColor};
`;

const PhoneFooter = styled.div`
  bottom: 10px;
  width: 100%;

  display: flex;
  justify-content: center;
  align-items: center;
  
`;

const PhoneDivider = styled.div`
  width: 135px;
  height: 5px;

  background: #000000;
  mix-blend-mode: normal;
  opacity: 0.14;

  border-radius: 100px;
`;

const App = React.memo(({ iphoneColor,isHigh }) => {
  return (
    <PhoneBox isHigh={isHigh}>
      <PhoneHeader>
        <PhoneClock>9:41</PhoneClock>
        <Icon name="iphone_header" />

        <PhoneStatus>
          <Icon name="connection" />
          <Icon name="wifi" />
          <Icon name="battery" />
        </PhoneStatus>
      </PhoneHeader>

      <PhoneBody iphoneColor={iphoneColor} >
        <Header />
        <Body />
        <Footer />
      </PhoneBody>

      <PhoneFooter>
        <PhoneDivider />
      </PhoneFooter>
    </PhoneBox>
  );
});

const mapStateToProps = (state) => ({
  iphoneColor: state.StyleReducer.iphoneColor,
});


export default connect(mapStateToProps, null)(App);
// export default App;
