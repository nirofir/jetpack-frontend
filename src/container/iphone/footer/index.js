import React from "react";
import styled from "styled-components";

import FooterBtn from "./components/FooterBtn";
import { connect } from "react-redux";

const FooterBox = styled.div`

  display: flex;
  justify-content: space-between;

  padding-top: 1rem;
  padding-bottom: 1rem;
  background: ${props => props.iphoneColorFoot};
`;
const Footer = React.memo((props) => {
  return (
    <FooterBox iphoneColorFoot={props.iphoneColorFoot}>
      {props.titles.map((title, index) => (
        <FooterBtn key={index} title={title} index={index} icon={props.icons[index]}></FooterBtn>
      ))}
    </FooterBox>
  );
});

const mapStateToProps = (state) => ({
  titles: state.StyleReducer.tabs ? state.StyleReducer.tabs.map((tab) => tab.title) : [],
  icons: state.StyleReducer.tabs ? state.StyleReducer.tabs.map((tab) => tab.icon) : [],
  iphoneColorFoot: state.StyleReducer.iphoneColorFoot,
});

export default connect(mapStateToProps, null)(Footer);
