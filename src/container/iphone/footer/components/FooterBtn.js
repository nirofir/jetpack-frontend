import React  from "react";
import styled from "styled-components";
import { changeTab } from "redux/style";
import { changeLayoutPage ,goBackLayoutPage} from "redux/layout";

import { connect } from "react-redux";

import {icons} from "layout/tabsLayout/icons";


const FooterTab = styled.div`
  width: 100%;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 16px;
  display: flex;
  align-items: center;
  text-align: center;

  cursor: pointer;
`;

const TabIcon = (props) => {
  const object  = Object.entries(icons({width:"24px", height:"30px", fill: props.color}))
  .filter(([key, value]) => key === props.icon)[0][1];
  return <>{object.icon }</>
}


const FooterBtn = React.memo((props) => {
  const color = props.currentTab === props.index ? "#F62765" : "#737383";

  const clickOnTab = () => {
    if(props.layoutPage === "edit_component" || props.layoutPage === "new_component"){
      props.goBackLayoutPage()
    }
    props.changeTab(props.index);
  };
  return (
    <FooterTab onClick={() => clickOnTab(props.index)}>
      <span style={{color:color}}>{props.title}</span>
      <TabIcon icon={props.icon} color={color}></TabIcon>
    </FooterTab>
  );
});

const mapStateToProps = (state) => ({
  currentTab: state.StyleReducer.currentTab,
  layoutPage : state.LayoutReducer.layoutPage
});

const mapDispatchToProps = (dispatch) => ({
  changeTab: (tabIndex) => dispatch(changeTab(tabIndex)),
  goBackLayoutPage: () => dispatch(goBackLayoutPage()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FooterBtn);
