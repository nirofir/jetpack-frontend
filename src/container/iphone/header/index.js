import React, { useCallback } from "react";
import styled from "styled-components";

import { connect } from "react-redux";
// import { goBackAppPage } from "redux/app";
import { toggleComponentView } from "redux/style";
import { getFilterFunction } from "../../../helper"

import Icon from "widgets/Icon";

const HeaderBox = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 25px;
  color: ${props => props.iphoneColorHeadFont};

  padding-top: 20px;
  padding-bottom: 20px;

  position: relative;

  display: flex;
  justify-content: center;
  background: ${props => props.iphoneColorHead};
`;

const LeftControl = styled.span`
  & svg {
    position: absolute;
    left: 35px;
    top: 50%;
    transform: translateY(-50%);

    cursor: pointer;
  }

  & svg path {
    fill: #f62765;
  }
`;
/*
const RightControl = styled.span`
  & svg {
    position: absolute;
    right: 35px;
    top: 50%;
    transform: translateY(-50%);

    cursor: pointer;
  }

  & svg path {
    fill: #f62765;
  }
`;
*/

const Header = React.memo((props) => {
  const { componentView, headerTitle,appHeaderTitle,appHeaderCustomTitle, mondayData, iphoneColorHead, iphoneColorFont, iphoneColorHeadFont } = props;
  const { toggleComponentView } = props;

  const renderTitle = () => {
    if (appHeaderTitle === "none" || !componentView) return headerTitle;
    if (appHeaderTitle === "custom") return appHeaderCustomTitle;
    const value = mondayData.items[mondayData.itemIndex][appHeaderTitle].text;
    if (typeof value === "string") return value;
    return "";
  };

  return (
    <HeaderBox iphoneColorHeadFont={iphoneColorHeadFont} iphoneColorHead={iphoneColorHead} iphoneColorFont={iphoneColorFont}>
      {componentView ? (
        <LeftControl onClick={() => toggleComponentView()}>
          <Icon name="back" />
        </LeftControl>
      ) : (
          <LeftControl>
            <Icon name="alert" />
          </LeftControl>
        )}
      <label>{renderTitle()}</label>
    </HeaderBox>
  );
});

const getFilteredMondayData = (mondayData, filterBy, sortBy) => {
  const filterFunc = filterBy !== "none" ? getFilterFunction(filterBy) : "none"
  const filteredItems = filterFunc !== "none" ? filterFunc(mondayData.items) : mondayData.items;

  const sortedItems = sortBy !== "none" ? sortBy(mondayData.items) : filteredItems;
  return {
    ...mondayData,
    items: sortedItems
  }
}

const mapStateToProps = (state) => ({
  layoutPage: state.LayoutReducer.layoutPage,
  componentView: state.StyleReducer.componentView,
  headerTitle : state.StyleReducer.tabs[state.StyleReducer.currentTab].title,
  appHeaderTitle: state.StyleReducer.tabs
    ? state.StyleReducer.tabs[state.StyleReducer.currentTab].property.appHeaderTitle
    : "loading",
  appHeaderCustomTitle: state.StyleReducer.tabs
    ? state.StyleReducer.tabs[state.StyleReducer.currentTab].property.appHeaderCustomTitle
    : "loading",
    mondayData: getFilteredMondayData(state.MondayReducer,
      state.StyleReducer.tabs[state.StyleReducer.currentTab].property.filter_by,
      state.StyleReducer.tabs[state.StyleReducer.currentTab].property.sort_by),
  iphoneColorHead: state.StyleReducer.iphoneColorHead,
  iphoneColorFont: state.StyleReducer.iphoneColorFont,
  iphoneColorHeadFont: state.StyleReducer.iphoneColorHeadFont,
});
const mapDispatchToProps = (dispatch) => ({
  toggleComponentView: () => dispatch(toggleComponentView()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
