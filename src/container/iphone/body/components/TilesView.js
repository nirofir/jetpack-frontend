import React, { useCallback, useState } from "react";
import styled from "styled-components";
import RSearchBar from '../../../../widgets/RSearchBar';
import { connect } from 'react-redux';
import { getHeight, getFontStyle, getTextAlignment } from "helper";

const TilesViewBox = styled.div`
  font-family: Open Sans;
  font-style: normal;

  overflow-y: scroll;
  height: calc(100% - 120px);

  margin-right: -17px;
  height: 100%;

  text-transform: capitalize;
`;

const TilesViewItem = styled.div`
  padding-left: 10px;
  padding-right: 10px;
  margin-bottom: 2rem;

  cursor: pointer;

  display: inline-block;
  width: calc(25% - 20px);
`;

const TilesViewItemAvatar = styled.div`
  overflow: hidden;

  width: 100%;

  background-repeat: no-repeat;
  background-size: cover;
`;

const TilesViewItemTitle = styled.p`
  margin: 0;
  padding: 0;

  font-weight: 600;
  font-size: 18px;
  line-height: 25px;
  color: #303655;

  text-align: center;

  height: 25px;
  white-space: nowrap;
  overflow: hidden;
`;

const TilesViewItemDetail = styled.p`
  margin: 0;
  padding: 0;

  font-weight: 600;
  font-size: 14px;
  line-height: 19px;
  color: #737383;

  text-align: center;

  height: 25px;
  white-space: nowrap;
  overflow: hidden;
`;

const TilesViewAvatar = styled.div`
  background-repeat: no-repeat;
  background-size: cover;

  overflow: hidden;

  width: 100%;

  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;

  color: #ffffff;

  position: relative;

  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;

const TilesViewBack = styled.div`
  position: absolute;
  height: 50%;
  width: 100%;
  bottom: 0;
  background: linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, #000000 100%);
`;

const TilesViewTitle = styled.p`
  margin: 0;
  padding: 0;
  padding-left: 12px;
  padding-right: 12px;

  font-size: 18px;
  line-height: 25px;

  bottom: 13px;

  height: 25px;
  white-space: nowrap;
  overflow: hidden;

  z-index: 1;
`;

const TilesViewDetail = styled.p`
  margin: 0;
  padding: 0;
  padding-left: 12px;
  padding-right: 12px;

  font-size: 14px;
  line-height: 19px;

  bottom: 30px;

  height: 25px;
  white-space: nowrap;
  overflow: hidden;

  z-index: 1;
`;

const TilesView = React.memo((props) => {
  const { items, propertyData, iphoneColorFont, iphoneColor, hasSearchBar } = props;
  const { selectItem, changeLayoutPage } = props;
  const [search, setSearch] = useState(null);


  const filterPersonHandler = event => {

    setSearch(event.target.value.toLowerCase());
    //items.filter(checkSearch);
  }
  const searchBar = <RSearchBar
    filterPersonHandler={filterPersonHandler}
    magnifingColor={iphoneColorFont}
    backgroundColor1={iphoneColor}
    placeHolderSearchColor={iphoneColorFont}
  />;

  const styles = {
    layout: {
      width: `calc(${100 / propertyData.tiles_in_row}% - ${
        propertyData.padding ? 0 : 10
        }px)`,
      paddingLeft: `${propertyData.padding ? 0 : 5}px`,
      paddingRight: `${propertyData.padding ? 0 : 5}px`,
    },
    avatar: {
      backgroundPosition: `${propertyData.crop ? "" : "center"}`,
      height: `${getHeight(propertyData.shape) / propertyData.tiles_in_row}px`,
      borderRadius:
        propertyData.shape === "circle"
          ? "50%"
          : propertyData.corner
            ? "0"
            : "5px",
    },
    avatarDiv: getTextAlignment(
      propertyData.text_position,
      propertyData.text_alignment
    ),
    titleText: getFontStyle(
      propertyData.text_caps,
      propertyData.text_size,
      15,
      22
    ),
    detailText: getFontStyle(
      propertyData.text_caps,
      propertyData.text_size,
      11,
      16
    ),
  };

  const onViewDetail = useCallback((index) => {
    selectItem && selectItem(index);
    selectItem && changeLayoutPage("component");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderItem = useCallback(
    (item) => {
      if (propertyData.shape === "circle") {
        console.log(item[propertyData.image])
        return (
          <>

            <TilesViewItemAvatar
              style={{
                ...styles.avatar,
                backgroundImage: `url(${item[propertyData.image] && item[propertyData.image].text})`,
              }}
            ></TilesViewItemAvatar>
            <TilesViewItemTitle style={styles.titleText}>
              {item[propertyData.title] && item[propertyData.title].text && typeof item[propertyData.title].text === "string"
                ? item[propertyData.title].text
                : ""}
            </TilesViewItemTitle>
            <TilesViewItemDetail style={styles.detailText}>
              {item[propertyData.details] && item[propertyData.details].text && typeof item[propertyData.details].text === "string"
                ? item[propertyData.details].text
                : ""}
            </TilesViewItemDetail>
          </>
        );
      } else {
        return (
          <>

            <TilesViewAvatar
              style={{
                ...styles.avatar,
                ...styles.avatarDiv,
                backgroundImage: `url(${item[propertyData.image] && item[propertyData.image].text})`,
              }}
            >
              <TilesViewBack />
              <TilesViewTitle style={styles.titleText}>
                {item[propertyData.title] && item[propertyData.title].text && typeof item[propertyData.title].text === "string"
                  ? item[propertyData.title].text
                  : ""}
              </TilesViewTitle>
              <TilesViewDetail style={styles.detailText}>
                {item[propertyData.details] && item[propertyData.details].text && typeof item[propertyData.details].text === "string"
                  ? item[propertyData.details].text
                  : ""}
              </TilesViewDetail>
            </TilesViewAvatar>
          </>
        );
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [propertyData]
  );

  return (<>
    {hasSearchBar === true ? (

      <TilesViewBox>
        {searchBar}
        {items &&
          items.map((item, index) => {
            if (item.name.value.toLowerCase().includes(search) || !search) {
              return (
                <TilesViewItem
                  key={`tiles-view-${index}`}
                  onClick={() => onViewDetail(index)}
                  style={styles.layout}
                >
                  {renderItem(item)}
                </TilesViewItem>
              )
            }
          })}
      </TilesViewBox>
    ) : (

        <TilesViewBox>

          {items &&
            items.map((item, index) => (
              <TilesViewItem
                key={`tiles-view-${index}`}
                onClick={() => onViewDetail(index)}
                style={styles.layout}
              >
                {renderItem(item)}
              </TilesViewItem>
            ))}
        </TilesViewBox>
      )}
  </>);
});
const mapStateToProps = (state) => ({
  iphoneColorFont: state.StyleReducer.iphoneColorFont,
  iphoneColor: state.StyleReducer.iphoneColor,
  hasSearchBar: state.StyleReducer.tabs[state.StyleReducer.currentTab].property.hasSearchBar

});

export default connect(mapStateToProps, null)(TilesView);
