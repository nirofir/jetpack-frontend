import React, { useCallback } from "react";
import ListView from "./ListView";
import CompactView from "./CompactView";
import TilesView from "./TilesView";
import CalendarView from "./CalendarView";
import MapView from "./MapView";

const ListComponent = ({ mondayData, propertyData, selectItem, toggleComponentView, updateProperty, detailsData, isInline }) => {

  switch (propertyData.style) {
    case "list":
      return (
        <ListView
          isInline={isInline}
          items={mondayData.items}
          propertyData={propertyData}
          selectItem={selectItem}
          changeLayoutPage={toggleComponentView}
        />
      );
    case 0:
      return (<ListView
        isInline={isInline}

        items={mondayData.items}
        propertyData={propertyData}
      />)
    case "compact":
      return (

        <CompactView
          isInline={isInline}

          items={mondayData.items}
          propertyData={propertyData}
          selectItem={selectItem}
          changeLayoutPage={toggleComponentView}
        />
      );
    case 1:
      return (
        <CompactView
          isInline={isInline}

          items={mondayData.items}
          propertyData={propertyData}
        />
      );
    case "tiles":
      return (
        <TilesView
          isInline={isInline}
          items={mondayData.items}
          propertyData={propertyData}
          selectItem={selectItem}
          changeLayoutPage={toggleComponentView}
        />
      );
    case 2:
      return (
        <TilesView
          isInline={isInline}
          items={mondayData.items}
          propertyData={propertyData}
        />
      )
    case "calendar":
      return (
        <CalendarView
          items={mondayData.items}
          propertyData={propertyData}
          selectItem={selectItem}
          changeLayoutPage={toggleComponentView}
          isInline={isInline}

        />
      );
    case 3:
      return (
        <CalendarView
          items={mondayData.items}
          propertyData={propertyData}
          isInline={isInline}

        />
      )
    case "map":
      return (
        <MapView
          items={mondayData.items}
          propertyData={propertyData}
          selectItem={selectItem}
          changeLayoutPage={toggleComponentView}
          updateProperty={updateProperty}
          isInline={isInline}

        />
      );
    // case 4:
    //   return(
    //     <MapView
    //     items={mondayData.items}
    //     propertyData={propertyData}
    //     // selectItem={selectItem}
    //     // changeLayoutPage={toggleComponentView}
    //     // updateProperty={updateProperty}
    //   /> 
    //   )
    default:
      return <></>;
  }
}

export default ListComponent;