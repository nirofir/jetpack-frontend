import React, { useCallback, useState } from "react";
import styled from "styled-components";
import RSearchBar from '../../../../widgets/RSearchBar';
import { connect } from 'react-redux';
import Icon from "widgets/Icon";

const CompactViewBox = styled.div`
  padding-left: 20px;
  padding-right: 20px;

  overflow-y: scroll;

  margin-right: -17px;
  height: 100%;

  text-transform: capitalize;
`;

const CompactItem = styled.div`
  display: flex;
  justify-content: space-between;

  font-family: Open Sans;
  font-weight: 600;
  font-size: 16px;
  line-height: 25px;
  color: #303655;

  padding-left: 40px;
  padding-right: 20px;
  padding-top: 15px;
  padding-bottom: 15px;

  position: relative;

  border-top: 1px solid #c4c4c4;
  
  cursor: pointer;

  :last-child {
    border-bottom: 1px solid #c4c4c4;
  }

  & > p {
    padding: 0;
    margin: 0;

    display: flex;
    align-items: center;
  }

  & > svg {
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);

    text-align: right;

    color: #737383;
    width: 14px;
    height: 14px;
  }
`;

const CompactItemAvatar = styled.div`
  width: 30px;
  height: 30px;

  position: absolute;

  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  left: 0px;
  top: 50%;
  transform: translateY(-50%);

  border-radius: 50%;
`;

const CompactViewInfo = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;

  font-size: 14px;
  line-height: 19px;
  text-align: right;

  color: #737383;

  width: 100px;
`;

const CompactView = React.memo((props) => {
  const { items, propertyData, iphoneColorFont, iphoneColor, hasSearchBar } = props;
  const { selectItem, changeLayoutPage } = props;
  const [search, setSearch] = useState(null);


  const filterPersonHandler = event => {

    setSearch(event.target.value.toLowerCase());
    //items.filter(checkSearch);
  }
  const onViewDetail = useCallback((index) => {

    selectItem && selectItem(index);
    selectItem && changeLayoutPage("component");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const searchBar = <RSearchBar
    filterPersonHandler={filterPersonHandler}
    magnifingColor={iphoneColorFont}
    backgroundColor1={iphoneColor}
    placeHolderSearchColor={iphoneColorFont}
  />;
  const compactViewBox = <CompactViewBox>

    {items.map((item, index) => {
      if (item.name.value.toLowerCase().includes(search) || !search) {
        return (
          <CompactItem
            key={`compact-item-${index}`}
            onClick={() => onViewDetail(index)}
            style={{ color: iphoneColorFont }}
          >
            {propertyData.image !== "none" &&
              <CompactItemAvatar
                style={{

                  backgroundImage: `url(${item[propertyData.image] && item[propertyData.image].text})`,
                }}
              ></CompactItemAvatar>
            }

            <p>
              {item[propertyData.title] && typeof item[propertyData.title].text === "string"
                ? item[propertyData.title].text
                : ""}
            </p>
            <CompactViewInfo
              style={{ color: iphoneColorFont }}>
              {item[propertyData.details] && typeof item[propertyData.details].text === "string"
                ? item[propertyData.details].text
                : ""}
            </CompactViewInfo>
            <Icon name="arrow_right" />
          </CompactItem>
        )
      }
    })}
  </CompactViewBox>
  return (<>
    {hasSearchBar === true ? (
      <>
        {searchBar}
        {compactViewBox}
      </>
    ) : (
        <>

          {compactViewBox}
        </>
      )}

  </>
  );
});

const mapStateToProps = (state) => ({
  iphoneColorFont: state.StyleReducer.iphoneColorFont,
  iphoneColor: state.StyleReducer.iphoneColor,
  hasSearchBar: state.StyleReducer.tabs[state.StyleReducer.currentTab].property.hasSearchBar

});

export default connect(mapStateToProps, null)(CompactView);
