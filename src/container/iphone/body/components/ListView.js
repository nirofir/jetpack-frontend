import React, { useCallback, useState, useEffect } from "react";
import styled from "styled-components";
import { connect } from 'react-redux';
import RSearchBar from "widgets/RSearchBar";
const ListViewBox = styled.div`
  padding-left: ${props => props.isInline ? "0" : "20px;"};

  padding-right: 20px;

  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 25px;
  color: ${props => props.iphoneColorFont};
  overflow-y: ${props => props.isInline ? "none" : "scroll"};
  


  margin-right: -17px;
  height: 100%;

  text-transform: capitalize;
`;
const ListItem = styled.div`
  border-top: 1px solid #c4c4c4;

  width: 100%;
  padding-top: 10px;
  padding-bottom: 10px;

  display: flex;
  justify-content: space-between;

  font-family: Open Sans;
  font-style: normal;

  cursor: pointer;
  color: ${props => props.iphoneColorFont};
  position: relative;

  :last-child {
    border-bottom: 1px solid #c4c4c4;
  }

  p {
    margin: 0;
  }
`;

const ListItemInfo = styled.div`
  justify-content: center;
  display: flex;
  color : ${props => props.iphoneColorFont};
  flex-direction: column;
`;
const ListItemAvatar = styled.div`
  width: 60px;
  min-width : 60px;
  height: 60px;

  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  border-radius: 50%;
`;
const FontWeightBold = styled.p`
  font-weight: bold;
  
`;
const FontMuted = styled.p`
  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 19px;

  display: flex;
  align-items: center;

  
`;

const ListView = React.memo((props) => {
  const { items, propertyData, iphoneColorFont, iphoneColor, isInline, hasSearchBar } = props;
  const { selectItem, changeLayoutPage } = props;

  const onViewDetail = useCallback((index) => {
    selectItem && selectItem(index);
    selectItem && changeLayoutPage("component");

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);



  const [search, setSearch] = useState(null);


  const filterPersonHandler = event => {

    setSearch(event.target.value.toLowerCase());
    //items.filter(checkSearch);
  }
  const listViewBox = <ListViewBox isInline={isInline} >
    {items &&
      items.map((item, index) => {

        if (item.name.value.toLowerCase().includes(search) || !search) {

          return (
            <ListItem
              key={`list-item-${index}`}
              onClick={() => onViewDetail(index)}
            >
              <ListItemInfo>
                <div>
                  <FontWeightBold style={{ color: props.iphoneColorFont }}>
                    {item[propertyData.title] && typeof item[propertyData.title].text === "string"
                      ? item[propertyData.title].text
                      : ""}
                  </FontWeightBold>
                  <FontMuted style={{ color: props.iphoneColorFont }}>
                    {item[propertyData.details] && typeof item[propertyData.details].text === "string"
                      ? item[propertyData.details].text
                      : ""}
                  </FontMuted>
                  <FontMuted style={{ color: props.iphoneColorFont }}>
                    {item[propertyData.caption] && typeof item[propertyData.caption].text === "string"
                      ? item[propertyData.caption].text
                      : ""}
                  </FontMuted>
                </div>
              </ListItemInfo>
              <ListItemAvatar
                style={{
                  backgroundImage: `url(${item[propertyData.image] && item[propertyData.image].text})`,
                }}
              ></ListItemAvatar>
            </ListItem>
          )
        }
      })}
  </ListViewBox>
  return (
    hasSearchBar === true ? (
      <>
        {!isInline &&
          <RSearchBar
            filterPersonHandler={filterPersonHandler}
            magnifingColor={iphoneColorFont}
            backgroundColor1={iphoneColor}
            placeHolderSearchColor={iphoneColorFont}
          />}
        {listViewBox}

      </>
    ) : (
        <>
          {listViewBox}

        </>
      )
  );
});




const mapStateToProps = (state) => ({
  iphoneColorFont: state.StyleReducer.iphoneColorFont,
  iphoneColor: state.StyleReducer.iphoneColor,
  hasSearchBar: state.StyleReducer.tabs[state.StyleReducer.currentTab].property.hasSearchBar

});
export default connect(mapStateToProps, null)(ListView);
