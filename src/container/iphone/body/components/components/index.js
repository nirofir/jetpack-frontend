import React, { useEffect, useState } from "react";
import mapboxgl from "mapbox-gl";
import moment from 'moment';

import {
  //STATUS_VALUES,
  STATUS_VALUES2,
  RATING_VALUES,
  LIKE_VALUES,
  MONTHS,
  MAPBOX_API_KEY,
  VOTE_ICONS_LIST
} from "constants/constants";
import { getGeometry } from "api/mapbox";
import { getProgressByTime, getZoom } from "helper";

import Icon from "widgets/Icon";

import { TEXT_ALIGNMENTS } from "constants/constants";
import indicator from "assets/img/vector.png";
import FontStyles from 'constants/namespaces/fonts.json';
import "./_style.css";
import DatePicker from "react-datepicker";
//import Modal from 'react-modal'

import "react-datepicker/dist/react-datepicker.css";
mapboxgl.accessToken = MAPBOX_API_KEY;

// const fetchBoardData = async ({ token, boardId, columnId, itemId, update }) => {
//     await  updateField({ token, boardId, columnId , itemId, update})
//     const res = await getBoardData(token, boardId);

//     const data = parseColumns(res.data);
//     console.log(data);
//     fetchData({
//       items: res.data.items.map((item, index) => ({ ...item, index })),
//       columns: data.columns,
//       column_json: data.column_json,
//       boardId: boardId,
//     });
//   }

const Vid = ({ youtube }) => {
  const queryString = youtube && youtube.split('?')[1]
  const urlParams = new URLSearchParams(queryString);
  const youtubeId = urlParams.get('v')

  console.log(youtubeId)
  return (
    <div
      className="video"
      style={{
        position: "relative",
        paddingBottom: "56.25%" /* 16:9 */,
        paddingTop: 25,
        height: 0
      }}
    >
      <iframe title={"unique"}
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%"
        }}
        src={`https://www.youtube.com/embed/${youtubeId}`}
        frameBorder="0"
      />
    </div>
  );
};

export const VideoComponent = (props) => {
  return (
    <div className="app-component-card">
      <label>{props.title}</label>
      <Vid youtube={props.link} />
    </div>
  )
}

export const ActionTextComponent = React.memo(({ email, title, action, data, changeTab, color }) => {
  const onButtonClickHandler = () => {
    switch (action) {
      case 'open_webview':
      case 'open_link':
      case 'show_sharing':
        window.location.href = data.includes('http') ? data : `http://${data}`;
        return;
      case "duplicate":
        const textArea = document.createElement("textarea");
        textArea.value = data;
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("Copy");
        textArea.remove();
        return;
      case "show_map":
        window.location.href = `https://www.google.com/maps/search/?api=1&query=${data}`;
        return;
      case "link_screen":
        console.log(data)
        changeTab(data)
        return
      default: return;
    }
  }
  const getLink = () => {
    switch (action) {
      case 'send_email':
        return `mailto:${data}`
      case "dial_phone":
        return `tel:${data}`
      case 'send_message':
        return `sms://${data}`;
      default:
        // eslint-disable-next-line no-script-url
        return "javascript:;";
    }
  }
  return (

    <div className="app-component-card">
      <label style={{ color: color }} >{title}</label>
      <div className="d-flex justify-content-between mb-2">
        <p style={{ color: color }} className="normal-text">{email}</p>
        <a href={getLink()} className="component-button" onClick={() => onButtonClickHandler()}>
          <Icon name={action} />
        </a>
      </div>
    </div>
  );
});
export const ButtonComponent = React.memo(({ text, style, action, data, changeTab }) => {

  const styles = {
    background: `${style < 2 ? "#f62765" : "transparent"}`,
    border: `${style === 2 ? "1px solid #f62765" : "none"}`,
    color: `${style === 0 ? "#ffffff" : style === 1 ? "#333333" : "#f62765"}`,
  };
  const onButtonClickHandler = () => {

    console.log(action, data)
    switch (action) {
      case 'open_webview':
      case 'open_link':
      case 'show_sharing':
        window.location.href = data.includes('http') ? data : `http://${data}`;
        return;
      case "duplicate":
        const textArea = document.createElement("textarea");
        textArea.value = data;
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("Copy");
        textArea.remove();
        return;
      case "show_map":
        window.location.href = `https://www.google.com/maps/search/?api=1&query=${data}`;
        return;
      case "link_screen":
        changeTab(data)
        return
      default: return;
    }
  }
  const getLink = () => {
    switch (action) {
      case 'send_email':
        return `mailto:${data}`
      case "dial_phone":
        return `tel:${data}`
      case 'send_message':
        return `sms://${data}`;
      default:
        // eslint-disable-next-line no-script-url
        return "javascript:;";
    }
  }
  return (
    <div className="mb-5">

      <div>

      </div>
      <a href={getLink()} >
        <button className="large-component-button" style={styles} onClick={() => onButtonClickHandler()}>

          {text}
        </button>
      </a>
    </div >
  );
});
export const ImageComponent = React.memo(({ property, image_height, fill, style, crop, action, data, changeTab }) => {
  const onButtonClickHandler = () => {

    console.log(action, data,property)
    switch (action) {
      case 'open_webview':
      case 'open_link':
      case 'show_sharing':
        window.location.href = data.includes('http') ? data : `http://${data}`;
        return;
      case "duplicate":
        const textArea = document.createElement("textarea");
        textArea.value = data;
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("Copy");
        textArea.remove();
        return;
      case "show_map":
        window.location.href = `https://www.google.com/maps/search/?api=1&query=${data}`;
        return;
      case "link_screen":
        console.log(data)
        changeTab(data)
        return
      default: return;
    }
  }
  const getLink = () => {
    switch (action) {
      case 'send_email':
        return `mailto:${data}`
      case "dial_phone":
        return `tel:${data}`
      case 'send_message':
        return `sms://${data}`;
      default:
        // eslint-disable-next-line no-script-url
        return "javascript:;";
    }
  }
  const styles = {
    backgroundImage: `url(${property})`,
    backgroundSize: fill === "fill_area" ? "cover" : "contain",
    backgroundPosition: crop ? "center" : "",
    marginLeft: style === 'insert_content' ? 0 : '-20px',
    width: style === 'insert_content' ? "100%" : "calc(100% + 40px)",
    height: image_height === 3 ? `327px` : `${300 / (image_height + 1)}px`,
  };
  return (
    <div className="app-component-card">
      <a href={getLink()}>
        <div className="large-property-img mb-5" style={styles} onClick={() => onButtonClickHandler()} />
      </a>
    </div>
  );
});

export const TextComponent = React.memo((props) => {
  const {
    style,
    align,
    is_all_caps,
    // is_trancated,
    label,
  } = props;
  const fontStyle = {
    textAlign: TEXT_ALIGNMENTS[align].label,
    textTransform: is_all_caps ? "uppercase" : "",
  };

  return (
    <div className="app-component-card">
      <p className="component-text" style={{ ...fontStyle, ...FontStyles[style] }}>
        {label}
      </p>
    </div>
  );
});

export const TitleComponent = React.memo(({ name, color, company, avatar }) => {
  return (
    <div className="app-component-card">
      <div
        className="large-avatar mb-5"
        style={{ backgroundImage: `url(${avatar})` }}
      >
        <div className="avatar-info">
          <p style={{ color: color }}>{company}</p>
          <p style={{ color: color }}>{name} </p>
        </div>
      </div>
    </div>
  );
});



export const SeparatorComponent = React.memo(() => (
  <div className="w-100 separator mb-5"></div>
));



export const MapComponent = React.memo(({ location, zoom }) => {
  const [mapLocation, setLocation] = useState({
    lng: 5,
    lat: 34,
    zoom: 6,
  });

  useEffect(() => {
    const map = new mapboxgl.Map({
      container: "myContainer",
      style: "mapbox://styles/mapbox/streets-v11",
      center: [mapLocation.lng, mapLocation.lat],
      zoom: getZoom(zoom),
    });

    const mapMarker = new mapboxgl.Marker()
      .setLngLat([mapLocation.lng, mapLocation.lat])
      .addTo(map);

    map.on("move", () => {
      setLocation({
        lng: map.getCenter().lng.toFixed(4),
        lat: map.getCenter().lat.toFixed(4),
        zoom: map.getZoom().toFixed(2),
      });
    });

    getGeometry(location).then((res) => {
      const lng = res.data.features[0].center[0].toFixed(4),
        lat = res.data.features[0].center[1].toFixed(4);
      setLocation({
        ...location,
        lng,
        lat,
      });
      map.flyTo({
        center: [lng, lat],
      });
      mapMarker.setLngLat([lng, lat]);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location, zoom]);
  return (
    <div className="app-component-card">
      <div id="myContainer" className="mapContainer" />
    </div>
  );
});

export const LinkComponent = React.memo(({ link, label, title, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <div className="d-flex justify-content-between">
      <p style={{ color: color }} className="normal-text">{link}</p>
      <a href={link.includes('http') ? link : `http://${link}`} className="component-button">
        <Icon name="link" />
      </a>
    </div>
  </div>
));

export const PhoneComponent = React.memo(({ phone, body, title, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <div className="d-flex justify-content-between">
      <p style={{ color: color }} className="normal-text">+{phone} </p>
      <a href={`tel:${phone}`} className="component-button">
        <Icon name="phone" />
      </a>
    </div>
  </div>
));

export const DateComponent = React.memo(({ date, title, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <div className="entry-row-form">
      <p style={{ color: color }}>{date}</p>
      <div className="form-icon">
        <Icon name="calendar" />
      </div>
    </div>
  </div>
));

export const DateTimeComponent =
  // React.memo(
  ({ title, date, color, format, token, boardId, columnId, itemId, fetchBoardData }) => {
    const [startDate, setStartDate] = useState(new Date(date));

    const ExampleCustomInput = ({ date, onClick }) => (
      <p style={{ color: color }} className="normal-text" onClick={onClick}>
        {moment(date).format(format)}
      </p>
    );

    const handleChange = (date) => {
      setStartDate(date);
      fetchBoardData({ token, boardId, columnId, itemId, update: { date: date } })
      console.log("update")
    }

    return (
      <div className="app-component-card">
        <label style={{ color: color }}>{title}</label>
        <br />
        <DatePicker
          selected={startDate}
          onChange={date => handleChange(date)}
          customInput={<ExampleCustomInput date={date} />}
        />
      </div>
    );
  }
// )

export const CommentComponent = React.memo((color) => (
  <div className="app-component-card">
    <label style={{ color: color }}>Comments</label>
    <div className="d-flex">
      <button className="component-button black-color">
        <Icon name="user" />
      </button>
      <button className="component-link">
        <Icon name="plus" />
        &nbsp;&nbsp; Add Comment
      </button>
    </div>
  </div>
));

export const TextEntryComponent = React.memo(({ text, title, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <p className="component-text">{text}</p>
  </div>
));

export const NoteComponent = React.memo(({ text, title, color }) => (
  <div className="app-component-card disabled pl-2">
    <label style={{ color: color }}>{title}</label>
    <p className="normal-text">{text}</p>
  </div>
));

export const LongTextComponent = React.memo(({ text, title, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <p className="normal-text">{text}</p>
  </div>
));

export const NumberComponent = React.memo(({ number, title, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <p className="normal-text">{number}</p>
  </div>
));

export const TagComponent = React.memo(({ tags, title, color }) => {
  const tagArray = tags.split(",");

  return (
    <div className="app-component-card">
      <label style={{ color: color }}>{title}</label>
      <div style={{ marginTop: 10 }}>
        {tagArray.map((tag, index) => (
          <span className="tag-button" key={`tag-btn-${index}`}>
            {tag}
          </span>
        ))}
      </div>
    </div>
  );
});

export const DateCalendarComponent = React.memo(({ date, title, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <p className="normal-text">{date}</p>
  </div>
));

export const ItemIDComponent = React.memo(({ itemId, title, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <p className="normal-text">{itemId}</p>
  </div>
));

export const PeopleComponent = React.memo(({ people, title, color }) => {
  return (
    <div className="app-component-card">
      {(typeof people !== 'undefined' && typeof people !== 'string') &&
        people.map((user, index) => (
          <div className="d-flex" key={`user-team-${index}`}>
            <div className="small-avatar">
              <img src={user.photo_thumb} alt="small-avatar" />
            </div>
            <div>
              <label style={{ color: color }}>{title}</label>
              <p className="normal-text">{user.name}</p>
            </div>
          </div>
        ))}
    </div>
  );
});

export const ProgressBarComponent = React.memo(({ progress, from, to, title, color }) => {
  const percent = getProgressByTime(from, to);
  return (
    <div className="app-component-card">
      <label style={{ color: color }}>{title}</label>
      <div className="component-progress-bar my-2">
        <div
          className="component-progress-done"
          style={{ width: `${percent}%` }}
        >
          {percent < 85 && percent > 15
            ? MONTHS[new Date(from).getMonth()]
            : ""}{" "}
          {percent < 85 && percent > 15 ? new Date(from).getDate() : ""}
        </div>
        <div
          className="component-progress-yet"
          style={{ width: `${100 - percent}%` }}
        >
          {percent < 85 && percent > 15 ? MONTHS[new Date(to).getMonth()] : ""}{" "}
          {percent < 85 && percent > 15 ? new Date(to).getDate() : ""}
        </div>
      </div>
      {percent >= 85 ? (
        <div className="component-progress-text active">
          {MONTHS[new Date(from).getMonth()]} {new Date(from).getDate()} -{" "}
          {MONTHS[new Date(to).getMonth()]} {new Date(to).getDate()}
        </div>
      ) : (
          ""
        )}
      {percent <= 15 ? (
        <div className="component-progress-text inactive">
          {MONTHS[new Date(from).getMonth()]} {new Date(from).getDate()} -{" "}
          {MONTHS[new Date(to).getMonth()]} {new Date(to).getDate()}
        </div>
      ) : (
          ""
        )}
      <p className="normal-text text-center">{percent}% Done</p>
    </div>
  );
});

export const TimelineComponent = React.memo(({ from, to, title, color }) => {
  const progress = getProgressByTime(from, to);

  return (
    <div className="app-component-card">
      <label style={{ color: color }}>{title}</label>
      <div className="d-flex justify-content-center component-gauge my-2">
        <Icon name="rainbow" />
        <img
          src={indicator}
          alt="indicator"
          style={{
            transform: `rotate(${parseInt(progress * 1.76, 10) - 91}deg)`,
          }}
        />
      </div>
      <p style={{ color: color }} className="normal-text text-center">Complete {progress} %</p>
    </div>
  );
});

export const ChartProgressComponent = React.memo(({ progress, from, to, color }) => {
  const percent = getProgressByTime(from, to);

  return (
    <div className="app-component-card">
      <label style={{ color: color }}>Chart Progress</label>
      <div className="component-progress-bar">
        <div
          className="component-progress-done"
          style={{ width: `${percent}%` }}
        >
          {percent < 85 && percent > 15 ? percent + "%" : ""}
        </div>
        <div
          className="component-progress-yet"
          style={{ width: `${100 - percent}%` }}
        >
          {percent < 85 && percent > 15 ? 100 - percent + "%" : ""}
        </div>
      </div>
      {percent >= 85 ? (
        <div className="component-progress-text active">
          {percent}% - {100 - percent}%
        </div>
      ) : (
          ""
        )}
      {percent <= 15 ? (
        <div className="component-progress-text inactive">
          {percent}% - {100 - percent}%
        </div>
      ) : (
          ""
        )}
      <p className="normal-text text-center">{percent}% Done</p>
    </div>
  );
});

export const ChartGaugeComponent = React.memo(({ from, to, color }) => {
  const progress = getProgressByTime(from, to);

  return (
    <div className="app-component-card">
      <label style={{ color: color }}>Chart Gauge</label>
      <div className="d-flex justify-content-center component-gauge mb-2">
        <Icon name="rainbow" />
        <img
          src={indicator}
          alt="indicator"
          style={{
            transform: `rotate(${progress * 1.76 - 91}deg)`,
          }}
        />
      </div>
      <p className="normal-text text-center">Complete {progress} %</p>
    </div>
  );
});

export const WorldNightClockComponent = React.memo(({ clock, title, format, color }) => {
  const [date, setDate] = useState(null);
  const [isNight, setNight] = useState(false);

  useEffect(() => {
    let timezone = "";
    if (clock.value) {
      switch (clock.value.timezone) {
        case "US/Pacific":
          timezone = "America/Los_Angeles";
          break;
        case "US/Samoa":
          timezone = "Pacific/Apia";
          break;
        default:
          timezone = clock.value.timezone;
          break;
      }
      let options = {
        timeZone: timezone,
        hour: "numeric",
        minute: "numeric",
        hour12: format === '12',
      },
        formatter = new Intl.DateTimeFormat([], options);

      let date = formatter.format(new Date());
      const hour = parseInt(date.slice(0, date.indexOf(":")), 10);
      const temp = date.slice(date.indexOf(" "), date.length);

      if (format === '12') {
        if (temp.includes("PM") && hour === 12) setNight(false);
        else if (temp.includes("AM") && hour === 0) setNight(true);
        else if (
          (temp.includes("PM") && hour >= 5) ||
          (temp.includes("AM") && hour < 5)
        )
          setNight(true);
        else setNight(false);
      } else {
        if (hour >= 17) setNight(true);
        else if (hour >= 5) setNight(false);
      }

      setDate(date);
    } else {
      setDate(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [clock, format]);



  return (
    <div className="app-component-card">
      <label style={{ color: color }}>{title}</label>
      {
        date ? <div className={`clock-form ${isNight ? "black" : "primary"}`} style={{ marginTop: 10 }}>
          {isNight ? <Icon name="moon" /> : <Icon name="sun" />}
          <p>
            {date} | {clock.text}
          </p>
        </div>
          :
          <></>
      }

    </div>
  );
});

// export const StatusCircleComponent = React.memo(({ status, title }) => {
//   return (
//     <div className="app-component-card">
//       <label>{title}</label>
//       <div className="d-flex mb-3">
//         <div
//           className="status-component circle"
//           style={{ backgroundColor: STATUS_VALUES[status.value.index].color }}
//         ></div>

//         <p className="normal-weight-text">{status.text}</p>
//       </div>
//     </div>
//   );
// });
export const StatusSquareComponent = ({ status, title, shape, options, updateObj, setThingIsOpen, color }) => {
  return (
    <div className="app-component-card">
      <div onClick={() => setThingIsOpen({ updateObj: { ...updateObj, updateType: "status", shape }, options })}>
        <label style={{ color: color }}>{title}</label>
        <div className="d-flex mb-3">
          {status && status.value && status.value.index &&
            <div
              className={`status-component my-1 ${shape === 0 ? '' : 'circle'}`}

              style={{ backgroundColor: typeof status.value.index !== 'undefined' ? STATUS_VALUES2[status.value.index].color : STATUS_VALUES2[4].color }}
            ></div>
          }
          {
            status &&
            <p style={{ color: color }} className="normal-weight-text">{status.text}</p>
          }
        </div>
      </div>
    </div>
  )
};

export const VoteComponent = React.memo(({ title, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <button className="large-component-button mb-1">
      <Icon name="vote_outline" />
      &nbsp; Vote
    </button>

    <button className="large-component-button disabled">
      <Icon name="vote" />
      &nbsp; Voted
    </button>
  </div>
));

export const VoteStatusComponent = React.memo(({ vote, title, icon, option, itemId, column, boardId, token, color, fetchBoardData, userId, user }) => {
  const number = parseInt(vote, 10);
  //updateField
  const voted = user[column] && user[column].value && user[column].value.voted === "true";
  return (
    <div className="app-component-card">
      <label style={{ color: color }}>{title}</label>
      {!voted &&
        <button onClick={() => fetchBoardData({ token, boardId, columnId: column, itemId, update: { voted: true } })} className={`large-component-button mb-1 active`}>
          <Icon name={VOTE_ICONS_LIST[icon].vote.icon} />
          &nbsp; Vote
        </button>
      }
      {voted &&
        <button onClick={() => fetchBoardData({ token, boardId, columnId: column, itemId, update: { voted: false } })} className={`large-component-button mb-1 disabled`}>
          <Icon name={VOTE_ICONS_LIST[icon].vote.icon} />
          &nbsp; Voted
        </button>
      }
      {/* <button className={`large-component-button mb-1 ${number > 0 ? 'disabled' : 'active'}`}>
        <Icon name={VOTE_ICON_LIST[icon].icon} />
      &nbsp; {number > 0 ? 'Voted' : 'Vote'}
      </button> */}
      {option ? <p className="normal-text text-center mb-2">{number ? number : 0} votes</p> : ''}


      {/* <button className="large-component-button disabled">
      <Icon name="vote"/>
      &nbsp; Voted
    </button>
    <p className="normal-text text-center">{vote ? vote : 0} votes</p> */}
    </div>
  )
});

export const VoteButtonComponent = React.memo(({ vote, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>Vote Button</label>
    <button className="large-component-button mb-1">Vote</button>
    <p className="normal-text text-center mb-2">{vote ? vote : 0} votes</p>

    <button className="large-component-button disabled">Voted</button>
    <p className="normal-text text-center">{vote ? vote : 0} votes</p>
  </div>
));

export const RatingComponent = React.memo(({ rating, title, token, boardId, column, itemId, color, fetchBoardData }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <div className="component-rank">
      {RATING_VALUES.map((value, index) =>
        index < parseInt(rating, 10) ? (
          <span key={`star-${index}`} onClick={() => fetchBoardData({ token, boardId, columnId: column, itemId, update: { rating: index + 1 } })}>
            <Icon name="star_fill" />
          </span>
        ) : (
            <span key={`star-${index}`} onClick={() => fetchBoardData({ token, boardId, columnId: column, itemId, update: { rating: index + 1 } })}>
              <Icon name="star" />
            </span>
          )
      )}
    </div>
  </div>
));

export const RaringHeartComponent = React.memo(({ rating, title, token, boardId, column, itemId, color, fetchBoardData }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <div className="component-rank">
      {LIKE_VALUES.map((value, index) =>
        index < parseInt(rating, 10) ? (
          <span key={`star-${index}`} onClick={() => fetchBoardData({ token, boardId, columnId: column, itemId, update: { rating: index + 1 } })} >
            <Icon name="heart_fill" />
          </span>
        ) : (
            <span key={`star-${index}`} onClick={() => fetchBoardData({ token, boardId, columnId: column, itemId, update: { rating: index + 1 } })}>
              <Icon name="heart" />
            </span>
          )
      )}
    </div>
  </div>
));

export const CreationLogComponent = React.memo(({ time, title, people, color }) => (
  <div className="app-component-card">
    <label style={{ color: color }}>{title}</label>
    <p style={{ color: color }} className="small-text">
      {time.replace(" UTC", "")} by {people.map((item, index) => (`${item.name}${index === people.length - 1 ? '' : ','} `))}
    </p>
  </div>
));

export const LastUpdateLogComponent = React.memo(({ time, title, people }) => (
  <div className="app-component-card">
    <label>{title}</label>
    <p className="small-text">
      {time.replace(" UTC", "")} by {people.map((item, index) => (`${item.name}${index === people.length - 1 ? '' : ','} `))}
    </p>
  </div>
));

