import React, { useCallback, useState, useEffect } from "react";
import styled from "styled-components";
import RSearchBar from '../../../../widgets/RSearchBar';
import { connect } from 'react-redux';
import { getDate } from "helper";

const CalendarViewBody = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;

  color: #737383;

  overflow-y: scroll;
  margin-right: -17px;

  height: 100%;

  text-transform: capitalize;
`;

const ListItemParentBox = styled.div`
  height: 39px;
  background: #f2f2f2;

  padding-left: 13px;
  padding-right: 13px;

  display: flex;
  justify-content: space-between;
  align-items: center;

  & > label:first-child {
    color: #f62765;
  }
`;

const ListItemChildBox = styled.div`
  height: 57px;

  display: grid;
  grid-template-columns: [first] 25% [line2] auto;

  border-bottom: 2px solid #f2f2f2;

  cursor: pointer;

  & > label {
    display: flex;
    align-items: center;
  }

  & > label:first-child {
    justify-content: center;
  }

  & > label:last-child {
    padding-left: 5px;
  }
`;

const CalendarView = React.memo((props) => {
  const { items, propertyData } = props;
  const { selectItem, changeLayoutPage, iphoneColorFont, iphoneColor, hasSearchBar } = props;
  const [sortedItems, setSort] = useState(null);

  useEffect(() => {
    if(propertyData.when){
      setSort(propertyData.when !== "none" ? getDate(items,propertyData.when) : []);
    }
  }, [items, propertyData]);

  const onClickItem = useCallback(
    (value) => {
      console.log("Selected Item: ", value);
      console.log("Items: ", items);

      let id = 0;
      items.forEach((item, index) => {
        if (item.index === value.index) id = index;
      });
      selectItem && selectItem(id);
      selectItem && changeLayoutPage("component");
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [items]
  );
  const [search, setSearch] = useState(null);


  const filterPersonHandler = event => {

    setSearch(event.target.value.toLowerCase());
    //items.filter(checkSearch);
  }
  const searchBar = <RSearchBar
    filterPersonHandler={filterPersonHandler}
    magnifingColor={iphoneColorFont}
    backgroundColor1={iphoneColor}
    placeHolderSearchColor={iphoneColorFont}
  />;

  return (
    <>
      {hasSearchBar === true ? (

        <>
          {searchBar}

          <CalendarViewBody>
            {sortedItems &&
              sortedItems.map((item, index) => {
                if (item.name.value.toLowerCase().includes(search) || !search) {
                  return (
                    <div key={`calendar-event-${index}`}>
                      <ListItemParentBox>
                        <label>{item.date4.text}</label>
                      </ListItemParentBox>
                      <ListItemChildBox onClick={() => onClickItem(item)}>
                        {item.date4.time && <label>{item.date4.time}</label>}
                        <label>
                          {item[propertyData.title] && typeof item[propertyData.title].text === "string"
                            ? item[propertyData.title].text
                            : ""}
                        </label>
                      </ListItemChildBox>
                    </div>
                  )
                }
              })}
          </CalendarViewBody>
        </>) : (
          <>

            <CalendarViewBody>
              {sortedItems &&
                sortedItems.map((item, index) => (
                  <div key={`calendar-event-${index}`}>
                    <ListItemParentBox>
                      <label>{item.date4.text}</label>
                    </ListItemParentBox>
                    <ListItemChildBox onClick={() => onClickItem(item)}>
                      {item.date4.time && <label>{item.date4.time}</label>}
                      <label>
                        {item[propertyData.title] && typeof item[propertyData.title].text === "string"
                          ? item[propertyData.title].text
                          : ""}
                      </label>
                    </ListItemChildBox>
                  </div>
                ))}
            </CalendarViewBody>
          </>
        )}
    </>
  );
});

const mapStateToProps = (state) => ({
  iphoneColorFont: state.StyleReducer.iphoneColorFont,
  iphoneColor: state.StyleReducer.iphoneColor,
  hasSearchBar: state.StyleReducer.tabs[state.StyleReducer.currentTab].property.hasSearchBar

});
export default connect(mapStateToProps, null)(CalendarView);
