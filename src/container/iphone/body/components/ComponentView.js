import React, { useState } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { getFilterFunction } from "../../../../helper"
import { changeTab } from "redux/style";

import {
  STATUS_VALUES2,
} from "constants/constants";
import { updateField, getBoardData } from "api/monday";

import "./components/_style.css"
import {
  VideoComponent,
  ActionTextComponent,
  TextComponent,
  TitleComponent,
  ImageComponent,
  SeparatorComponent,
  ButtonComponent,
  MapComponent,
  LinkComponent,
  PhoneComponent,
  DateComponent,
  DateTimeComponent,
  CommentComponent,
  TextEntryComponent,
  NoteComponent,
  LongTextComponent,
  NumberComponent,
  TagComponent,
  ItemIDComponent,
  PeopleComponent,
  ProgressBarComponent,
  TimelineComponent,
  WorldNightClockComponent,
  StatusSquareComponent,
  VoteStatusComponent,
  RatingComponent,
  RaringHeartComponent,
  CreationLogComponent,
  LastUpdateLogComponent,
} from "./components";

import ListComponent from "./ListComponent"
import { parseColumns } from "helper";
import { fetchData } from "redux/monday";
// import ListView from "./ListView";

const ComponentViewBox = styled.div`
  padding-left: 20px;
  padding-right: 20px;

  overflow-y: scroll;
  margin-right: -17px;
  height: 100%;

  text-transform: capitalize;
`;


const UpdateComponent = ({ setThingIsOpen, updateObj, options, fetchBoardData }) => {

  const closeModal = (update) => {
    fetchBoardData({ ...updateObj, update: { label: update } })
    setThingIsOpen(false);
  }

  if (updateObj.updateType === true) {
    return <div onClick={() => setThingIsOpen(false)}></div>
  } else if (updateObj.updateType === "status") {
    return (
      <div className="app-component-card">
        {Object.entries(options.settings_str.labels)
          .map((key, value) => {
            const colorz = STATUS_VALUES2.filter(x => x.value + "" === key[0])[0].color
            return (
              <div onClick={() => closeModal(key[1])}>
                <div className="d-flex mb-3">
                  <div className={`status-component my-1 ${updateObj.shape === 0 ? '' : 'circle'}`} style={{ backgroundColor: colorz }}>
                  </div>
                  <p className="normal-weight-text">{key[1]}</p>
                </div>
              </div>)
          })}
      </div>)
  }
}

const ComponentView = (props) => {
  const { fetchData, changeTab, mondayData, componentData, componentDetails, detailsData, updateProperty, propertyData, boardId, token, iphoneColorFont, userId } = props;

  const [thingIsOpen, setThingIsOpen] = useState(false)

  const fetchBoardData = async ({ token, boardId, columnId, itemId, update }) => {
    await updateField({ token, boardId, columnId, itemId, update })
    const res = await getBoardData(token, boardId);

    const data = parseColumns(res.data);
    console.log(data);
    fetchData({
      items: res.data.items.map((item, index) => ({ ...item, index })),
      columns: data.columns,
      column_json: data.column_json,
      boardId: boardId,
    });
  }

  const renderItem =
    // useCallback(
    (item, index) => {
      const user = { ...mondayData.items[mondayData.itemIndex || propertyData.detailsIndex]};
      const detail = componentDetails[index];
      const columnOptions = mondayData.columns.filter(x => x.id === detail.column)[0]
      const columnsData = mondayData.column_json;
      console.log(user)
      if (detail.viz_filter_by && detail.viz_filter_by !== "none") {
        const filterObj = getFilterFunction(detail.viz_filter_by);
        if (filterObj([user]).length === 0) return
      }


      switch (item.name) {
        case "Video":
          return <VideoComponent
            title={detail.title || (columnsData[detail.column] || detail.column)}
            link={user[detail.link] && user[detail.link].text}

          />

        case "Action Text":
          return <ActionTextComponent
            email={user[detail.column] ? user[detail.column].text : detail.column}
            title={detail.title || (columnsData[detail.column] || detail.column)}
            data={user[detail.data] ? user[detail.data].text : detail.data}
            action={detail.action}
            detail={detail}
            changeTab={changeTab}
            color={iphoneColorFont}

          />;
        case "Text":
          return <TextComponent
            style={detail.style}
            align={detail.align}
            is_all_caps={detail.is_all_caps}
            label={detail.text !== 'custom' ? user[detail.text] && user[detail.text].text : detail.custom}
            title={detail.text !== 'custom' ? columnsData[detail.text] : ""}
            color={iphoneColorFont}
          />;
        case "Title":
          return (
            <TitleComponent
              avatar={detail.image !== 'custom' ? (user[detail.image] ? user[detail.image].text : "") : detail.custom_image}
              name={detail.title !== 'custom' ? (user[detail.title] ? user[detail.title].text : "") : detail.custom_title}
              company={detail.details !== 'custom' ? (user[detail.details] ? user[detail.details].text : "") : detail.custom_details}
              color={"white"}
            />
          );
        case "Image":
          return (
            <ImageComponent
              {...detail}
              property={user[detail.image] && user[detail.image].text}
              data={user[detail.data] ? user[detail.data].text : detail.data}
              action={detail.action}
              changeTab={changeTab}
              color={iphoneColorFont}
            />
          );
        case "Separator":
          return <SeparatorComponent />;
        case "Button":
          return <ButtonComponent data={user[detail.data] ? user[detail.data].text : detail.data} action={detail.action}
            changeTab={changeTab} text={detail.title} style={detail.show_as} />;
        case "Map":
          return <MapComponent location={user[detail.address] && user[detail.address].text} zoom={detail.zoom} />;
        case "Link":
          return (
            <LinkComponent
              link={user[detail.link].text}
              label={detail.label}
              title={detail.link}
              color={iphoneColorFont}
            />
          );
        case "Phone":
          return (
            <PhoneComponent
              phone={user[detail.column] && user[detail.column].text}
              body={user[detail.body].text}
              title={detail.title || columnsData[detail.column]}
              color={iphoneColorFont}
            />
          );
        case "Date":
          return <DateComponent date={user[detail.column] && user[detail.column].text} title={detail.title || columnsData[detail.column]}
            color={iphoneColorFont} />;
        case "Date Time":
          return <DateTimeComponent date={user[detail.column] && user[detail.column].text} title={detail.title || columnsData[detail.column]} format={detail.format}
            itemId={user.item_id.text}
            columnId={detail.column}
            boardId={boardId}
            color={iphoneColorFont}
            token={token} />;
        case "Comments":
          return <CommentComponent />;
        case "Text Entry":
          return (
            <TextEntryComponent
              text={user[detail.column] && user[detail.column].text}
              title={detail.title || columnsData[detail.column]}
              hint={detail.hint_text}
            />
          );
        case "Long Text":
          return <LongTextComponent text={user.long_text.text} />;
        case "Notes":
          return (
            <NoteComponent
              text={user[detail.column] && user[detail.column].text}
              details={detail.details}
              image={detail.image}
              title={detail.title || columnsData[detail.column]}
            />
          );
        case "Number Entry":
          return (
            <NumberComponent
              number={user[detail.column] && user[detail.column].text}
              title={detail.title || columnsData[detail.column]}
              hint={detail.hint_text}
            />
          );
        case "Tags":
          return <TagComponent tags={user[detail.column] && user[detail.column].text} title={detail.title || columnsData[detail.column]} />;
        case "Item ID":
          return <ItemIDComponent itemId={user[detail.column] && user[detail.column].text} title={detail.title || columnsData[detail.column]} />;
        case "People":
          return <PeopleComponent people={user[detail.column] && user[detail.column].text} title={detail.title || columnsData[detail.column]} />;
        case "Progress Bar":
          if (detail.type === 0)
            return (
              <TimelineComponent
                from={user.timeline.value.from}
                to={user.timeline.value.to}
                title={detail.title || columnsData[detail.column]}
                color={iphoneColorFont}
              />
            );
          else
            return (
              <ProgressBarComponent
                progress={user.progress.text}
                from={user.timeline.value.from}
                to={user.timeline.value.to}
                title={detail.title || columnsData[detail.column]}
              />
            );
        case "Timeline":
          if (detail.type === 0)
            return (
              <TimelineComponent
                from={user.timeline.value.from}
                to={user.timeline.value.to}
                title={detail.title || columnsData[detail.column]}
                color={iphoneColorFont}
              />
            );
          else
            return (
              <ProgressBarComponent
                progress={user.progress.text}
                from={user.timeline.value.from}
                to={user.timeline.value.to}
                title={detail.title || columnsData[detail.column]}
                color={iphoneColorFont}
              />
            );
        case "World Clock":
          return <WorldNightClockComponent
            clock={detail.column === "world_clock" ? user.world_clock : ''}
            title={detail.title || columnsData[detail.column]}
            format={detail.format}
            color={iphoneColorFont}
          />;
        case "Status":
          return <StatusSquareComponent status={user[detail.column]}
            title={detail.title || columnsData[detail.column]}
            shape={detail.shape}
            options={columnOptions}
            color={iphoneColorFont}
            fetchBoardData={fetchBoardData}
            updateObj={{
              itemId: user.item_id.text,
              columnId: detail.column,
              boardId: boardId,
              token: token,

            }}
            setThingIsOpen={setThingIsOpen}
          />;
        case "Vote":
          return <VoteStatusComponent
            itemId={user.item_id.text}
            column={detail.column}
            boardId={boardId}
            token={token}
            vote={user[detail.column] && user[detail.column].text}
            title={detail.title || columnsData[detail.column]}
            icon={detail.icon}
            option={detail.option}
            color={iphoneColorFont}
            fetchBoardData={fetchBoardData}
            user={user}
            userId={userId}
          />;
        case "Rating":
          console.log(user[detail.column] && user[detail.column].text)
          return detail.icon === 0 ?
            <RatingComponent
              rating={user[detail.column] && user[detail.column].text}
              title={detail.title || columnsData[detail.column]}
              itemId={user.item_id.text}
              column={detail.column}
              boardId={boardId}
              token={token}
              color={iphoneColorFont}
              fetchBoardData={fetchBoardData}
            />
            : <RaringHeartComponent
              rating={user[detail.column] && user[detail.column].text}
              title={detail.title || columnsData[detail.column]}
              itemId={user.item_id.text}
              column={detail.column}
              boardId={boardId}
              token={token}
              color={iphoneColorFont}
              fetchBoardData={fetchBoardData}
            />

        case "Creation Log":
          return (
            <CreationLogComponent
              time={user[detail.column] && user[detail.column].text}
              title={detail.title || columnsData[detail.column]}
              people={user.person.text}
              color={iphoneColorFont}
            />
          );
        case "Last Updated":
          return (
            <LastUpdateLogComponent
              time={user[detail.column] && user[detail.column].text}
              title={detail.title || columnsData[detail.column]}
              people={user.person.text}
              color={iphoneColorFont}
            />
          );
        case "Inline List":
          return (
            <ListComponent
              mondayData={mondayData}
              propertyData={detail}
              isInline={true}
              color={iphoneColorFont}
              // selectItem={selectItem}
              // toggleComponentView={toggleComponentView}
              updateProperty={updateProperty}
              columnsData={columnsData}
              detailsData={detailsData}
            >

            </ListComponent>
          )
        default:
          return <></>;
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    // [mondayData, componentData, componentDetails]
    ;
  return (
    <ComponentViewBox>
      {thingIsOpen ? <UpdateComponent setThingIsOpen={setThingIsOpen}
        updateObj={thingIsOpen.updateObj}
        options={thingIsOpen.options}
        fetchBoardData={fetchBoardData}>

      </UpdateComponent>
        :
        componentData &&
        componentData.map((item, index) => (
          <div key={`app-data-${index}`}>{renderItem(item, index)}</div>
        ))}
    </ComponentViewBox>
  );
};

const mapDispatchToProps = (dispatch) => ({
  changeTab: (tab) => dispatch(changeTab(tab)),
  fetchData: (data) => dispatch(fetchData(data))
})

const mapStateToProps = (state) => ({
  propertyData: state.StyleReducer.tabs
    ? state.StyleReducer.tabs[state.StyleReducer.currentTab].property
    : {},
  boardId: state.MondayReducer.boardId,
  token: state.MondayReducer.token,
  iphoneColorFont: state.StyleReducer.iphoneColorFont,
  userId: state.MondayReducer.userId,

});


export default connect(mapStateToProps, mapDispatchToProps)(ComponentView);

// export default ComponentView;
