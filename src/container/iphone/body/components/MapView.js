import React, { useState, useEffect, useCallback } from "react";
import styled from "styled-components";
import mapboxgl from "mapbox-gl";

import { MAPBOX_API_KEY } from "constants/constants";
import { getGeometry } from "api/mapbox";

import ListView from "./ListView";
import Icon from "widgets/Icon";

mapboxgl.accessToken = MAPBOX_API_KEY;

const MapViewBox = styled.div`
  padding: 0px 10px 0px 10px;

  overflow: hidden;
  overflow-y: scroll;
  margin-right: -17px;

  height: 100%;

  display: flex;
  flex-direction: column;
  justify-content: space-between;

  position: relative;

  text-transform: capitalize;
`;

const MapContainerBox = styled.div`
  width: 100%;
  height: 100%;

  position: relative;
  z-index: 0;
`;

const UserDetailBox = styled.div`
  height: 80px;
  background: #ffffff;

  display: grid;
  grid-template-columns: [first] 30% [line2] 65%;

  cursor: pointer;
`;

const UserAvatar = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 62px;
  height: 62px;

  border: none;
  border-radius: 50%;

  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  
  align-self: center;
  justify-self: center;
}
`;

const UserDetail = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  & > p {
    font-family: Open Sans;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 19px;

    margin: 0;
    padding: 2px;
  }

  & > p:first-child {
    font-weight: 600;
    font-size: 18px;
    line-height: 25px;

    color: #303655;
  }
`;

const ViewModeControl = styled.button`
  border: none;
  border-radius: 50%;

  cursor: pointer;

  z-index: 1;

  padding: 0;
  margin: 0;

  background: transparent;

  width: 60px;
  min-width : 60px;

  height: 60px;

  display: flex;
  justify-content: center;
  align-items: center;

  position: absolute;
  right: 10px;

  &.currentLoc {
    background: #ffffff;

    bottom: 80px;

    & > svg path {
      fill: #f62765;
    }
  }

  &.listView {
    background: #f62765;

    bottom: 10px;

    & > svg path {
      fill: #ffffff;
    }
  }

  &.mapView {
    background: #f62765;

    bottom: 10px;

    & > svg path {
      fill: #333333;
    }
  }
`;

const MapView = React.memo((props) => {
  const { items, propertyData } = props;
  const { selectItem, changeLayoutPage, updateProperty } = props;

  const [item, setItem] = useState(null);
  // ['MAP_VIEW', 'LIST_VIEW' ]
  const [location, setLocation] = useState({
    lng: 5,
    lat: 34,
    zoom: 6,
  });
  const [markers, setMarkers] = useState([]);

  useEffect(() => {
    (async function generateMarkers() {
      if (!items) return;
      let markerArray = [];
      for (const itemValue of items) {
        const res = await getGeometry(itemValue[propertyData.address] && itemValue[propertyData.address].text);
        const lng = res.data.features[4].center[0].toFixed(4),
          lat = res.data.features[4].center[1].toFixed(4);

        markerArray.push({
          address: itemValue[propertyData.address] && itemValue[propertyData.address].text,
          name: itemValue[propertyData.title].text,
          lng,
          lat
        });

      }
      setMarkers(markerArray)
    })();
  }, [items,propertyData]);

  useEffect(() => {
    if (!items || !propertyData.view) return;

    const map = new mapboxgl.Map({
      container: "myContainer",
      style: "mapbox://styles/mapbox/streets-v11",
      center: [location.lng, location.lat],
      zoom: location.zoom,
    });

    map.on("move", () => {
      setLocation({
        lng: map.getCenter().lng.toFixed(4),
        lat: map.getCenter().lat.toFixed(4),
        zoom: map.getZoom().toFixed(2),
      });
    });

    if (markers.length === 0) return;

    for (let i in items) {
      const marker = markers[i];
      const mapMarker = new mapboxgl.Marker()
        .setLngLat([marker.lng, marker.lat])
        .addTo(map);

      mapMarker.getElement().addEventListener("click", () => {
        setItem({
          value: items[i],
          index: i
        });
        map.flyTo({
          center: [marker.lng, marker.lat],
        });
      });
    }

    if (markers.length > items.length) {
      const marker = markers[markers.length - 1];
      new mapboxgl.Marker()
        .setLngLat([marker.lng, marker.lat])
        .addTo(map);
      map.flyTo({
        center: [marker.lng, marker.lat],
      });
    }
    else map.flyTo({
      center: [markers[0].lng, markers[0].lat],
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [items, propertyData.view, markers]);

  const showCurrentLocation = useCallback(() => {
    if (navigator.geolocation && markers.length === items.length) {
      navigator.geolocation.getCurrentPosition((position) => {
        setMarkers([
          ...markers,
          {
            lng: `${position.coords.longitude.toFixed(4)}`,
            lat: `${position.coords.latitude.toFixed(4)}`,
          },
        ]);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [markers, items]);

  const onUserClickHandler = useCallback((value) => {
    if (!value) return;
    selectItem(value.index);
    changeLayoutPage("component");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <MapViewBox>
      {propertyData.view ? (
        <>
          <MapContainerBox id="myContainer">
            {
              propertyData.option ? <ViewModeControl
                className="currentLoc"
                onClick={() => showCurrentLocation()}
              >
                <Icon name="center" />
              </ViewModeControl>
                :
                <></>
            }

            <ViewModeControl
              className="listView"
              onClick={() => updateProperty("view", false)}
            >
              <Icon name="list" />
            </ViewModeControl>
          </MapContainerBox>


          {item ? (
            <UserDetailBox onClick={() => onUserClickHandler(item)}>
              <UserAvatar
                style={{ backgroundImage: `url(${item.value[propertyData.image] && typeof item.value[propertyData.image].text === 'string' && item.value[propertyData.image].text})` }}
              />
              <UserDetail>
                <p>{item.value[propertyData.title] && typeof item.value[propertyData.title].text === 'string' && item.value[propertyData.title].text}</p>
                <p>{item.value[propertyData.details] && typeof item.value[propertyData.details].text === 'string' && item.value[propertyData.details].text}</p>
              </UserDetail>
            </UserDetailBox>
          ) : (
              <></>
            )}


        </>
      ) : (
          <>
            <ListView
              items={items}
              propertyData={propertyData}
              selectItem={selectItem}
              changeLayoutPage={changeLayoutPage}
            />
            <ViewModeControl
              className="listView"
              onClick={() => updateProperty("view", true)}
            >
              <Icon name="map" />
            </ViewModeControl>
          </>
        )}
    </MapViewBox>
  );
});

export default MapView;
