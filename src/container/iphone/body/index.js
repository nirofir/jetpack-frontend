import React, { useCallback } from "react";
import { connect } from "react-redux";
import ListComponent from "./components/ListComponent"
import ComponentView from "./components/ComponentView";

import { getFilterFunction } from "../../../helper"

import { selectItem } from "redux/monday";
import { updateProperty, toggleComponentView } from "redux/style";




const NotListComponent = ({
  // columnsData,
  mondayData, detailsData, componentData, propertyData, componentDetails, componentView,chosenId }) => {
  if (!componentView) {
    return (
      <ComponentView
        // columnsData={columnsData}
        mondayData={mondayData}
        detailsData={detailsData}
        componentData={detailsData.components}
        componentDetails={detailsData.componentDetails}
      />
    );
  } else {
    return (
      <ComponentView
        // columnsData={columnsData}
        mondayData={mondayData}
        componentData={componentData}
        componentDetails={componentDetails}
        detailsData updateProperty propertyData
      />
    );
  }
}

const Body = (props) => {
  const {
    componentView,
    propertyData,
    componentData,
    mondayData,
    // columnsData,
    componentDetails,
    iphoneColor,
    detailsData,
    currentTab,

  } = props;
  const { selectItem, toggleComponentView, updateProperty } = props;
  const renderBody = () => {

    // useCallback(() => {
    if (propertyData.style === "details" || componentView) {
      return <NotListComponent
        // columnsData={columnsData}
        mondayData={mondayData}
        componentData={componentData}
        componentDetails={componentDetails}
        detailsData={detailsData}
        propertyData={propertyData}
        componentView={componentView}>

      </NotListComponent>
    } else {
      return <ListComponent
        mondayData={mondayData}
        selectItem={selectItem}
        toggleComponentView={toggleComponentView}
        updateProperty={updateProperty}
        componentDetails={componentDetails}
        propertyData={propertyData}
      >

      </ListComponent>
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }
  // , [componentView, mondayData, propertyData, componentData, detailsData, currentTab]);
  return (
    <>
      {/*propertyData.hasSearchBar ? <RSearchBar /> : ""*/}
      {renderBody()}
    </>
  );
};

const getFilteredMondayData = (mondayData, filterBy, sortBy) => {
  const filterFunc = filterBy !== "none" ? getFilterFunction(filterBy) : "none"
  const filteredItems = filterFunc !== "none" ? filterFunc(mondayData.items) : mondayData.items;

  const sortedItems = sortBy !== "none" ? sortBy(mondayData.items) : filteredItems;
  return {
    ...mondayData,
    items: sortedItems
  }
}



const mapStateToProps = (state) => ({
  layoutPage: state.LayoutReducer.layoutPage,
  componentView: state.StyleReducer.componentView,
  propertyData: state.StyleReducer.tabs
    ? state.StyleReducer.tabs[state.StyleReducer.currentTab].property
    : {},
  componentData: state.StyleReducer.tabs
    ? state.StyleReducer.tabs[state.StyleReducer.currentTab].components
    : {},
  componentDetails: state.StyleReducer.tabs
    ? state.StyleReducer.tabs[state.StyleReducer.currentTab].componentDetails
    : {},
  currentTab: state.StyleReducer.currentTab,
  mondayData: getFilteredMondayData(state.MondayReducer,
    state.StyleReducer.tabs[state.StyleReducer.currentTab].property.filter_by,
    state.StyleReducer.tabs[state.StyleReducer.currentTab].property.sort_by),
  // columnsData: state.ColumnsReducer,
  detailsData: state.StyleReducer.tabs[state.StyleReducer.currentTab].detailPageStyle,
  chosenId : state.StyleReducer.tabs[state.StyleReducer.currentTab].property.item_id,
  iphoneColor: state.StyleReducer.iphoneColor,


});

const mapDispatchToProps = (dispatch) => ({
  selectItem: (itemIndex) => dispatch(selectItem(itemIndex)),
  // changeAppPage: (value) => dispatch(changeAppPage(value)),
  toggleComponentView: (value) => dispatch(toggleComponentView(value)),
  updateProperty: (field, value) => dispatch(updateProperty(field, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Body);
