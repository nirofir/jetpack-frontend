import React from 'react'
//import MondayImage from '../../assets/svg/monday'
import LogoImg from '../../assets/img/64px-Monday_logo.svg.png'
import jetPackLogo from '../../assets/img/logo.png'

import './login.css'
const Login = props => {

    return (
        <div className={'Login'}  >
            <div>
                <h3>Simple CRM</h3>
            </div>

            <div>
                <h1>Welcome</h1>
                <h2> Login with Monday.com below</h2>
            </div>
            <div>

                <button className={'LoginBtn'} onClick={props.clicked}>
                    <p className='Logo'>{props.children + " "}<img src={LogoImg} /></p>

                </button>


            </div>

            <div >
                <div className='jetPack'>
                    <p style={{ fontStyle: 'italic' }}>Made with  <img src={jetPackLogo} /> </p>
                    <p style={{ fontWeight: 'bold', color: '#303655' }}> Jetpack</p>


                </div>
            </div>


        </div>
    )

}




export default Login;