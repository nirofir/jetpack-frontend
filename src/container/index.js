import React from "react";
import styled from "styled-components";

import IPhone from "./iphone";

const ContainerBox = styled.div`
  width: calc(100vw - 350px);
  height: calc(100vh - 100px);
  overflow-y: scroll;
  
  background: #e5e5e5;

  display: flex;
  justify-content: center;
  align-items: center;
  direction: rtl;
  overflow: auto;
`;

const Container = React.memo(() => {
  return (
    <ContainerBox>
      <IPhone />
    </ContainerBox>
  );
});

export default Container;
