const CHANGE_PAGE_INDEX = "CHANGE_PAGE_INDEX";

// state
const initialState = 0;

// action
export const changePage = (pageIndex) => {
  return {
    type: CHANGE_PAGE_INDEX,
    pageIndex,
  };
};

// reducer
const NavReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_PAGE_INDEX:
      return action.pageIndex;
    default:
      return state;
  }
};

export default NavReducer;
