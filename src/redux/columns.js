const FETCH_COLUMNS = 'FETCH_COLUMNS';

// state
const initialState = null;

// actions
export const fetchColumns = (data) => {
    return {
        type: FETCH_COLUMNS,
        data
    }
}

// reducers
const ColumnsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COLUMNS:
            console.log(action)
            return {
                ...action.data
            }
        default:
            return state;
    }
}

export default ColumnsReducer;