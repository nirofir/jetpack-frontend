import { serialize } from "helper/formater";
import ComponentJSON from "constants/components.json";
import ComponentDetailJSON from "constants/component_details.json";
import { emptyState, defualtProperties, PropertiesTypeList, getAvailColumns,defualtComponent } from "constants/defualtPageView";
import { setDefaultLocale } from "react-datepicker";
// import initDetails from "constants/defualtDetails";

//set style state
const FETCH_STYLE = "FETCH_STYLE"

const UPDATE_EVERY_PROPERTY = "UPDATE_EVERY_PROPERTY"
const UPDATE_EVERY_COMPONENT = "UPDATE_EVERY_COMPONENT"
//app columns 
//const FETCH_COLUMNS 

//filters
const GET_FLITERS = "FETCH_FILRERS";
const ADD_FILTER_LIST = "ADD_FILTER";
const ADD_FILTER_COMPONENT = "ADD_FILTER_COMPONENT";
const ADD_FILTER_INLINE_LIST = "ADD_FILTER_INLINE_LIST";
const ADD_FILTER_INLINE_COMPONENT = "ADD_FILTER_INLINE_COMPONENT";

//app tabs
const CHANGE_TAB = "CHANGE_TAB";
const ADD_TAB = "ADD_TAB";
const REORDER_TAB = "REORDER_TAB";
const UPDATE_TAB_TITLE = "UPDATE_TAB_TITLE";
const CHANGE_TAB_ICON = "CHANGE_TAB_ICON";
const DELETE_TAB = "DELETE_TAB"

//?
const TOGGLE_COMPONENT_VIEW = "TOGGLE_COMPONENT_VIEW";

//app list view and component view
const ADD_COMPONENT = "ADD_COMPONENT";
const ADD_COMPONENT_INLINE = "ADD_COMPONENT_INLINE"
const REMOVE_COMPONENT = "REMOVE_COMPONENT";
const UPDATE_COMPONENT = "UPDATE_COMPONENT";
const SELECT_COMPONENT = "SELECT_COMPONENT";
const REORDER_COMPONENTS = "REORDER_COMPONENTS";
const DUPLICATE_COMPONENT = "DUPLICATE_COMPONENT";
const UPDATE_PROPERTY = "UPDATE_PROPERTY";

//detail view
const FETCH_DETAIL_COMPONENT = "FETCH_DETAIL_COMPONENT";
const ADD_DETAIL_COMPONENT = "ADD_DETAIL_COMPONENT";
const REMOVE_DETAIL_COMPONENT = "REMOVE_DETAIL_COMPONENT";
const UPDATE_DETAIL_COMPONENT = "UPDATE_DETAIL_COMPONENT";
const SELECT_DETAIL_COMPONENT = "SELECT_DETAIL_COMPONENT";
const REORDER_DETAIL_COMPONENTS = "REORDER_DETAIL_COMPONENTS";
const DUPLICATE_DETAIL_COMPONENT = "DUPLICATE_DETAIL_COMPONENT";

const CHANGE_COLOR = "CHANGE_COLOR";
const CHANGE_COLOR_HEADER = "CHANGE_COLOR_HEADER";
const CHANGE_COLOR_FOOTER = "CHANGE_COLOR_FOOTER";
const CHANGE_COLOR_THEME = "CHANGE_COLOR_THEME";
const CHANGE_COLOR_THEME_LIGHT = "CHANGE_COLOR_THEME_LIGHT";
const CHANGE_COLOR_FONT = "CHANGE_COLOR_FONT";


// state

const initialState = {
  currentTab: 0,
  componentView: false,
  iphoneColor: "#ffffff",
  iphoneColorHead: "#ffffff",
  iphoneColorFoot: "#ffffff",
  iphoneColorFont: '#303655',
  iphoneColorHeadFont: '#303655',
  searchShow: true,
  tabs: [
    {
      ...emptyState,
      title: "PEOPLE", icon: "lemon", id: 0
    },
    {
      ...emptyState,
      title: "PLACES", icon: "africa", id: 1
    },
  ],
};

export const changeColor = (color) => {
  return {
    type: CHANGE_COLOR,
    color,
  };
};

export const changeColorFont = (color) => {
  return {
    type: CHANGE_COLOR_FONT,
    color,
  }
}
export const changeColorHead = (color) => {
  return {
    type: CHANGE_COLOR_HEADER,
    color,
  };
};
export const changeColorFoot = (color) => {
  return {
    type: CHANGE_COLOR_FOOTER,
    color,
  };
};


export const changeColorThemeLight = (colorBody, colorHead, colorFoot, colorFontHead) => {
  return {
    type: CHANGE_COLOR_THEME_LIGHT,
    colorBody, colorHead, colorFoot, colorFontHead
  };
};
export const changeColorTheme = (colorBody, colorHead, colorFoot, colorFont, colorFontHead) => {
  return {
    type: CHANGE_COLOR_THEME,
    colorBody, colorHead, colorFoot, colorFont, colorFontHead
  };
};

export const fetchStyle = (style) => {
  return {
    type: FETCH_STYLE,
    style
  };
};

export const addComponent = (label) => {
  return {
    type: ADD_COMPONENT,
    component: { ...ComponentJSON[label] },
    componentDetail: { ...ComponentDetailJSON[label] },
  };
};
export const addComponentInline = (label) => {
  return {
    type: ADD_COMPONENT_INLINE,
    component: { ...ComponentJSON[label] },
    componentDetail: { ...ComponentDetailJSON[label] },
  };
};
export const removeComponent = (index) => {
  return {
    type: REMOVE_COMPONENT,
    componentIndex: index,
  };
};
export const updateComponent = (field, value) => {
  return {
    type: UPDATE_COMPONENT,
    field,
    value,
  };
};
export const selectComponent = (componentIndex) => {
  return {
    type: SELECT_COMPONENT,
    componentIndex,
  };
};
export const duplicateComponent = (componentIndex) => {
  return {
    type: DUPLICATE_COMPONENT,
    componentIndex,
  };
};
export const updateProperty = (field, value) => {
  return {
    type: UPDATE_PROPERTY,
    field,
    value,
  };
};
export const reorderComponents = (components, componentDetails) => {
  return {
    type: REORDER_COMPONENTS,
    components,
    componentDetails,
  };
};
export const reorderTabs = (orderedTabs) => {
  return {
    type: REORDER_TAB,
    orderedTabs,
  };
};

export const updateTabLabel = (newTitle) => {
  return {
    type: UPDATE_TAB_TITLE,
    newTitle
  }
}
export const changeTabIcon = (newIcon) => {
  return {
    type: CHANGE_TAB_ICON,
    newIcon
  }
}

export const deleteTab = () => {
  return { type: DELETE_TAB }
}
// export const selectTab = (index) => {
//   return {
//     type: SELECT_TAB,
//     index,
//   };
// };
export const changeTab = (tabIndex) => {
  return {
    type: CHANGE_TAB,
    tabIndex,
  };
};

export const addTab = (title, icon) => {
  return {
    type: ADD_TAB,
    title,
    icon,
  };
};
//filters
export const addListFilter = (filter) => {
  return {
    type: ADD_FILTER_LIST,
    filter
  }
}

export const toggleComponentView = () => {
  return {
    type: TOGGLE_COMPONENT_VIEW,
  };
};

export const fetchDetailComponent = (style) => {
  return {
    type: FETCH_DETAIL_COMPONENT,
    style,
  };
};
export const addDetailComponent = (label) => {
  return {
    type: ADD_DETAIL_COMPONENT,
    component: { ...ComponentJSON[label] },
    componentDetail: { ...ComponentDetailJSON[label] },
  };
};
export const removeDetailComponent = (index) => {
  return {
    type: REMOVE_DETAIL_COMPONENT,
    componentIndex: index,
  };
};
export const updateDetailComponent = (field, value) => {
  return {
    type: UPDATE_DETAIL_COMPONENT,
    field,
    value,
  };
};
export const selectDetailComponent = (componentIndex) => {
  return {
    type: SELECT_DETAIL_COMPONENT,
    componentIndex,
  };
};
export const duplicateDetailComponent = (componentIndex) => {
  return {
    type: DUPLICATE_DETAIL_COMPONENT,
    componentIndex,
  };
};
export const reorderDetailComponents = (components, componentDetails) => {
  return {
    type: REORDER_DETAIL_COMPONENTS,
    components,
    componentDetails,
  };
};

export const updateEveryProperty = (columns) => {
  return {
    type: UPDATE_EVERY_PROPERTY,
    columns,
  };
};
export const updateComponentComponent = (columns) => {
  return {
    type: UPDATE_EVERY_COMPONENT,
    columns,
  };
};

// reducers
const StyleReducer = (state = initialState, action) => {
  const tabs = state.tabs;
  switch (action.type) {
    case FETCH_STYLE:
      // tabs[state.currentTab] = { ...tabs[state.currentTab], ...action.style };
      return {
        ...state,
        ...action.style,
      };

    // case UPDATE_EVERY_COMPONENT:
    //   ComponentJSON.map()
    //   ComponentDetailJSON.map()
    //   action.componentList
    //   let updatedComponents = [...tabs[state.currentTab].components, action.component];
    //   let updatedDetails = [...tabs[state.currentTab].componentDetails, action.componentDetail];
    //   console.log(updatedDetails)
    //   tabs[state.currentTab].components = serialize(updatedComponents, updatedDetails);
    //   tabs[state.currentTab].componentDetails = updatedDetails;
    //   tabs[state.currentTab].componentIndex = tabs[state.currentTab].components.length - 1;
    //   return {
    //     ...state,
    //     tabs,
    //   };

    case UPDATE_EVERY_PROPERTY:
      const first = getAvailColumns(action.columns, "stringy")[0] && getAvailColumns(action.columns, "stringy")[0].id
      const secound = first && getAvailColumns(action.columns, "stringy")[1] && getAvailColumns(action.columns, "stringy")[1].id
      const third = secound && getAvailColumns(action.columns, "stringy")[2] && getAvailColumns(action.columns, "stringy")[2].id
      const newProperties = {
        title: first || "none",
        details: secound || "none",
        caption: third || "none",
        image: getAvailColumns(action.columns, "imagey")[0] && getAvailColumns(action.columns, "imagey")[0].id || "none",
        when: getAvailColumns(action.columns, "datey")[0] && getAvailColumns(action.columns, "datey")[0].id || "none",
        address: getAvailColumns(action.columns, "addressy")[0] && getAvailColumns(action.columns, "addressy")[0].id || "none"
      }

      tabs[state.currentTab].property = { ...tabs[state.currentTab].property, ...newProperties }
      return {
        ...state,
        tabs
      }



    case ADD_COMPONENT:
      let updatedComponents = [...tabs[state.currentTab].components, action.component];
      let updatedDetails = [...tabs[state.currentTab].componentDetails, action.componentDetail];
      console.log(updatedDetails)
      tabs[state.currentTab].components = [...serialize(updatedComponents, updatedDetails)];
      tabs[state.currentTab].componentDetails = updatedDetails;
      tabs[state.currentTab].componentIndex = tabs[state.currentTab].components.length - 1;
      console.log(tabs[state.currentTab].components.length)
      return {
        ...state,
        tabs,
      };

    // case ADD_COMPONENT_INLINE:
    //   let updatedInline = tabs[state.currentTab].inlineComponents
    //   let updatedInlineComponents = [...tabs[state.currentTab].components, action.component];
    //   let updatedInlineDetails = [...tabs[state.currentTab].componentDetails, action.componentDetail];
    //   tabs[state.currentTab].components = serialize(updatedInlineComponents, updatedInlineDetails);
    //   tabs[state.currentTab].componentDetails = updatedInlineDetails;
    //   tabs[state.currentTab].componentIndex = tabs[state.currentTab].components.length -1;
    //   tabs[state.currentTab].inlineComponents = [...state, {}]
    //   return {
    //     ...state,
    //     tabs,
    //   };

    case REMOVE_COMPONENT:
      tabs[state.currentTab].components = tabs[state.currentTab].components.filter(
        (item, index) => index !== action.componentIndex
      );
      tabs[state.currentTab].componentDetails = tabs[state.currentTab].componentDetails.filter(
        (item, index) => index !== action.componentIndex
      );
      tabs[state.currentTab].componentIndex = null;
      return {
        ...state,
        tabs,
      };

    case UPDATE_COMPONENT:
      let detail = tabs[state.currentTab].componentDetails;

      detail[tabs[state.currentTab].componentIndex][action.field] = action.value;

      // tabs[state.currentTab].component = serialize(state.components, detail)
      tabs[state.currentTab].componentDetails = [...detail];
      return {
        ...state,
        tabs,
      };

    case SELECT_COMPONENT:
      tabs[state.currentTab].componentIndex = action.componentIndex;
      return {
        ...state,
        tabs,
      };

    case DUPLICATE_COMPONENT:
      let newComponents = [...tabs[state.currentTab].components],
        newDetails = [...tabs[state.currentTab].componentDetails];
      newComponents.splice(action.componentIndex + 1, 0, {
        ...newComponents[action.componentIndex],
      });
      newDetails.splice(action.componentIndex + 1, 0, {
        ...newDetails[action.componentIndex],
      });
      tabs[state.currentTab].component = serialize(newComponents, newDetails);
      tabs[state.currentTab].newDetails = newDetails;
      return {
        ...state,
        tabs,
      };

    case REORDER_COMPONENTS:
      tabs[state.currentTab].components = serialize(action.components, action.componentDetails);
      tabs[state.currentTab].componentDetails = [...action.componentDetails];

      return {
        ...state,
        tabs,
      };

    case UPDATE_PROPERTY:
      tabs[state.currentTab].property = {
        ...tabs[state.currentTab].property,
        [action.field]: action.value,
        prev: [...state.tabs[state.currentTab].property.prev, state.tabs[state.currentTab].property],
      };
      return {
        ...state,
        tabs,

      };
    // case updatevtab title
    case UPDATE_TAB_TITLE:
      tabs[state.currentTab].title = action.newTitle.toUpperCase();
      return {
        ...state,
        tabs,

      }
    // case SELECT_TAB:
    //   tabs.currentTab = action.index;
    //   return {
    //     ...state,
    //     tabs,
    //   };
    case REORDER_TAB:
      return {
        ...state,
        tabs: action.orderedTabs,
      };

    case ADD_TAB:
      return {
        ...state,
        currentTab: tabs.length,
        tabs: [
          ...tabs,
          {
            ...emptyState,
            id: tabs.length + 1,
            title: action.title.toUpperCase(),
            icon: action.icon,
          },
        ],
      };

    case CHANGE_TAB:
      return {
        ...state,
        componentView: false,
        currentTab: typeof action.tabIndex === "string" ? tabs.map(tab => tab.title).indexOf(action.tabIndex) : action.tabIndex,
      };

    case DELETE_TAB:
      const newTab = tabs.filter((x, index) => index !== state.currentTab);
      const newIndex = state.currentTab - 1 === -1 ? 0 : state.currentTab - 1;
      return {
        ...state,
        tabs: newTab,
        currentTab: newIndex,
      }

    case CHANGE_TAB_ICON:
      tabs[state.currentTab].icon = action.newIcon;
      return {
        ...state,
        tabs
      }
    //filter 
    case ADD_FILTER_LIST:
      tabs[state.currentTab].property.filter = action.filter;
      return {
        ...state,
        tabs
      }

    case TOGGLE_COMPONENT_VIEW:
      return { ...state, componentView: !state.componentView };

    case FETCH_DETAIL_COMPONENT:
      tabs[state.currentTab].detailPageStyle = action.style;
      return {
        ...state,
        tabs: JSON.parse(JSON.stringify(tabs)),
      };

    case ADD_DETAIL_COMPONENT:
      let newdetailPageStyle = { ...tabs[state.currentTab].detailPageStyle }
      let newDetails2 = [...newdetailPageStyle.componentDetails, action.componentDetail];
      let newCompo2 = [...newdetailPageStyle.components, action.component];

      tabs[state.currentTab].detailPageStyle.components = serialize(newCompo2, newDetails2);
      tabs[state.currentTab].detailPageStyle.componentDetails = newDetails2
      tabs[state.currentTab].detailPageStyle.componentIndex = tabs[state.currentTab].detailPageStyle.components.length - 1;

      return {
        ...state,
        tabs: JSON.parse(JSON.stringify(tabs)),
      };

    case REMOVE_DETAIL_COMPONENT:
      tabs[state.currentTab].detailPageStyle.components = tabs[state.currentTab].
        detailPageStyle.components.filter((item, index) => index !== action.componentIndex);
      tabs[state.currentTab].detailPageStyle.componentDetails = tabs[state.currentTab].
        detailPageStyle.componentDetails.filter((item, index) => index !== action.componentIndex);
      tabs[state.currentTab].detailPageStyle.componentIndex = null;
      return {
        ...state,
        tabs: JSON.parse(JSON.stringify(tabs)),
      };

    case UPDATE_DETAIL_COMPONENT:
      const detailState = tabs[state.currentTab].detailPageStyle
      detailState.componentDetails[detailState.componentIndex][action.field] = action.value;
      // detailState.component = serialize(
      //   detailState.component,
      //   detailState.componentDetails
      //   );
      tabs[state.currentTab].detailPageStyle = detailState

      return {
        ...state,
        tabs: JSON.parse(JSON.stringify(tabs)),
      };

    case SELECT_DETAIL_COMPONENT:

      tabs[state.currentTab].detailPageStyle.componentIndex = action.componentIndex;

      return {
        ...state,
        tabs: JSON.parse(JSON.stringify(tabs)),
      };

    case DUPLICATE_DETAIL_COMPONENT:
      tabs[state.currentTab].detailPageStyle.components.splice(action.componentIndex + 1, 0, {
        ...tabs[state.currentTab].detailPageStyle.components[action.componentIndex],
      });

      tabs[state.currentTab].detailPageStyle.componentDetails.splice(action.componentIndex + 1, 0, {
        ...tabs[state.currentTab].detailPageStyle.componentDetails[action.componentIndex],
      });

      return {
        ...state,
        tabs: JSON.parse(JSON.stringify(tabs)),
      };

    case REORDER_DETAIL_COMPONENTS:
      tabs[state.currentTab].detailPageStyle.components = serialize(
        action.components,
        action.componentDetails
      );
      tabs[state.currentTab].detailPageStyle.componentDetails = [...action.componentDetails];
      return {
        ...state,
        tabs: JSON.parse(JSON.stringify(tabs)),
      };
    case CHANGE_COLOR:
      return {
        ...state,
        iphoneColor: action.color.hex,
      }
    case CHANGE_COLOR_FONT:
      return {
        ...state,
        iphoneColorFont: action.color.hex,
      }
    case CHANGE_COLOR_HEADER:
      return {
        ...state,
        iphoneColorHead: action.color.hex,
      }
    case CHANGE_COLOR_FOOTER:
      return {
        ...state,
        iphoneColorFoot: action.color.hex,
      }
    case CHANGE_COLOR_THEME_LIGHT:
      return {
        ...state,
        iphoneColor: action.colorBody,
        iphoneColorHead: action.colorHead,
        iphoneColorFoot: action.colorFoot,
        iphoneColorFont: action.colorFont,
        iphoneColorHeadFont: action.colorFontHead,
      }
    case CHANGE_COLOR_THEME:
      return {
        ...state,
        iphoneColor: action.colorBody,
        iphoneColorHead: action.colorHead,
        iphoneColorFoot: action.colorFoot,
        iphoneColorFont: action.colorFont,
        iphoneColorHeadFont: action.colorFontHead,
      }
    default:
      return state;
  }
};

export default StyleReducer;
