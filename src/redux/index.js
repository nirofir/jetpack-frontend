import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import { combineReducers } from "redux";

import NavReducer from "./nav";
import LayoutReducer from "./layout";
import MondayReducer from "./monday";
import StyleReducer from "./style";

const reducers = combineReducers({
  NavReducer,
  // AppReducer,
  LayoutReducer,
  MondayReducer,
  StyleReducer,
  // ColumnsReducer,
  // DetailReducer,
});

const initialState = {};

const store = createStore(
  reducers,
  initialState,
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  applyMiddleware(createLogger())
);

export default store;
