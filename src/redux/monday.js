const FETCH_DATA = "FETCH_DATA";
const SELECT_ITEM = "SELECT_ITEM";
const SET_TOKEN = "SET_TOKEN";
const SET_SESSION_TOKEN = "SET_SESSION_TOKEN";
// store
const initialState = null;

export const setSessionToken = (token) => {
  return {
    type : SET_SESSION_TOKEN,
    token
  }
}
// actions
export const setToken = (token) => {
  return {
    type: SET_TOKEN,
    token,
  };
};

export const fetchData = (data) => {
  return {
    type: FETCH_DATA,
    data,
  };
};

export const selectItem = (itemIndex) => {
  return {
    type: SELECT_ITEM,
    itemIndex,
  };
};

// reducers
const MondayReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SESSION_TOKEN:
      return {
        ...state,
        sessionToken: action.token,
      };

    case SET_TOKEN:
      return {
        ...state,
        token: action.token,
      };

    case FETCH_DATA:
      return {
        ...state,
        ...action.data,
      };

    case SELECT_ITEM:
      return {
        ...state,
        itemIndex: action.itemIndex,
      };

    default:
      return state;
  }
};

export default MondayReducer;
