const CHANGE_LAYOUT_PAGE_INDEX = "CHANGE_LAYOUT_PAGE_INDEX";
const GO_BACK_LAYOUT_PAGE = "GO_BACK_LAYOUT_PAGE";
const REMOVE_SEARCH = "REMOVE_SEARCH";
const SHOW_SEARCH = "SHOW_SEARCH";
// state
const initialState = {
  layoutPage: "property",
  prev: [],

};

// actions
export const changeLayoutPage = (layoutPage) => {
  return {
    type: CHANGE_LAYOUT_PAGE_INDEX,
    layoutPage,
  };
};

export const goBackLayoutPage = () => {
  return {
    type: GO_BACK_LAYOUT_PAGE,
  };
};

export const showSearch = (showVal) => {
  return {
    type: SHOW_SEARCH,
    showVal

  };
}

// reducers
const LayoutReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_LAYOUT_PAGE_INDEX:
      return {
        layoutPage: action.layoutPage,
        prev: [...state.prev, state.layoutPage],
      };
    case GO_BACK_LAYOUT_PAGE:
      return {
        layoutPage: state.prev.pop(),
        prev: state.prev,
      };

    case SHOW_SEARCH:
      return {
        state,
        searchShow: action.showVal.value,
      }
    default:
      return state;
  }
};

export default LayoutReducer;
