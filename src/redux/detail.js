import { serialize } from "helper/formater";
import ComponentJSON from "constants/components.json";
import ComponentDetailJSON from "constants/component_details.json";

const FETCH_COMPONENT = "FETCH_DETAIL_COMPONENT";
const ADD_COMPONENT = "ADD_DETAIL_COMPONENT";
const REMOVE_COMPONENT = "REMOVE_DETAIL_COMPONENT";
const UPDATE_COMPONENT = "UPDATE_DETAIL_COMPONENT";
const SELECT_COMPONENT = "SELECT_DETAIL_COMPONENT";
const REORDER_COMPONENTS = "REORDER_DETAIL_COMPONENTS";
const DUPLICATE_COMPONENT = "DUPLICATE_DETAIL_COMPONENT";

// state
const initialState = {
    components: [
        {
            id: "actionText_0",
            icon: "action_text",
            label: "email",
            name: "Action Text"
        },
        {
            id: "title_2",
            icon: "title",
            label: "name",
            name: "Title"
        },
        {
            id: "link_5",
            icon: "link",
            label: "email",
            name: "Link"
        },
        {
            id: "phone_6",
            icon: "phone_",
            label: "phone",
            name: "Phone"
        },
        {
            id: "dateCalendar_7",
            icon: "date",
            label: "name",
            name: "Date"
        },
        {
            id: "timeline_8",
            icon: "timeline",
            label: "Timeline",
            name: "Timeline"
        },
        {
            id: "worldClock_9",
            icon: "clock",
            label: "date4",
            name: "World Clock"
        },
    ],
    componentDetails: [
        {
            "hint": "column",
            "column": "email",
            "title": "",
            "options": true,
            "action": "send_email",
            "data": "email"
        },
        {
            "hint": "title",
            "title": "name",
            "custom_title": "",
            "details": "text",
            "custom_details": "",
            "image": "files5",
            "custom_image": "",
            "design": "url"
        },
        {
            "hint": "link",
            "link": "email",
            "label": "fill_area",
            "title": "fill_area"
        },
        {
            "hint": "column",
            "column": "phone",
            "body": "name",
            "title": ""
        },
        {
            "hint": "column",
            "column": "date4",
            "title": "",
            "format": "DD/MM/YYYY"
        },
        {
            "column": "timeline",
            "title": "",
            "type": 0
        },
        {
            "hint": "column",
            "column": "world_clock",
            "title": "",
            "format": "12"
        },
    ],
    componentIndex: null,
};

// actions
export const fetchComponent = (style) => {
    return {
        type: FETCH_COMPONENT,
        style
    }
}
export const addComponent = (label) => {
    return {
        type: ADD_COMPONENT,
        component: { ...ComponentJSON[label] },
        componentDetail: { ...ComponentDetailJSON[label] },
    };
};
export const removeComponent = (index) => {
    return {
        type: REMOVE_COMPONENT,
        componentIndex: index
    };
};
export const updateComponent = (field, value) => {
    return {
        type: UPDATE_COMPONENT,
        field,
        value,
    };
};
export const selectComponent = (componentIndex) => {
    return {
        type: SELECT_COMPONENT,
        componentIndex,
    };
};
export const duplicateComponent = (componentIndex) => {
    return {
        type: DUPLICATE_COMPONENT,
        componentIndex,
    };
};
export const reorderComponents = (components, componentDetails) => {
    return {
        type: REORDER_COMPONENTS,
        components,
        componentDetails,
    };
};

// reducers
const DetailReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMPONENT:
            return {
                ...action.style
            }
        case ADD_COMPONENT:
            let updatedComponents = [...state.components, action.component];
            let updatedDetails = [...state.componentDetails, action.componentDetail];
            return {
                ...state,
                components: serialize(updatedComponents, updatedDetails),
                componentDetails: updatedDetails,
                componentIndex: state.components.length,
            };
        case REMOVE_COMPONENT:
            return {
                ...state,
                components: state.components.filter(
                    (item, index) => index !== action.componentIndex
                ),
                componentDetails: state.componentDetails.filter(
                    (item, index) => index !== action.componentIndex
                ),
                componentIndex: null,
            };
        case UPDATE_COMPONENT:
            let detail = state.componentDetails;
            detail[state.componentIndex][action.field] = action.value;
            return {
                ...state,
                components: serialize(state.components, detail),
                componentDetails: [...detail],
            };
        case SELECT_COMPONENT:
            return {
                ...state,
                componentIndex: action.componentIndex,
            };
        case DUPLICATE_COMPONENT:
            let newComponents = [...state.components],
                newDetails = [...state.componentDetails];
            newComponents.splice(action.componentIndex + 1, 0, {
                ...newComponents[action.componentIndex],
            });
            newDetails.splice(action.componentIndex + 1, 0, {
                ...newDetails[action.componentIndex],
            });
            return {
                ...state,
                components: serialize(newComponents, newDetails),
                componentDetails: newDetails,
            };
        case REORDER_COMPONENTS:
            return {
                ...state,
                components: serialize(action.components, action.componentDetails),
                componentDetails: [...action.componentDetails],
            };
        default:
            return state;
    }
};

export default DetailReducer;
