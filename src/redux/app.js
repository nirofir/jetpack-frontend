const CHANGE_APP_PAGE_INDEX = "CHANGE_APP_PAGE_INDEX";
const GO_BACK_APP_PAGE = "GO_BACK_APP_PAGE";
const PUBLISH_APP = "PUBLISH_APP";
//maybe not in use
// state
const initialState = {
  appPage: "normal",
  prev: [],
  modalShown: false
};

// action
export const changeAppPage = (pageName) => {
  return {
    type: CHANGE_APP_PAGE_INDEX,
    pageName,
  };
};

export const publishApp = () => {
  initialState.modalShown = !initialState.modalShown
  return {
    type: PUBLISH_APP,
    modalShown: 'show',

  };




}



export const goBackAppPage = () => {
  return {
    type: GO_BACK_APP_PAGE,
  };
};

// reducer
const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_APP_PAGE_INDEX:
      return {
        appPage: action.pageName,
        prev: [...state.prev, state.appPage],
      };

    case GO_BACK_APP_PAGE:
      return {
        appPage: state.prev.pop(),
        prev: state.prev,
      };
    case PUBLISH_APP:
      return {
        modalShown: !state.modalShown


      };

    default:
      return state;
  }
};

export default AppReducer;
