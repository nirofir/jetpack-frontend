import React, { useState, useEffect, useCallback } from "react";
import { useLocation, useHistory, useParams } from "react-router-dom";
import styled from "styled-components";
import { connect } from "react-redux";
import { getAuthUrl } from "helper";
import Header from "header";
import LogoImg from '../assets/img/64px-Monday_logo.svg.png'
import Container from "container";
import Layout from "layout";

import { fetchData, setToken, setSessionToken } from "redux/monday";
import { fetchColumns } from "redux/columns";
import { fetchStyle ,updateEveryProperty,updateProperty, addComponent} from "redux/style"

import { parseQuery, parseColumns } from "helper";
import {  getBoardData, getStyleNoName } from "api/monday";
import loadingGif from "assets/img/loading.gif";

import mondaySdk from "monday-sdk-js";
const monday = mondaySdk();

const AppBox = styled.div`
  width: 100vw;
  height: 100vh;

  background: white;

  color: #333333;
  font-family: Open Sans;
`;

const BodyBox = styled.div`
  width: 100vw;
  height: calc(100vh - 80px);

  position: fixed;

  display: flex;
  flex-direction: column;
`;

const LoadingDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 100vw;
  height: 100vh;
`;

const RefreshBtn = styled.div`
  padding-left: 20px;
  background: #e5e5e5;
  cursor: pointer;
  width: calc(100vw - 370px);
`;

const App = React.memo((props) => {
  const { mondayData, boardId } = props;
  const { updateEveryProperty, fetchData, fetchStyle ,setSessionToken,updateProperty,addComponent} = props;
  let { boardIdParam } = useParams();

  const [isLoading, setLoad] = useState(false);
  const [error, setError] = useState(null);
  const [isFirstTime, setIsFirstTime] = useState(true)
  const location = useLocation();
  let history = useHistory();

  const getBoardIdFromMongoSdk = async () => {
    // if(getCookie("msToken")){
    //   setSessionToken(getCookie("msToken"))
    // }else{
    //   const tokenRes = await monday.get("sessionToken")
    //   setSessionToken(tokenRes.data)
    //   document.cookie = "msToken=" + mondayData.token;
    // }

    const res = await monday.get("context")
    console.log(res)
    return res.data.boardId
    // console.log(res.data.boardId)
  }


  const fetchStyleData = async (boardId) => {
    try {
      console.log(boardId)
      if (!boardId || boardId==="undefined") {
        boardId = await getBoardIdFromMongoSdk();
      }
      const res = await getStyleNoName(boardId);
      fetchStyle(res.data.style)
      console.log(res)
    } catch (e) {

    }
  }
  const doAuth = (() => {
    if (!mondayData) document.location.href = getAuthUrl(boardId);
  });

  //can move to helper function. same function in Published.js
  const fetchBoardData = async (boardId) => {
    try {
      if (!boardId || boardId==="undefined") {
        boardId = await getBoardIdFromMongoSdk();
      }
      console.log(boardId)
      const res = await getBoardData(mondayData.token, boardId);
      console.log(res)

      const data = parseColumns(res.data);
      fetchData({
        items: res.data.items.map((item, index) => ({ ...item, index })),
        columns: data.columns,
        column_json: data.column_json,
        boardId: boardId,
        userId: res.data.userId,
        itemIndex : 0
      });
      updateProperty("item_id", res.data.items[0].item_id.value);


      data.columns.map(column => {
        console.log(column.type)
        switch(column.type){
          case "color":
            return addComponent("Status")
          case "votes":
            return addComponent("Vote")
          case "rating":
            return addComponent("Rating")
          case "location":
            return addComponent("Map")
          case "numeric":
            return addComponent("Number Entry")
          case "columns-battery":
            return addComponent("Progress Bar")
          case "long-text":
            return addComponent("Text Entry")
          default:
            return
            
        }
      })

      updateEveryProperty(data.columns)
      // fetchColumns({data : data.column_json, newData : data.columns});

      setLoad(false);
      history.push(`/app/${boardId || boardIdParam}`);
    } catch (err) {
      // setError("Interval Error");
      // console.error(err);
    }
  };

  useEffect(() => {
    const updateBoard = async () => {
      const { code } = parseQuery(location.search);

      if (isFirstTime && ((mondayData && mondayData.token))) {
        setIsFirstTime(false)
        setLoad(true)
        if (mondayData && mondayData.token) document.cookie = "token=" + mondayData.token + ";SameSite=None;Secure;";
        // console.log("token")
        await fetchBoardData(boardId || boardIdParam || await getBoardIdFromMongoSdk());
        await fetchStyleData(boardId || boardIdParam);
      }

    };
    updateBoard();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location, mondayData]);

  const hasData = useCallback(() => {
    if (!mondayData)
      return (
        <>
          <p style={{ textAlign: "center" }}>You need to sync with Monday.com</p>

          <p style={{ cursor: "pointer", marginTop: '19%' }} onClick={() => doAuth()} className='Logo' >{"Authorize us with "}<img src={LogoImg} /></p>

        </>
      );
    return (
      <BodyBox>
        <RefreshBtn onClick={() => fetchBoardData(mondayData.boardId)}>Refresh Data</RefreshBtn>
        <Container />
        <Layout />
      </BodyBox>
    );
  }, [fetchBoardData, mondayData]);

  const renderBody = useCallback(() => {
    if (error) {
      return <p>{error}</p>;
    } else if (isLoading) {
      return (
        <LoadingDiv>
          <img src={loadingGif} alt="loading" />
        </LoadingDiv>
      );
    } else {
      return (
        <>
          <Header/>
          {hasData()}
        </>
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error, isLoading]);

  return <AppBox>{renderBody()}</AppBox>;
});

const mapStateToProps = (state) => ({
  mondayData: state.MondayReducer,
  styleReducer : state.styleReducer
});

const mapDispatchToProps = (dispatch) => ({
  updateEveryProperty : (columns) => dispatch(updateEveryProperty(columns)),
  fetchData: (param) => dispatch(fetchData(param)),
  fetchStyle: (param) => dispatch(fetchStyle(param)),
  setToken: (token) => dispatch(setToken(token)),
  setSessionToken: (token) => dispatch(setSessionToken(token)),
  fetchColumns: (data) => dispatch(fetchColumns(data)),
  updateProperty: (field, value) => dispatch(updateProperty(field, value)),
  addComponent: (label) => dispatch(addComponent(label))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
