import React from "react";
import styled from "styled-components";
import RDragBox from "widgets/RDragBox";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

const FullBox = styled.div`
  display: flex;
  justify-content: center;
`;

const StatusBox = styled.div`
  width: 300px;
  height: 500px;
`;

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const grid = 8;

const getItemStyle = (draggableStyle, isDragging) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  //   padding: grid * 2,
  margin: `0 0 ${grid}px 0`,

  // change background colour if dragging
  //   background: isDragging ? "lightgreen" : "grey",

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getItemStyle1 = (draggableStyle, isDragging) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  //   padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  boxShadow: isDragging ? `0px 5px 5px rgba(0, 0, 0, 0.25)` : "",

  // change background colour if dragging
  //   background: isDragging ? "lightgreen" : "grey",

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  //   background: isDraggingOver ? "lightblue" : "lightgrey",
  //   padding: grid,
  //   width: 250,
});

class TestBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        { id: "item-0", icon: "printer", name: "title", label: "title" },
        { id: "item-1", icon: "note", name: "note", label: "about" },
        { id: "item-2", icon: "mail", name: "email", label: "email" },
        { id: "item-3", icon: "phone", name: "phone", label: "phone" },
        { id: "item-4", icon: "no_select", name: "separator", label: "" },
        { id: "item-5", icon: "comments", name: "comment", label: "email" },
      ],
    };
    this.onDragEnd = this.onDragEnd.bind(this);
  }

  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(
      this.state.items,
      result.source.index,
      result.destination.index
    );

    this.setState({
      items,
    });
  }

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity
  render() {
    return (
      <FullBox>
        <StatusBox>
          <DragDropContext onDragEnd={this.onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided, snapshot) => (
                <div
                  ref={provided.innerRef}
                  style={getListStyle(snapshot.isDraggingOver)}
                  {...provided.droppableProps}
                >
                  {this.state.items.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <div>
                          <div
                            ref={provided.innerRef}
                            {...provided.dragHandleProps}
                            {...provided.draggableProps}
                            style={getItemStyle(
                              provided.draggableProps.style,
                              snapshot.isDragging
                            )}
                          >
                            <RDragBox
                              {...item}
                              ref={provided.innerRef}
                              {...provided.dragHandleProps}
                              {...provided.draggableProps}
                              style={getItemStyle1(
                                provided.draggableProps.style,
                                snapshot.isDragging
                              )}
                            />
                          </div>
                          {provided.placeholder}
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </StatusBox>
      </FullBox>
    );
  }
}

export default TestBox;
