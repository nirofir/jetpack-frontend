import React, { useState, useEffect, useCallback } from "react";
import { useLocation, useHistory, useParams } from "react-router-dom";
import styled from "styled-components";
import { connect } from "react-redux";
import Header from "container/iphone/header";
import Body from "container/iphone/body";
import Footer from "container/iphone/footer";
import Login from "container/login/login"
import { fetchData, setToken } from "redux/monday";
import { fetchColumns } from "redux/columns";
import { fetchStyle } from "redux/style"

import { parseColumns } from "helper";
import {  getBoardData, getStyle } from "api/monday";
import loadingGif from "assets/img/loading.gif";
import { getAuthUrlMobile } from "helper";

const MobileBox = styled.div`
  height: 100%;
  
  overflow-x: hidden;                                                                                                                  
  display: flex;
  flex-direction: column;
`;

const LoadingDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 100vw;
  height: 100vh;
`;

const RefreshBtn = styled.div`
  padding-left: 20px;
  background: #e5e5e5;
  cursor: pointer;
  width: calc(100vw - 370px);
`;



const Published = React.memo((props) => {
  const { mondayData, boardName, boardId } = props;
  const { fetchData, setToken, fetchColumns, fetchStyle, iphoneColor } = props;

  const [isLoading, setLoad] = useState(false);
  const [error, setError] = useState(null);
  const [isFirstTime, setIsFirstTime] = useState(true)
  const [isFirstTimeCode, setIsFirstTimeCode] = useState(true)
  const [isFirstTimeCookie, setIsFirstTimeCookie] = useState(true)
  const [firstTimeLogin, setFirstTimeLogin] = useState(true)
  const location = useLocation();
  let history = useHistory();
  let { boardIdParam, boardNameParam } = useParams();

  const fetchStyleData = async (boardId, boardName) => {
    try {
      console.log(boardId)
      if(boardId && boardName && boardName !== "undefined"){
        console.log(boardName)
        const res = await getStyle(boardId, boardName);
        res.data.style && fetchStyle(res.data.style)
      }
      // {
      // fetchStyle{{}}
      // }

    } catch (e) {

    }
  }

  const doAuth = () => {
    document.location.href = getAuthUrlMobile(boardIdParam, boardNameParam);
  };

  const fetchBoardData = useCallback(async () => {
    try {
      console.log(boardId || boardIdParam)
      const res = await getBoardData(mondayData.token, boardId || boardIdParam);
      const data = parseColumns(res.data);
      fetchData({
        items: res.data.items.map((item, index) => ({ ...item, index })),
        columns: data.columns,
        column_json: data.column_json,
        boardId: boardId,
      });

      fetchColumns(data.column_json);
      setLoad(false);
      // console.log(boardName,boardNameParam,boardIdParam,boardNameParam)
      history.push(`/ninja/${boardId || boardIdParam}/${boardName || boardNameParam}`);
    } catch (err) {
      setError("Interval Error");
      console.error(err);
    }
  });

  useEffect(() => {
    console.log("monady")
    const updateBoard = async () => {
      if (isFirstTime && ((mondayData && mondayData.token))) {
        setIsFirstTime(false)
        setLoad(true)
        await fetchBoardData();
        await fetchStyleData(boardId || boardIdParam, boardName || boardNameParam);
      } else if (firstTimeLogin) {
        setFirstTimeLogin(false)
      };

    };
    updateBoard();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  });

  const hasData = () => {

    if (!mondayData)
      return (

        <Login clicked={() => doAuth()}>
          Login With
        </Login >
        // <span clicked={() => doAuth()}>click to login</span>

      );
    return (
      <MobileBox style={{ backgroundColor: iphoneColor }}>
        {mondayData ? (
          <>
            <Header />
            <Body />
            <Footer />
          </>
        ) : (
            <p onClick={() => fetchBoardData()}>You need to authenticate with Monday</p>
          )}
      </MobileBox>
    );
  };

  const renderBody = () => {
    if (error) {
      return <p>{error}</p>;
    } else if (isLoading) {
      return (
        <LoadingDiv>
          <img src={loadingGif} alt="loading" />
        </LoadingDiv>
      );
    } else {
      return (
        <>
          {hasData()}
        </>
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  };

  return <>{renderBody()}</>;
});

const mapStateToProps = (state) => ({
  mondayData: state.MondayReducer,
  iphoneColor: state.StyleReducer.iphoneColor,
});

const mapDispatchToProps = (dispatch) => ({
  fetchData: (param) => dispatch(fetchData(param)),
  setToken: (token) => dispatch(setToken(token)),
  fetchColumns: (data) => dispatch(fetchColumns(data)),
  fetchStyle: (data) => dispatch(fetchStyle(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Published);
