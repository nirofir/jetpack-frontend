import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => (
    <>
        <Link to="/app">Go to Editor</Link>
        <br />
        <Link to="/ninja">Go to mobile view</Link>
    </>
);

export default Home;