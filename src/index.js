import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
// import { BrowserRouter, Switch, Route } from "react-router-dom";

// import Home from './components/Home';
// import App from "./components/App";
// import Mobile from './components/Mobile';
import Router from 'router';
import * as serviceWorker from "./serviceWorker";

import store from "./redux";
import "./index.css";

ReactDOM.render(
  <Provider store={store}>

    <Router />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
