import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { BrowserRouter, Switch, Route } from "react-router-dom";
//import { useLocation, useHistory, useParams,   useRouteMatch} from "react-router-dom";

import { connect } from "react-redux";

import Home from "./components/Home";
import App from "./components/App";
// import Mobile from "./components/Mobile";
import Published from "components/Published";
import { fetchData, setToken } from "redux/monday";
import { parseQuery, getCookie } from "helper";
// import { initialize } from "helper/monday";
import loadingGif from "assets/img/loading.gif";

import { getNewToken, getNewTokenMobile } from "api/monday";

const LoadingDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 100vw;
  height: 100vh;
`;

const Router = (props) => {
  const { setToken } = props;
  const [isLoading, setLoad] = useState(false);
  const [isFirstTimeCode, setIsFirstTimeCode] = useState(true)
  const [isFirstTimeCookie, setIsFirstTimeCookie] = useState(true)
  const [isApp, setIsApp] = useState()
  const [code, setCode] = useState()
  const [boardState, setBoardState] = useState({ name: "", id: "" })
  const loc = window.location


  useEffect(() => {
    if (loc.href.includes("code")) {
      const { code } = parseQuery(loc.search);
      const lol = parseQuery(loc.search);
      console.log("rcode", code)
      console.log("board state", parseQuery(loc.search))
      setCode(code)
      loc.pathname === "/app" ? setIsApp(true) : setIsApp(false)

      loc.pathname === "/app" ? setBoardState({ id: lol.state.split("_")[0] })
        : setBoardState({ name: lol.state.split("_")[1], id: lol.state.split("_")[0] })
    };
  }, [loc])

  useEffect(() => {
    const getToken = async () => {
      if (code && isFirstTimeCode) {
        setIsFirstTimeCode(false)
        // setLoad(true);
        try {
          const res = isApp ? await getNewToken(code) : await getNewTokenMobile(code)
          console.log(res)
          setToken(res.data.token);
          document.cookie = "token=" + res.data.token;
        } catch (err) {
          console.error(err);
        }
      } else if (getCookie("token") && isFirstTimeCookie) {
        console.log(getCookie("token"))
        setIsFirstTimeCookie(false)
        setToken(getCookie("token"))
      }
    }
    getToken()
  }, [code, isApp, isFirstTimeCode, isFirstTimeCookie, setToken])

  return isLoading ? (
    <LoadingDiv>
      <img src={loadingGif} alt="loading" />
    </LoadingDiv>
  ) : (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/app/:boardIdParam">
            <App />
          </Route>
          <Route path="/app/">
            <App boardId={boardState.id} />
          </Route>
          <Route path="/ninja/:boardIdParam/:boardNameParam">
            <Published />
          </Route>
          <Route path="/ninja/">
            <Published boardName={boardState.name} boardId={boardState.id} />
          </Route>
        </Switch>
      </BrowserRouter>
    );
};

const mapDispatchToProps = (dispatch) => ({
  fetchData: (param) => dispatch(fetchData(param)),
  setToken: (token) => dispatch(setToken(token)),
});

export default connect(null, mapDispatchToProps)(Router);
// export default Router;
