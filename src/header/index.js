import React, {  useState } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
// import { Link } from 'react-router-dom';
import womanLogo from '../assets/svg/modalWoman.svg'
import exitBtn from '../assets/svg/exitBtn.svg'
import linkInput from '../assets/svg/linkInput.svg'
import { updateStyle ,updateStyleNoName} from "api/monday";
import { changePage } from "redux/nav";
import Icon from "widgets/Icon";
import QRCode from 'qrcode.react'
import Modal from 'react-modal'
import Nav from "constants/nav";
import { getAuthUrl } from "helper";


const size = {
  webPageS: '500px',
  webPageFull: '1680px',
  webPageM: '1000px'

}
const mode = {
  small: `(min-width: ${size.webPageS})`,
  medium: `(min-width: ${size.webPageM})`,
  full: `(min-width: ${size.webPageFull})`,

}


const HeaderBox = styled.div`
  width: 100vw;
  height: 80px;

  color: #333333;
  background: #ffffff;
  box-shadow: 0px 5px 17px rgba(0, 0, 0, 0.11);

  display: flex;
  justify-content: space-between;
`;

const NavBar = styled.div`
  width: 30%;

  display: flex;
  align-items: center;
  

  padding-left: 23px;
  padding-right: 23px;

  @media  ${mode.small} {
    margin-left: -13px;
  }
  
`;

const NavControl = styled.div`
  width: 10%;

  display: flex;
  
  justify-content: space-between;

  padding-left: 23px;
  flex-direction: row-reverse;
  padding-right: 23px;
  @media  ${mode.small} {
    width: fit-content;
    margin-right: -28px;
  }
  
`;

const NavItem = styled.div`
  
  padding : 10px;

  display: flex;
  
  align-items: center;

  cursor: pointer;

  border: none;
  border-radius: 32px;

  :hover,
  &.active {
    background: rgba(0, 154, 255, 0.15);
  }

  & svg {
    width: 18px;
    height: 18px;
  }

  & svg path {
    fill: #333333;
  }
  @media  ${mode.small} {
    margin-left : 10px;
  }
`;

const Divider = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  @media ${mode.small} {
    margin-left : 10px;
  }
`;

const Button = styled.button`
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 22px;

  
  align-items: center;
  text-align: center;

  height: 36px;
  margin-top: 21px;
  
  border: 1px solid #36a7fd;
  border-radius: 28px;

  cursor: pointer;
  
  &.outlined {
    background: transparent;
    color: #36a7fd;
  }

  &.filled {
    background: #36a7fd;
    color: white;
  }
  @media ${mode.small} {
    margin-right: 10px;
    width: fit-content;
  }
`;
const Header = ({ menuIndex, mondayData, publishApp, changePage, styleReducer }) => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const boardId = mondayData && mondayData.boardId
  function openModal() {
    setIsOpen(true);
  }
  function afterOpenModal() {
  }
  function closeModal() {
    setIsOpen(false);
  }
  const Modal1 = ({ closeModal, onHandleShare, modalIsOpen, afterOpenModal }) => {
    const [pageIndex, setPageIndex] = useState(0)
    const [QRvalue, setQRvalue] = useState('my-jet-app');
    const learnMore = <>Learn more.</>
    const changeModalPageIndex = (i) => {
      console.log(i)
      setPageIndex(i)
      if(i === 2){
        onHandleShare(QRvalue)
      }
    }
    const firstDiv = {
      container: {
        background: '#FFFFFF',
        borderRadius: '10px',
        width: '487',
        height: '481px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },

      header: {
        fontFamily: 'Open Sans',
        fontStyle: 'normal',
        paddingLeft: '40px',
        marginTop: '60px'
        ,
        fontSize: '24px',
        lineHeight: '33px',
        fontWeight: 'bold',


        alignItems: 'center',
        textAlign: 'center',
        color: '#333333'
      },
      sentence: {
        fontFamily: 'Open Sans',
        fontStyle: 'normal',

        fontSize: '18px',
        lineHeight: '25px',

        margin: '15px',
        display: 'inline-block',
        alignItems: 'center',
        textAlign: 'center',
        color: '#333333'
      },
      cancelBtn: {


        fontFamily: 'Open Sans',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: '16px',
        lineHeight: '22px',
        cursor: 'pointer',
        borderStyle: 'solid',
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        flexFlow: 'row',
        order: '0',
        color: '#36A7FD',
        borderColor: '#36A7FD',
        borderRadius: '28px',
        background: '#FFFFFF',
        margin: '10px'

      },
      buttns: {
        display: 'flex',
        flexFlow: 'row',
        justifyContent: 'flex-end',
        width: '457px',
        marginTop: '20px'


      },
      exitBtn: {

        width: 'inherit',

        margin: '10px',
        marginLeft: '457px',
      },
      publishBtn: {
        borderColor: '#36A7FD',
        borderStyle: 'solid',
        display: 'inline-block',
        order: '1',
        alignSelf: 'center',
        margin: '10px 0px',
        fontFamily: 'Open Sans',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: '16px',
        lineHeight: '22px',

        cursor: 'pointer',

        alignItems: 'center',
        textAlign: 'center',


        color: '#FFFFFF',

        borderRadius: '28px',
        background: '#36A7FD'
      }

    }
    const secondDiv = {
      container: {
        background: '#FFFFFF',
        borderRadius: '10px',
        width: 'inherit',
        height: 'inherit',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
      header: {
        fontFamily: 'Open Sans',
        fontStyle: 'normal',

        fontSize: '18px',
        lineHeight: '33px',


        marginTop: '75px',

        display: 'inline-block',
        alignItems: 'center',
        textAlign: 'center',
        color: '#333333'
      },
      link: {
        fontFamily: 'Open Sans',
        fontStyle: 'normal',

        fontSize: '18px',
        lineHeight: '33px',
        width : "100%",

        marginTop: '10px',

        display: 'inline-block',
        alignItems: 'center',
        textAlign: 'center',
        color: '#333333'
      },
      exitBtn: {
        margin: '10px',
        marginLeft: '457px',
      },
      sentence: {
        fontFamily: 'Open Sans',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: '18px',
        lineHeight: '25px',


        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color: '#333333'
      },
      cancelBtn: {


        fontFamily: 'Open Sans',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: '16px',
        lineHeight: '22px',

        borderStyle: 'solid',
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        flexFlow: 'row',
        order: '0',
        color: '#36A7FD',
        borderColor: '#36A7FD',
        borderRadius: '10px',
        background: '#FFFFFF',
        margin: '10px'

      },
      buttns: {
        display: 'flex',
        flexFlow: 'row',
        justifyContent: 'flex-end',
        width: '100%',
        cursor: 'pointer'

      },
      row: {
        display: 'flex',
        flexFlow: 'row',
        width: '100%'
      },
      copyBtn: {
        borderColor: '#36A7FD',
        borderStyle: 'solid',
        display: 'inline-block',
        order: '1',
        alignSelf: 'center',
        margin: '10px 0px',
        fontFamily: 'Open Sans',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: '16px',
        lineHeight: '22px',

        cursor: 'pointer',

        alignItems: 'center',
        textAlign: 'center',
        color: '#FFFFFF',

        borderRadius: '28px',
        background: '#36A7FD',
        width: '71px',
        height: '27px',
      },

      input: {
        marginRight: '20px',
        width: '100%',
        border: '1px solid #BDBDBD',
        boxSizing: 'border-box',
        borderRadius: '28px',
        backgroundImage: `url(${linkInput}) `,
        backgroundPosition: '10px',
        backgroundRepeat: 'none',
        paddingLeft: '28px',
        backgroundRepeat: 'no-repeat',
      },
      appLink: {
        fontFamily: 'Open Sans',
        fontStyle: 'normal',
        width: '100%',
        fontWeight: 'bold',
        margin: '0px 0px 14px 28px',
        fontSize: '16px',
        lineHeight: '22px',
      },
      qr: {
        marginTop: '34px',
      },
      appLinkBeforClick: {
        fontFamily: 'Open Sans',
        fontStyle: 'normal',
        width: '100%',
        fontWeight: 'bold',

        fontSize: '16px',
        lineHeight: '22px',
        margin: '308px 0px 14px 28px',
      },
    }

    const customStyles = {
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        width: '487px',
        height: '481px',
        marginRight: '-50%',
        borderRadius: '10px',

        transform: 'translate(-50%, -50%)',
      }
    };

    const ninjaLinkPrefix = `https://app.jetpackapps.co/ninja/${boardId}/`
    Modal.setAppElement('#root')
    Modal.defaultStyles.overlay.backgroundColor = 'rgba(51, 51, 51, 0.8)'
    const setQRCode = event => {
      setQRvalue(event.target.value)
    }

    const getQR_or_BeforePublish = () => {
      switch (pageIndex) {
        case 0:
          return <div style={firstDiv.container}>
            <div style={firstDiv.exitBtn}>
              <img onClick={closeModal} src={exitBtn} />
            </div>
            <div>
              <img src={womanLogo} />
            </div>
            <div>
              <div style={firstDiv.header}>First , Publish your app!</div>
              <p style={firstDiv.sentence}>Publishing allows you to open the app on your phone and share it with others.
             <span style={{ textDecoration: 'underline' }}>{learnMore}</span>
              </p>
            </div>
            <div style={firstDiv.buttns}>
              <button style={firstDiv.cancelBtn} onClick={closeModal}>Cancel</button>
              <button style={firstDiv.publishBtn} onClick={() => changeModalPageIndex(1)}>Publish App</button>
            </div>
          </div >

        case 1:
          return <div style={secondDiv.container}
          >
            <div style={secondDiv.exitBtn}>
              <img onClick={closeModal} src={exitBtn} />
            </div>
            <div style={secondDiv.appLinkBeforClick}></div>
            <div style={secondDiv.row}>

              <input style={secondDiv.input} type='text' onChange={setQRCode} placeholder='my-jet-app' />
              <div style={secondDiv.copyBtn}>copy</div>

            </div>
            {QRvalue && <div style={secondDiv.copyBtn} onClick={() => changeModalPageIndex(2)}>Publish</div>}

          </div>
        case 2:
          return (<div style={secondDiv.container}
          >
            <div style={secondDiv.exitBtn}>
              <img onClick={closeModal} src={exitBtn} />
            </div>
            <QRCode style={secondDiv.qr} value={ninjaLinkPrefix + QRvalue} />
            <p style={secondDiv.link}>Your app is here and ready to be shared</p>
            <input disabled style={secondDiv.link} value={ninjaLinkPrefix + QRvalue}/>
            <p style={secondDiv.header}>Scan with camera to install app. <span style={{ textDecoration: 'underline' }}>Learn How</span></p>

            <div style={secondDiv.header}>Published!</div>
            <div style={secondDiv.row}>

              {/* <input style={secondDiv.input} type='text' onChange={setQRCode} placeholder='my-jet-app' />
              <div style={secondDiv.copyBtn}>copy</div> */}

            </div>
          </div>)
        default:
      }
    }
    return (
      <Modal
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >{getQR_or_BeforePublish()}
      </Modal>
    )
  }





  const doAuth = (() => {
    if (!mondayData) document.location.href = getAuthUrl(boardId);
  });

  const onHandleShare = (name) => {
    updateStyle(styleReducer, mondayData.boardId, name);
    updateStyleNoName(styleReducer, mondayData.boardId);
  }


  return (

    boardId && mondayData ? (
      <HeaderBox>
        <NavBar>

          {Nav.map((item, index) => {

            return (
              item.class && boardId ? (
                <NavItem
                  key={`nav-item-${index}`}
                  className={menuIndex === index ? "active" : ""}
                  onClick={() => changePage(index)}>
                  <Icon name={item.icon} />
              &nbsp;
                  {item.label}
                </NavItem>
              ) : (
                  <Divider key={`nav-item-${index}`}>
                    <Icon name={item.icon} />
                  </Divider>
                )
            )
          }
          )}
        </NavBar>

        <NavControl>
          <Button className="filled" onClick={() => openModal()} style={{ marginRight: "15px" }}>
            Share App
        </Button>
          <Button className="filled" onClick={() => updateStyleNoName(styleReducer, mondayData.boardId)} style={{ marginRight: "15px" }}>
            Save
          </Button>
        </NavControl>
        <Modal1 closeModal={closeModal}
          onHandleShare={onHandleShare}
          modalIsOpen={modalIsOpen}
          afterOpenModal={afterOpenModal} />


      </HeaderBox >


    ) :
      <div />


  );
};

const mapStateToProps = (state) => ({
  menuIndex: state.NavReducer,
  mondayData: state.MondayReducer,
  styleReducer: state.StyleReducer,
  // token : state.MondayReducer.token
  // publishApp: state.AppReducer
});

const mapDispatchToProps = (dispatch) => ({
  changePage: (menu) => dispatch(changePage(menu)),
  // publishApp: () => dispatch(publishApp())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
