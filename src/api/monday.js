import axios from "axios";

import { API_END_POINT, API_REDIRECT_URI , API_REDIRECT_URIMobile} from "constants/constants";

export const fetchToken = async (code) => {
  return await axios.post(`${API_END_POINT}/monday/token`, {
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
};

export const saveToken = async (token) => {
  return await axios.post(
    `${API_END_POINT}/monday/saveToken`,
    { token },
    {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    }
  );
};

export const getNewToken = async (code) => {
  return await axios.post(`${API_END_POINT}/?code=${code}&redirect_uri=${API_REDIRECT_URI}`, {
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
};
export const getNewTokenMobile = async (code) => {
  return await axios.post(`${API_END_POINT}/?code=${code}&redirect_uri=${API_REDIRECT_URIMobile}`, {
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
};

export const getBoardData = async (token,boardId) => {
  return await axios.post(
    `${API_END_POINT}/board`,
    {
      token,
      boardId
    },
    {
      headers: {
        "Access-Control-Allow-Origin": "*",

        // "Content-Type": "application/json",
        // "Access-Control-Allow-Origin": "*",
      },
    }
  );
};

// export const fetchBoardData = async (token,boardId) => {
//   return await axios.post(
//     `${API_END_POINT}/monday/board`,
//     {
//       token,
//       boardId
//     },
//     {
//       headers: {
//         "Content-Type": "application/json",
//         "Access-Control-Allow-Origin": "*",
//       },
//     }
//   );
// };

export const getPersonDetail = async (ids) => {
  return await axios.post(
    `${API_END_POINT}/person`,
    {
      ids,
    },
    {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    }
  );
};

export const getStyle = async (boardId,boardName) => {
  return await axios.get(`${API_END_POINT}/style/${boardId}/${boardName}`, {
    headers: {
      // "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
};
export const getStyleNoName = async (boardId,boardName) => {
  return await axios.get(`${API_END_POINT}/style/${boardId}`, {
    headers: {
      // "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });
};

export const updateStyle = async (style,boardId,boardName) => {
  return await axios.post(
    `${API_END_POINT}/style`,
    {
      style: JSON.stringify(style),
      // token : JSON.stringify(token),
      boardId : boardId,
      boardName
    },
    {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    }
  );
};
export const updateStyleNoName = async (style,boardId) => {
  return await axios.post(
    `${API_END_POINT}/style`,
    {
      style: JSON.stringify(style),
      boardId : boardId,
    },
    {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    }
  );
};

export const updateField = async ({token,boardId,columnId,itemId,update}) => {
  await axios.post(
    `${API_END_POINT}/monday/update`,
    {
      token : token,
      updateBody : {boardId,columnId,itemId,update}
    },
    {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    }
  )
  return
};

