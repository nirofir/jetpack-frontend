import axios from "axios";

import { MAPBOX_API_KEY } from "constants/constants";

export const getGeometry = async (location) => {
  return await axios.get(
    `https://api.mapbox.com/geocoding/v5/mapbox.places/${location}.json?access_token=${MAPBOX_API_KEY}`
  );
};
