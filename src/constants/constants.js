export const CLIENT_ID = "00f805498a060d4040a8bf7f851625fe";


export const API_END_POINT =  
document.location.href.includes("localhost") ?
 "http://localhost:4000"
: `${document.location.origin}/api`;


export const API_REDIRECT_URI = 
  document.location.href.includes("localhost") 
            ? "http://localhost:8301/app" :
           `${document.location.origin}/app`;

export const API_REDIRECT_URIMobile = 
  document.location.href.includes("localhost")
  ? "http://localhost:8301/ninja"
  : `${document.location.origin}/ninja`;

export const MAPBOX_API_KEY =
  "pk.eyJ1Ijoibmlyb2ZpciIsImEiOiJja2JtMXUwdm4xZW1xMnFsOTA2cHZodzNxIn0.vpkWQWDkL447Y4VXhEMESg";

export const FRAME_URL = "/assets/frames/";

export const STYLE_WIDGETS = [
  {
    label: "List",
    icon: "list",
    id: "list",
  },
  {
    label: "Compact",
    icon: "compact",
    id: "compact",
  },
  {
    label: "Tiles",
    icon: "tiles",
    id: "tiles",
  },
  {
    label: "Calendar",
    icon: "calendar",
    id: "calendar",
  },
  {
    label: "Map",
    icon: "map",
    id: "map",
  },
  {
    label: "Details",
    icon: "details",
    id: "details",
  },
];

export const IMAGE_VIEW_METHODS = [
  {
    icon: "world",
    id: "url",
    label: "URL",
  },
];
export const IMAGE_FILL_METHODS = [
  {
    id: "fill_area",
    icon: "",
    label: "Fill the area",
  },
  {
    id: 'whole_image',
    icon: '',
    label: 'Show the whole image'
  }
];
export const IMAGE_STYLE_METHODS = [

  {
    id: "edge_edge",
    icon: "",
    label: "Edge to edge",
  },
  {
    id: 'insert_content',
    icon: '',
    label: 'Insert with content'
  }
]
export const TILE_SHAPES = [
  {
    icon: "square",
    id: "square",
    label: "Square",
  },
  {
    icon: "horizontal3_1",
    id: "horizontal3_1",
    label: "3:1",
  },
  {
    icon: "horizontal3_2",
    id: "horizontal3_2",
    label: "3:2",
  },
  {
    icon: "circle",
    id: "circle",
    label: "Circle",
  },
  {
    icon: "horizontal4_3",
    id: "horizontal4_3",
    label: "4:3",
  },
  {
    icon: "vertical2_3",
    id: "vertical2_3",
    label: "2:3 (vertical)",
  },
  {
    icon: "vertical3_4",
    id: "vertical3_4",
    label: "3:4 (vertical)",
  },
];
export const OVERLAYS = [
  {
    icon: "no_select",
    id: "none",
    label: "",
  },
];

export const STATUS_VALUES = [
  { color: "#009AFF", value: "Working in Progress" },
  { color: "#2AD27F", value: "Done" },
  { color: "#F62765", value: "Stuck" },
  { color: "#FBD024", value: "Negotiations" },
  { color: 'transparent', value: "Others" }
];

export const STATUS_VALUES2 = [
  { color: "#ff9933", value: 0 },
  { color: "#00c875", value: 1 },
  { color: "#e2445c", value: 2 },
  { color: "#0086c0", value: 3 },
  { color: "#a25ddc", value: 4 },
  { color: "#c4c4c4", value: 5 },
  { color: "#037f4c", value: 6 },
  { color: "#579bfc", value: 7 },
  { color: "#cab641", value: 8 },
  { color: "#ffcb00", value: 9 },
  { color: "#000000", value: 10 },
  { color: "#F62765", value: 11 },
  { color: "#F62765", value: 12 },
  { color: "#F62765", value: 13 },
  { color: "#F62765", value: 14 },
  { color: "#F62765", value: 15 },
  { color: "#F62765", value: 16 },
  { color: "#F62765", value: 17 },
  { color: "#F62765", value: 18 },
  { color: "#F62765", value: 19 },
  { color: "#F62765", value: 101 },
  { color: "#F62765", value: 102 },
  { color: "#F62765", value: 103 },
  { color: "#F62765", value: 104 },
  { color: "#F62765", value: 105 },
  { color: "#F62765", value: 106 },
  { color: "#F62765", value: 107 },
  { color: "#F62765", value: 108 },
  { color: "#F62765", value: 109 },
  { color: "#F62765", value: 110 },
];



export const MONTHS = [
  "JAN",
  "FEB",
  "MAR",
  "APR",
  "MAY",
  "JUN",
  "JUL",
  "AUG",
  "SEP",
  "OCT",
  "NOV",
  "DEC",
];
export const RATING_VALUES = [
  { isChecked: true },
  { isChecked: true },
  { isChecked: false },
  { isChecked: false },
  { isChecked: false },
];
export const LIKE_VALUES = [
  { isChecked: true },
  { isChecked: true },
  { isChecked: true },
  { isChecked: false },
  { isChecked: false },
];

export const COMPONENT_STYLES = [
  {
    label: "Text",
    items: [
      {
        icon: "action_text",
        label: "Action Text",
      },
      {
        icon: "action_text",
        label: "Text",
      },
      {
        icon: "rich_text",
        label: "Rich Text",
      },
      {
        icon: "tags",
        label: "Tags",
      },
    ],
  },
  {
    label: "Numbers",
    items: [
      {
        icon: "date",
        label: "Date",
      },
      {
        icon: "pin",
        label: "Item ID",
      },
    ],
  },
  {
    label: "People",
    items: [
      {
        icon: "contact",
        label: "People",
      },
    ],
  },
  {
    label: "Layout",
    items: [
      {
        icon: "separator",
        label: "Separator",
      },
      {
        icon: "title",
        label: "Title",
      },
    ],
  },
  {
    label: "Media",
    items: [
      {
        icon: "image_",
        label: "Image",
      },
      {
        icon: "movie",
        label: "Video",
      },
      {
        icon: "map_pointer",
        label: "Map",
      },
    ],
  },
  {
    label: "Buttons",
    items: [
      {
        icon: "ok",
        label: "Button",
      },
      {
        icon: "link",
        label: "Link",
      },
      {
        icon: "phone_",
        label: "Phone",
      },
    ],
  },
  {
    label: "Lists",
    items: [
      {
        icon: "list_",
        label: "Inline List",
      },
    ],
  },
  {
    label: "Pickers",
    items: [
      {
        icon: "date_",
        label: "Date Time",
      },
      {
        icon: "clock",
        label: "World Clock",
      },
      {
        icon: "status",
        label: "Status",
      },
      {
        icon: "rating",
        label: "Rating",
      },
      {
        icon: "vote",
        label: "Vote",
      },
    ],
  },
  {
    label: "Logs",
    items: [
      {
        icon: "todo",
        label: "Creation Log",
      },
      {
        icon: "reload_",
        label: "Last Updated",
      },
    ],
  },
  {
    label: "Charts",
    items: [
      {
        icon: "progress",
        label: "Progress Bar",
      },
      {
        icon: "timeline",
        label: "Timeline",
      },
    ],
  },
  {
    label: "Entry Fields",
    items: [
      {
        icon: "comment",
        label: "Comments",
      },
      {
        icon: "text_entry",
        label: "Text Entry",
      },
      {
        icon: "notes",
        label: "Notes",
      },
      {
        icon: "number_entry",
        label: "Number Entry",
      },
    ],
  },
];
export const COMPONENT_LIST_STYLES = [
  {
    label: "Text",
    items: [
      {
        icon: 'action_text',
        label: 'Action Text',
        text: 'Action Text -- A labeled value'
      },
      {
        icon: 'action_text',
        label: 'Text',
        text: 'Text -- A box full of text'
      },
      {
        icon: 'rich_text',
        label: 'Rich Text',
        text: 'Rich Text -- Show rich text'
      },
      {
        icon: 'tags',
        label: 'Tags',
        text: 'Tags -- List of Tags'
      }
    ]
  },
  {
    label: 'Numbers',
    items: [
      {
        icon: 'date',
        label: 'Date',
        text: 'Date -- Show a date picker'
      },
      {
        icon: 'pin',
        label: 'Item ID',
        text: 'Item ID -- Show item Ids'
      }
    ]
  },
  {
    label: 'People',
    items: [
      {
        icon: 'contact',
        label: 'People',
        text: 'People -- Show a people card'
      }
    ]
  },
  {
    label: "Layout",
    items: [
      {
        icon: "separator",
        label: "Separator",
        text: "Separator -- A horizontal line",
      },
      {
        icon: "title",
        label: "Title",
        text: "Title -- A title, subtitle, and image",
      },
    ],
  },
  {
    label: "Media",
    items: [
      {
        icon: "image_",
        label: "Image",
        text: "Image -- Display an image from link",
      },
      {
        icon: "map_pointer",
        label: "Map",
        text: "Map -- A map opens full screen",
      },
    ],
  },
  {
    label: "Buttons",
    items: [
      {
        icon: "ok",
        label: "Button",
        text: "Button -- Tap to perform an action",
      },
      {
        icon: "link",
        label: "Link",
        text: "Link -- A link that opens when tap it",
      },
      {
        icon: "phone_",
        label: "Phone",
        text: "Phone -- A number that can call/text",
      },
    ],
  },
  {
    label: "Lists",
    items: [
      {
        icon: "list_",
        label: "Inline List",
        text: "Inline List -- An inline collection of related items",
      },
    ],
  },
  {
    label: "Pickers",
    items: [
      {
        icon: "date_",
        label: "Date",
        text: "Date -- Display and edit a date",
      },
      {
        icon: "clock",
        label: "World Clock",
        text: "World Clock -- Show world clock",
      },
      {
        icon: "status",
        label: "Status",
        text: "Status -- Show item status",
      },
      {
        icon: "rating",
        label: "Rating",
        text: "Rating -- Show relevant rating",
      },
      {
        icon: "vote",
        label: "Vote",
        text: "Vote -- Show voting option",
      },
    ],
  },
  {
    label: "Logs",
    items: [
      {
        icon: "todo",
        label: "Creation Log",
        text: "Creation Log -- Display creation log entry",
      },
      {
        icon: "reload_",
        label: "Last Updated",
        text: "Last Updated -- Show last update information",
      },
    ],
  },
  {
    label: "Charts",
    items: [
      {
        icon: "progress",
        label: "Progress Bar",
        text: "Progress Bar -- A bar showing progress",
      },
      {
        icon: "timeline",
        label: "Timeline",
        text: "Timeline -- Display timeline",
      },
    ],
  },
  {
    label: "Entry Fields",
    items: [
      {
        icon: "comment",
        label: "Comments",
        text: "Comments -- Allows users to post comments",
      },
      {
        icon: "text_entry",
        label: "Text Entry",
        text: "Text Entry -- Edit a short piece of text",
      },
      {
        icon: "notes",
        label: "Notes",
        text: "Notes -- Text that you can read and edit",
      },
      {
        icon: "number_entry",
        label: "Number Entry",
        text: "Number Entry -- Edit a number",
      },
    ],
  },
];
export const TEXT_MENUS = ["LAYOUT", "FEATURES"];
export const TEXT_ALIGNMENTS = [
  {
    icon: "text_align_left",
    label: "left",
  },
  {
    icon: "text_align_center",
    label: "center",
  },
  {
    icon: "text_align_right",
    label: "right",
  },
  {
    icon: "text_align_fill",
    label: "justify",
  },
];
// ActionTextCard.jsx
export const ACTIONS_LIST = [
  {
    id: 'none',
    label: 'none',
    icon: 'duplicate'
  },
  {
    id: 'duplicate',
    label: 'Copy to clipboard',
    icon: 'duplicate'
  },
  {
    id: 'dial_phone',
    label: 'Dial phone number',
    icon: 'dial_phone'
  },
  {
    id: 'link_screen',
    label: 'Link to Screen',
    icon: 'link_screen'
  },
  {
    id: 'open_link',
    label: 'Open link',
    icon: 'open_link'
  },
  {
    id: 'open_webview',
    label: 'Open webview',
    icon: 'open_webview'
  },
  {
    id: 'send_email',
    label: 'Send Email',
    icon: 'send_email'
  },
  {
    id: 'send_message',
    label: 'Send text message',
    icon: 'send_message'
  },
  {
    id: 'show_sharing',
    label: 'Show sharing option',
    icon: 'show_sharing'
  },
  {
    id: 'show_map',
    label: 'View on map',
    icon: 'show_map'
  }
];

// ImageCard.jsx
export const IMAGE_HEIGHTS = [
  {
    icon: "square",
    label: "Square",
  },
  {
    icon: "horizontal3_1",
    label: "3:1",
  },
  {
    icon: "horizontal3_2",
    label: "3:2",
  },
];
export const CROP_BEHAVIOURS = [
  {
    icon: "crop",
    label: "Crop",
  },
  {
    icon: "center",
    label: "Center",
  },
];

// ButtonCard.jsx
export const TEXT_DECORATIONS = [
  {
    icon: "text_fill",
    label: "TextFill",
  },
  {
    icon: "text_invert",
    label: "TextInvert",
  },
  {
    icon: "text_outline",
    label: "TextOutline",
  },
  {
    icon: "small",
    label: "Transparent",
  },
];

// InlineListCard.jsx
export const INLINE_STYLES = [
  {
    name: "List",
    icon: "list",
  },
  {
    name: "Compact",
    icon: "compact",
  },
  {
    name: "Tiles",
    icon: "tiles",
  },
  {
    name: "Calendar",
    icon: "calendar",
  },
  {
    name: "Map",
    icon: "map",
  },
];

// TimelineCard.jsx
export const TIMELINE_CONTROLS = [
  {
    icon: "speedometer",
    label: "Speedometer",
  },
  {
    icon: "progress_invert",
    label: "Progress",
  },
];

// StatusCard.jsx
export const CIRCLE_COLORS = [
  "#F44336",
  "#FF5722",
  "#FF9800",
  "#4CAF50",
  "#009688",
  "#00BCD4",
  "#8854D0",
  "#F62765",
  "conic-gradient(from 180deg at 50% 50%, #FF0000 0deg, #FFF500 78.75deg, #00FF19 150deg, #00D1FF 202.5deg, #001AFF 286.87deg, #DB00FF 356.25deg, #FF0000 360deg)",
];
export const SHAPE_LIST = [
  {
    icon: "outline_rectangle",
    label: "Rectangle",
  },
  {
    icon: "circle",
    label: "Circle",
  },
];
export const RATING_SHAPE_LIST = [
  {
    icon: "star",
    label: "Star",
  },
  {
    icon: "heart",
    label: "Like",
  },
];

// key.jsx
export const VOTE_ICON_LIST = [
  {
    icon: "hand_outline",
    label: "Hand",
  },
  {
    icon: "checkbox",
    label: "CheckBox",
  },
  {
    icon: "key",
    label: "Key",
  },
];
export const VOTE_ICONS_LIST = [
  {
    vote: {
      icon: "hand_outline",
      label: "Hand",
    },
    voted: {
      icon: "hand_fill",
      label: "Hand",
    }
  },
  {
    vote: {
      icon: "checkout_round_outline",
      label: "CheckBox",
    },
    voted: {
      icon: "checkout_round_fill",
      label: "CheckBox",
    }
  },
  {
    vote: {
      icon: ''
    },
    voted: {
      icon: ''
    }
  },
];
// MapCard.jsx
export const MAP_ZOOM_LIST = [
  {
    id: "near",
    icon: "",
    label: "Near",
  }, {
    id: "medium",
    icon: "",
    label: "Medium",
  }, {
    id: "far",
    icon: "",
    label: "Far",
  },
];

// LinkCard.jsx
export const LINK_LABEL_LIST = [
  {
    id: "fill_area",
    icon: "fill_area",
    label: "Page title",
  },
];
export const LINK_TITLE_LIST = [
  {
    id: "fill_area",
    icon: "fill_area",
    label: "Link",
  },
];

// InlineList.jsx
export const INLINE_VALUES_LIST = [
  {
    id: "contacts",
    icon: "contact",
    label: "Contacts",
  },
];
export const INLINE_ACTIONS_LIST = [
  {
    id: "details",
    icon: "hand_pointer",
    label: "View Details",
  },
];

// CommentCard.jsx
export const COMMENT_ORDER_LIST = [
  {
    id: "order",
    icon: "order",
    label: "Order First",
  },
];

// PeopleCard.jsx
export const PEOPLE_COLUMN_LIST = [
  {
    id: "contacts",
    icon: "user",
    label: "Contacts",
  },
];
export const PEOPLE_ID_LIST = [
  {
    id: "avatar",
    icon: "member",
    label: "Avatar",
  },
];

// TimelineCard.jsx
export const TIMELINE_CHART_LIST = [
  {
    id: "start_sales",
    icon: "chart",
    label: "Start sales",
  },
  {
    id: "end_sales",
    icon: "chart",
    label: "Ending sales",
  },
];

// WorldClockCard.jsx
export const WORLDCLOCK_OPTION_LIST = [
  {
    id: "24",
    icon: "",
    label: "24 h",
  },
  {
    id: "12",
    icon: "",
    label: "12 h",
  },
];

// VoteCard.jsx
export const VOTE_OPTION_LIST = [
  {
    id: "hand_fill",
    icon: "hand_fill",
    label: "Votes",
  },
];

// StatusCard.jsx
export const STATUS_OPTION_LIST = [
  {
    id: "date",
    icon: "calendar",
    label: "Date",
  },
  {
    id: "fill_area",
    icon: "fill_area",
    label: "24 h",
  },
];

// ThemeCard.jsx
export const LIGHT_THEMES = [
  {
    label: "Light",
    image: "light.png",
    bodyColor: 'white',
    headColor: 'white',
    footColor: 'white',
    fontColorHead: '#303655',
  },
  {
    label: "Bold (light)",
    image: "bold-light.png",
    bodyColor: 'white',
    headColor: '#F62765',
    footColor: 'white',
    fontColorHead: 'white',
  },
  {
    label: "Pure (light)",
    image: "pure-light.png",
    bodyColor: 'white',
    headColor: 'white',
    footColor: 'white',
    fontColorHead: '#303655',
  },
];
export const DARK_THEMES = [
  {
    label: "Dark",
    image: "dark.png",
    bodyColor: '#1C1C25',
    headColor: '#303655',
    footColor: '#303655',
    fontColor: '#ffffff',
    fontColorHead: '#ffffff',
  },
  {
    label: "Bold (dark)",
    image: "bold-dark.png",
    bodyColor: '#1C1C25',
    headColor: '#F62765',
    footColor: '#303655',
    fontColor: '#ffffff',
    fontColorHead: '#ffffff',
  },
  {
    label: "Pure (dark)",
    image: "pure-dark.png",
    bodyColor: '#303655',
    headColor: '#303655',
    footColor: '#303655',
    fontColor: '#ffffff',
    fontColorHead: '#ffffff',
  },
];
