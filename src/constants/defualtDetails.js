export default {
  components: [
    {
      id: "actionText_0",
      icon: "action_text",
      label: "email",
      name: "Action Text",
    },
    {
      id: "title_2",
      icon: "title",
      label: "name",
      name: "Title",
    },
    {
      id: "link_5",
      icon: "link",
      label: "email",
      name: "Link",
    },
    {
      id: "phone_6",
      icon: "phone_",
      label: "phone",
      name: "Phone",
    },
    {
      id: "dateCalendar_7",
      icon: "date",
      label: "name",
      name: "Date",
    },
    {
      id: "timeline_8",
      icon: "timeline",
      label: "Timeline",
      name: "Timeline",
    },
    {
      id: "worldClock_9",
      icon: "clock",
      label: "date4",
      name: "World Clock",
    },
  ],
  componentDetails: [
    {
      hint: "column",
      column: "email",
      title: "",
      options: true,
      action: "send_email",
      data: "email",
    },
    {
      hint: "title",
      title: "name",
      custom_title: "",
      details: "text",
      custom_details: "",
      image: "files5",
      custom_image: "",
      design: "url",
      action: "send_email",
      data: "email",
    },
    {
      hint: "link",
      link: "email",
      label: "fill_area",
      title: "fill_area",
    },
    {
      hint: "column",
      column: "phone",
      body: "name",
      title: "",
    },
    {
      hint: "column",
      column: "date4",
      title: "",
      format: "DD/MM/YYYY",
    },
    {
      column: "timeline",
      title: "",
      type: 0,
    },
    {
      hint: "column",
      column: "world_clock",
      title: "",
      format: "12",
    },
  ],
  componentIndex: null,
};
