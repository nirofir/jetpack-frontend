export default [
  {
    label: "Layout",
    icon: "layout",
    class: "nav-item",
  },
  {
    label: "Tabs",
    icon: "tabs",
    class: "nav-item",
  },
  {
    label: "",
    icon: "divider",
  },

  // {
  //   label: "Data",
  //   icon: "data",
  //   class: "nav-item",
  // },
  // {
  //   label: "Preview",
  //   icon: "preview",
  //   class: "nav-item",
  // },
  {
    label: "Theme",
    icon: "settings",
    class: "nav-item",
  },
];
