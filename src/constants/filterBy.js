
// const mapFieldTypeToOperator = {

// }

// export default [{label : "Exist", id : "!!", filterType : "hasValue"},
//     {label : "Not Exist", id : "!!!", filterType : "hasValue"},
//     {label : "Equal to", id : "===" , filterType : "equal"},
//     {label : "Not equal to", id : "!==" , filterType : "equal"},
//     {label : "Contains" , id : "contains" , filterType : "text"},
//     {label : "Not contains" , id : "not contains" , filterType : "text"}
//     ]
const basicOperations = 
        [{label : "Exist", id : "!!", filterType : "hasValue"},
        {label : "Not Exist", id : "!!!", filterType : "hasValue"},
        {label : "Equal to", id : "===" , filterType : "equal"},
        {label : "Not equal to", id : "!==" , filterType : "equal"}] 
const texty = [      {label : "Contains" , id : "contains" , filterType : "text"},
{label : "Not contains" , id : "not contains" , filterType : "text"}]

export default {
    basic : basicOperations,
    text : [...basicOperations,
            ...texty
    ],
    name : [...basicOperations,
        ...texty
    ],
    color :  [...basicOperations,
        ...texty
    ],
    numeric : [...basicOperations,
        {label : "Bigger than" , id : ">" , filterType : "number"},
        {label : "Smaller than" , id : "<" , filterType : "number"},
    ],
    tags: [
        {label : "Have tag" , id : "ht" , filterType : "tag"},
        {label : "Dont have tag" , id : "dht" , filterType : "tag"},
    ],
}