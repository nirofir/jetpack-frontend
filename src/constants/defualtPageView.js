export const PropertiesTypeList = {
  stringy : ["name","ownerMaybe","text","status","color","date","timerange","tags","numeric","long-text","timezone","pulse-id","phone","location","votes","rating","email","pulse-log","pulse-updated","link"],
  imagey : ["file"],
  datey : ["date", "timelineMaybe"],
  addressy : ["location"],
  linky : ["link"],
  numbery : ["numeric"],
  votey : ["votes"],
  statusy : ["color"]
  
}

export const getAvailColumns = (columns,columnType) => (columns.filter(column => PropertiesTypeList[columnType].indexOf(column.type) !== -1))

export const defualtComponent =  
{
  components: [
  {
  icon: "action_text",
  label: "email",
  name: "Action Text",
},
{
  icon: "separator",
  label: "Separator",
  name: "Separator",
},
{
  icon: "title",
  label: "name",
  name: "Title",
},
{
  icon: "image_",
  label: "files",
  name: "Image",
},
{
  icon: "map_pointer",
  label: "location",
  name: "Map",
},
{
  icon: "link",
  label: "email",
  name: "Link",
},
{
  icon: "phone_",
  label: "phone",
  name: "Phone",
},
{
  icon: "date",
  label: "name",
  name: "Date",
},
{
  icon: "timeline",
  label: "Timeline",
  name: "Timeline",
},
{
  icon: "clock",
  label: "date4",
  name: "World Clock",
},
{
  icon: "status",
  label: "status",
  name: "Status",
},
{
  icon: "todo",
  label: "creation_log",
  name: "Creation Log",
},
],
componentDetails: [
{
  hint: "column",
  column: "email",
  title: "",
  options: true,
  action: "send_email",
  data: "email",
},
{},
{
  hint: "title",
  title: "name",
  custom_title: "",
  details: "text",
  custom_details: "",
  image: "files5",
  custom_image: "",
  design: "url",
},
{
  hint: "image",
  image: "files",
  custom_image: "",
  image_height: 0,
  fill: "fill_area",
  style: "insert_content",
  action: "none",
  data: "email",
  crop: 0,
},
{
  hint: "address",
  address: "location",
  zoom: "near",
},
{
  hint: "link",
  link: "email",
  label: "fill_area",
  title: "fill_area",
},
{
  hint: "column",
  column: "phone",
  body: "name",
  title: "",
},
{
  hint: "column",
  column: "date4",
  title: "",
  format: "DD/MM/YYYY",
},
{
  column: "timeline",
  title: "",
  type: 0,
},
{
  hint: "column",
  column: "world_clock",
  title: "",
  format: "12",
},
{
  hint: "column",
  column: "status",
  title: "",
  shape: 0,
  color: "#00BCD4",
},
{
  hint: "column",
  column: "creation_log",
  title: "",
}]}

export const defualtProperties = {
  style: "list",
  appHeaderTitle: "none",
  appHeaderCustomTitle: "",
  hasSearchBar: true,
  show_image: true,
  image_is: "url",
  shape: "square",
  tiles_in_row: 1,
  corner: true, // if rectangle = true, circle => false
  padding: true, // if padding out => true, padding in => false
  crop: true, // if crop => true, center => false
  text_position: true, // if bottom => true, center => false
  text_alignment: "align_center",
  text_size: "small",
  text_wrap: false,
  text_caps: false,
  // app_filter_by: "none",
  detailsIndex : 0,
  filter_by: "none",
  sort_by: "none",
  button: "none",
  tag: "none",
  avatar: "none",
  avatar_text: "none",
  caption_overlay: "none",
  when: "none",
  order: "none",
  address: "none",
  title: "none",
  details: "none",
  caption: "none",
  image: "none",
  view: true,
  option: false,
  prev: [],
}
export const emptyState =  {
  // detailPageStyle : defualtComponent,
  detailPageStyle : {
    components: [],
    componentDetails : []
  },
  property : defualtProperties,
  components: [],
  componentDetails : [],
  // ...defualtComponent,
  componentIndex: null,
  itemIndex: 0,
};
